import { grid } from '../grid';
import { Position } from '../position.model';
import { Stat } from './stat';

/**
 * - Le plateau de jeu de mines
 * @public
 */
export class Board extends grid<Stat> {
    constructor() {
        super(20, 20, Stat.dead);
        this.initGrid();
    }

    /**
     * - Initialise la grille
     */
    initGrid(): void {
        const alivePositions: Position[] = [
            { y: 0, x: 1 },
            { y: 1, x: 2 },
            { y: 2, x: 0 },
            { y: 2, x: 1 },
            { y: 2, x: 2 },

            { y: 12, x: 7 },
            { y: 12, x: 8 },
            { y: 12, x: 9 },

            { y: 5, x: 7 },
            { y: 5, x: 8 },
            { y: 6, x: 7 },
            { y: 6, x: 8 },
            { y: 7, x: 9 },
            { y: 7, x: 10 },
            { y: 8, x: 9 },
            { y: 8, x: 10 },

            { y: 3, x: 13 },
            { y: 3, x: 14 },
            { y: 4, x: 13 },
            { y: 4, x: 15 },
            { y: 5, x: 14 },

            { y: 13, x: 16 },
            { y: 13, x: 17 },
            { y: 13, x: 18 },
            { y: 14, x: 15 },
            { y: 14, x: 16 },
            { y: 14, x: 17 },
        ];

        alivePositions.forEach((position) => {
            this.setValue(position, Stat.alive);
        });
    }

    /** - Change l'état de la cellule */
    toggleValue(position: Position): void {
        if (this.positionIsValid(position)) {
            this.setValue(
                position,
                this.getValue(position) === Stat.alive ? Stat.dead : Stat.alive
            );
        }
    }

    /**
     * - Renvoie le nombre de voisins vivants de la cellule
     * @param position - Position de la cellule
     */
    neighboursCount(position: Position): number {
        let neighboursCount = 0;
        if (this.positionIsValid(position)) {
            for (let line = position.y - 1; line <= position.y + 1; line++) {
                for (
                    let column = position.x - 1;
                    column <= position.x + 1;
                    column++
                ) {
                    const cellPosition: Position = { x: column, y: line };
                    if (
                        this.positionIsValid(cellPosition) &&
                        this.getValue(cellPosition) === Stat.alive &&
                        (line !== position.y || column !== position.x)
                    ) {
                        neighboursCount++;
                    }
                }
            }
            return neighboursCount;
        }

        console.error(
            `Impossible de faire neighboursCount({x:${position.x}, y:${position.y}}), la position n'est pas valide!`
        );
        return 0;
    }

    /**
     * - Renvoie l'état de la cellule au tour suivant
     * @param position
     * @returns Une cellule naît si elle a deux voisins et reste en vie si elle en a deux ou trois
     */
    willBeAlive(position: Position): Stat {
        if (
            this.neighboursCount(position) === 3 ||
            (this.neighboursCount(position) === 2 &&
                this.getValue(position) === Stat.alive)
        ) {
            return Stat.alive;
        }
        return Stat.dead;
    }

    /**
     * - Fais évoluer la grille d'un tour
     * @returns Renvoie vrai si la grille a été modifiée, faux sinon
     */
    evolve(): boolean {
        /** - Vaudra vrai si la grille a été modifiée, faux sinon */
        let isModified: boolean = false;
        // Faire une copie de la grille réprésentant l'état suivant
        const evolvedGrid: Stat[][] = [];
        for (let iRow = 0; iRow < this.getHeight(); iRow++) {
            const row: Stat[] = [];
            evolvedGrid.push(row);
            for (let iColumn = 0; iColumn < this.getLength(); iColumn++) {
                row.push(this.willBeAlive({ x: iColumn, y: iRow }));
            }
        }

        for (let iRow = 0; iRow < this.getHeight(); iRow++) {
            for (let iColumn = 0; iColumn < this.getLength(); iColumn++) {
                isModified ||=
                    this.getValue({ x: iColumn, y: iRow }) !==
                    evolvedGrid[iRow][iColumn];
                this.setValue(
                    { x: iColumn, y: iRow },
                    evolvedGrid[iRow][iColumn]
                );
            }
        }
        return isModified;
    }
}
