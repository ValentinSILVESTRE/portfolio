export interface Link {
    title: string;
    url: string;
    hasIcon: boolean;
    iconSrc?: string;
    iconAlt?: string;
}
