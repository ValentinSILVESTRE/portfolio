/** - Liste des langages et frameworks */
export enum Tag {
    IA = 'Intelligence Artificielle',
    Angular = 'Angular',
    TypeScript = 'TypeScript',
    Sass = 'Sass',
    Html = 'HTML',
    Javascript = 'JavaScript',
    Symfony = 'Symfony',
    Css = 'CSS',
    Php = 'PHP',
}

export function getTagIconSource(tag: Tag): string {
    switch (tag) {
        case Tag.Angular:
            return 'assets/icons/angular.png';
        case Tag.IA:
            return 'assets/icons/ai.png';
        case Tag.TypeScript:
            return 'assets/icons/typescript.png';
        case Tag.Sass:
            return 'assets/icons/sass.png';
        case Tag.Html:
            return 'assets/icons/html.png';
        case Tag.Javascript:
            return 'assets/icons/js.svg';
        case Tag.Symfony:
            return 'assets/icons/symfony.png';
        case Tag.Css:
            return 'assets/icons/css.png';
        case Tag.Php:
            return 'assets/icons/php.png';
    }
}
