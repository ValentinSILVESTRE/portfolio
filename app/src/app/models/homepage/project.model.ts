import { Link } from './link.model';
import { Tag } from './tag';

/**
 * - Un projet du portfolio, ex : Mondrian
 * @public
 */
export interface Project {
    /** - L'id du projet */
    id?: number;
    /** - Le nom du projet */
    name: string;
    /** - La description du projet */
    description?: string;
    /** - L'url du projet */
    url: string;
    /** - Vrai si l'url est externe à l'application Angular (ex: MHB) */
    externalUrl: boolean;
    /** - L'url de la photo de présentation du projet */
    imageUrl: string;
    /** - L'url de la photo de présentation du projet */
    imageUrlBis?: string;
    /** - L'attribut alt de la photo */
    imageAlt: string;
    /** - L'attribut alt de la photo */
    isFinish: boolean;
    /** - Les tags */
    tags: Tag[];
    /** - Les liens */
    links: Link[];
}
