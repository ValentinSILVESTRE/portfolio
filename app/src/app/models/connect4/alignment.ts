import { Position } from '../position.model';
import { Cell } from './cell';

export interface Alignment {
    cells: Cell[];
    position: Position;
    direction: string;
    decalage: number;
}
