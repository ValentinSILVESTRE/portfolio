export enum Direction {
    vertical = 'vertical',
    horizontal = 'horizontal',
    diagonal1 = 'diagonal1',
    diagonal2 = 'diagonal2',
}
