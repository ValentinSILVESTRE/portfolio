import { Position } from '../position.model';
import { Color } from './color';

export interface Cell {
    position: Position;
    color: Color;
}
