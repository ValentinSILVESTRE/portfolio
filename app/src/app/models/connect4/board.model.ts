import { grid } from '../grid';
import { Position } from '../position.model';
import { Cell } from './cell';
import { Color } from './color';
import { Alignment } from './alignment';
import { Direction } from './direction';

/**
 * - Le plateau de jeu du puissance4
 * @public
 */
export class Board extends grid<Color> {
    private winner: Color | null = null;
    botColor: Color = Color.yellow;
    humanColor: Color = Color.red;
    debug: boolean = true;
    constructor() {
        super(7, 6, Color.none);
        // this.setValue({ x: 1, y: 5 }, this.botColor);
        // this.setValue({ x: 2, y: 5 }, this.botColor);
        // this.setValue({ x: 4, y: 4 }, this.botColor);
        // this.setValue({ x: 5, y: 3 }, this.botColor);
        // this.setValue({ x: 4, y: 5 }, this.humanColor);
        // this.setValue({ x: 5, y: 5 }, this.humanColor);
        // this.setValue({ x: 6, y: 5 }, this.botColor);
        // this.setValue({ x: 5, y: 4 }, this.humanColor);
        // this.setValue({ x: 6, y: 4 }, this.humanColor);
        // this.setValue({ x: 6, y: 3 }, this.humanColor);
        // this.setValue({ x: 2, y: 5 }, this.botColor);
        // this.setValue({ x: 3, y: 5 }, this.botColor);
    }

    reset(): void {
        this.resetGrid();
        this.winner = null;
    }

    getWinner(): Color | null {
        return this.winner;
    }

    getWinningPositions(): Position[] | null {
        if (this.getWinningData()) {
            return this.getWinningData()!.positions;
        }
        return null;
    }

    toStringPosition(position: Position): string {
        return `{x:${position.x}, y:${position.y}}`;
    }

    toStringAlignment(alignement: Alignment): string {
        let text: string = `Alignement ${
            alignement.direction
        } de ${this.toStringPosition(alignement.position)}, décalage ${
            alignement.decalage
        }`;
        for (const cell of alignement.cells) {
            text += `\n${this.toStringPosition(cell.position)} - ${cell.color}`;
        }
        return text;
    }

    /**
     * - Renvoie vrai une pièce à cette position ferait gagner color
     */
    isWinningPosition(position: Position, color: Color): boolean {
        for (const alignment of this.getAlignments(position, color)) {
            if (alignment.cells.every((cell: Cell) => cell.color === color)) {
                return true;
            }
        }
        return false;
    }

    /**
     * - Renvoie vrai si la position est occupée, faux sinon
     */
    positionIsTaken(position: Position): boolean {
        return this.getValue(position) !== Color.none;
    }

    /**
     * - Renvoie vrai si la colonne est pleine, faux sinon
     */
    columnIsFull(columnIndex: number): boolean {
        return this.positionIsTaken({ x: columnIndex, y: 0 });
    }

    /**
     * - Pose une pièce de la couleur demandée dans la colonne si elle n'est pas pleine, et qu'il n'y a pas de gagnant
     */
    play(columnIndex: number, color: Color): void {
        if (
            !this.columnIsFull(columnIndex) &&
            color !== Color.none &&
            !this.winner
        ) {
            let rowIndex = this.getHeight() - 1;
            while (
                rowIndex >= 0 &&
                this.getValue({ x: columnIndex, y: rowIndex }) !== Color.none
            ) {
                rowIndex--;
            }
            this.setValue({ x: columnIndex, y: rowIndex }, color);

            // On détermine le gagnant s'il y en a un
            const winningData = this.getWinningData();
            if (winningData) {
                this.winner = winningData.color;
            }
        }
    }

    /**
     * - Renvoie la couleure gagnante s'il y en a une, faux sinon
     */
    getWinningData(): { color: Color; positions: Position[] } | null {
        // On vérifie en ligne
        for (let line = 0; line < this.getHeight(); line++) {
            for (let column = 0; column < 4; column++) {
                let color0: Color = this.getValue({ y: line, x: column });
                let color1: Color = this.getValue({ y: line, x: column + 1 });
                let color2: Color = this.getValue({ y: line, x: column + 2 });
                let color3: Color = this.getValue({ y: line, x: column + 3 });
                if (
                    color0 !== Color.none &&
                    color0 === color1 &&
                    color0 === color2 &&
                    color0 === color3
                ) {
                    return {
                        color: color0,
                        positions: [
                            { y: line, x: column },
                            { y: line, x: column + 1 },
                            { y: line, x: column + 2 },
                            { y: line, x: column + 3 },
                        ],
                    };
                }
            }
        }

        // On vérifie en colonne
        for (let line = 0; line < 3; line++) {
            for (let column = 0; column < 7; column++) {
                let color0: Color = this.getValue({ y: line, x: column });
                let color1: Color = this.getValue({ y: line + 1, x: column });
                let color2: Color = this.getValue({ y: line + 2, x: column });
                let color3: Color = this.getValue({ y: line + 3, x: column });
                if (
                    color0 !== Color.none &&
                    color0 === color1 &&
                    color0 === color2 &&
                    color0 === color3
                ) {
                    return {
                        color: color0,
                        positions: [
                            { y: line, x: column },
                            { y: line + 1, x: column },
                            { y: line + 2, x: column },
                            { y: line + 3, x: column },
                        ],
                    };
                }
            }
        }

        // On vérifie en première diagonale
        for (let line = 0; line < 3; line++) {
            for (let column = 0; column < 4; column++) {
                let color0: Color = this.getValue({ y: line, x: column });
                let color1: Color = this.getValue({
                    y: line + 1,
                    x: column + 1,
                });
                let color2: Color = this.getValue({
                    y: line + 2,
                    x: column + 2,
                });
                let color3: Color = this.getValue({
                    y: line + 3,
                    x: column + 3,
                });
                if (
                    color0 !== Color.none &&
                    color0 === color1 &&
                    color0 === color2 &&
                    color0 === color3
                ) {
                    return {
                        color: color0,
                        positions: [
                            { y: line, x: column },
                            { y: line + 1, x: column + 1 },
                            { y: line + 2, x: column + 2 },
                            { y: line + 3, x: column + 3 },
                        ],
                    };
                }
            }
        }

        // On vérifie en seconde diagonale
        for (let line = 0; line < 3; line++) {
            for (let column = 3; column < 7; column++) {
                let color0: Color = this.getValue({ y: line, x: column });
                let color1: Color = this.getValue({
                    y: line + 1,
                    x: column - 1,
                });
                let color2: Color = this.getValue({
                    y: line + 2,
                    x: column - 2,
                });
                let color3: Color = this.getValue({
                    y: line + 3,
                    x: column - 3,
                });
                if (
                    color0 !== Color.none &&
                    color0 === color1 &&
                    color0 === color2 &&
                    color0 === color3
                ) {
                    return {
                        color: color0,
                        positions: [
                            { y: line, x: column },
                            { y: line + 1, x: column - 1 },
                            { y: line + 2, x: column - 2 },
                            { y: line + 3, x: column - 3 },
                        ],
                    };
                }
            }
        }
        return null;
    }

    // ! ===============================================================================================|
    // !                                                                                                |
    // !                                              BOT                                               |
    // !                                                                                                |
    // ! ===============================================================================================|

    /**
     * - Renvoie le nombre de pièces de la couleur demandée
     */
    colorCount(color: Color): number {
        let count = 0;
        for (let rowIndex = 0; rowIndex < this.getHeight(); rowIndex++) {
            for (
                let columnIndex = 0;
                columnIndex < this.getLength();
                columnIndex++
            ) {
                if (this.getValue({ y: rowIndex, x: columnIndex }) === color) {
                    count++;
                }
            }
        }
        return count;
    }

    botPlay(color: Color): void {
        const botChoice = this.botChoice();
        this.play(botChoice, color);
    }

    /** - Le choix du premier tour */
    firstShot(): number {
        return 3;
    }

    /** - Nombre de pièces dans la colonne */
    columnFillingCount(column: number): number {
        let height: number = this.getHeight() - 1;
        let count = 0;
        while (height >= 0 && this.positionIsTaken({ x: column, y: height })) {
            height--;
            count++;
        }
        return count;
    }

    /**
     * - Renvoie la liste des alignements de length pièces qui passent par la case passée en paramètres si je la pose
     */
    getAlignments(
        position: Position,
        color: Color,
        length: 2 | 3 | 4 = 4
    ): Alignment[] {
        const alignments: Alignment[] = [];

        // Si les données sont valides
        if (this.positionIsValid(position) && !this.positionIsTaken(position)) {
            // Pour chaque direction
            for (const direction in Direction) {
                // Pour chaque décalage
                for (let decalage = length - 1; decalage >= 0; decalage--) {
                    // On définit les deux extrémités de l'alignement
                    const firstPosition: Position = { x: 0, y: 0 };
                    const lastPosition: Position = { x: 0, y: 0 };

                    switch (direction) {
                        case Direction.vertical:
                            firstPosition.x = position.x;
                            firstPosition.y = position.y - decalage;
                            lastPosition.x = position.x;
                            lastPosition.y = firstPosition.y + (length - 1);
                            break;
                        case Direction.horizontal:
                            firstPosition.x = position.x - decalage;
                            firstPosition.y = position.y;
                            lastPosition.x = firstPosition.x + (length - 1);
                            lastPosition.y = position.y;
                            break;
                        case Direction.diagonal1:
                            firstPosition.x = position.x - decalage;
                            firstPosition.y = position.y - decalage;
                            lastPosition.x = firstPosition.x + (length - 1);
                            lastPosition.y = firstPosition.y + (length - 1);
                            break;
                        case Direction.diagonal2:
                            firstPosition.x = position.x - decalage;
                            firstPosition.y = position.y + decalage;
                            lastPosition.x = firstPosition.x + (length - 1);
                            lastPosition.y = firstPosition.y - (length - 1);
                            break;
                    }

                    // Si les deux extrémités sont dans la grille
                    if (
                        this.positionIsValid(firstPosition) &&
                        this.positionIsValid(lastPosition)
                    ) {
                        // On ajoute l'alignement à la liste
                        const alignment: Alignment = {
                            cells: [],
                            decalage: decalage,
                            position: position,
                            direction: direction,
                        };
                        alignments.push(alignment);

                        for (let i = 0; i < length; i++) {
                            const iPosition: Position = { x: 0, y: 0 };
                            switch (direction) {
                                case 'vertical':
                                    iPosition.x = firstPosition.x;
                                    iPosition.y = firstPosition.y + i;
                                    break;
                                case 'horizontal':
                                    iPosition.x = firstPosition.x + i;
                                    iPosition.y = firstPosition.y;
                                    break;
                                case 'diagonal1':
                                    iPosition.x = firstPosition.x + i;
                                    iPosition.y = firstPosition.y + i;
                                    break;
                                case 'diagonal2':
                                    iPosition.x = firstPosition.x + i;
                                    iPosition.y = firstPosition.y - i;
                                    break;
                            }

                            // La couleur est celle de la case, sauf si c'est celle passée en paramètres de la fonction, auquel cas on la remplis avec la couleur passée en paramètres
                            let iColor: Color = this.getValue(iPosition);
                            if (
                                iPosition.x === position.x &&
                                iPosition.y === position.y
                            ) {
                                iColor = color;
                            }

                            const cell: Cell = {
                                color: iColor,
                                position: iPosition,
                            };

                            alignment.cells.push(cell);
                        }
                    }
                }
            }
        }
        return alignments;
    }

    firstAvailableLine(column: number): number {
        return this.getHeight() - 1 - this.columnFillingCount(column);
    }

    /**
     * - On renvoie la prochainne position disponible de la colonne
     * @param column - Colonne non pleine d'indice valide
     */
    getNextPosition(column: number): Position {
        return { x: column, y: this.firstAvailableLine(column) };
    }

    /**
     * - Renvoie vrai si en plaçant une pièce dans la colonne, la couleur gagne
     */
    isWinningColumn(column: number, color: Color): boolean {
        return (
            !this.columnIsFull(column) &&
            this.isWinningPosition(
                {
                    x: column,
                    y: this.firstAvailableLine(column),
                },
                color
            )
        );
    }

    /**
     * - Les colonnes qui sont à deux pièces de gagner
     */
    winningSetupColumns(color: Color): number[] {
        const columns: number[] = [];
        // Pour chaque colonne
        for (let column = 0; column < this.getLength(); column++) {
            // Si la colonne a deux cases disponibles
            if (this.columnFillingCount(column) <= this.getHeight() - 2) {
                // Et que la seconde plus basse fais gagner color
                if (
                    this.isWinningPosition(
                        { x: column, y: this.firstAvailableLine(column) - 1 },
                        color
                    )
                ) {
                    // Alors on l'ajoute à la liste
                    columns.push(column);
                }
            }
        }
        return columns;
    }

    /**
     * - Renvoie vrai si l'alignement est composé de 3 color, et un vide, et que le vide est au dessus d'une pièce
     */
    alignmentIsWinningNextTurn(alignement: Alignment, color: Color): boolean {
        let colorCount = 0;
        let emptyCount = 0;
        const emptyPosition: Position = { x: 0, y: 0 };
        for (const cell of alignement.cells) {
            switch (cell.color) {
                case color:
                    colorCount++;
                    break;
                case Color.none:
                    emptyCount++;
                    emptyPosition.x = cell.position.x;
                    emptyPosition.y = cell.position.y;
                    break;
            }
        }

        if (colorCount === 3 && emptyCount === 1) {
            const belowPosition: Position = {
                x: emptyPosition.x,
                y: emptyPosition.y! + 1,
            };
            return (
                emptyPosition.y === this.getHeight() - 1 ||
                this.positionIsTaken(belowPosition)
            );
        }

        return false;
    }

    /**
     * - Renvoie vrai si mettre une pièce dans cette colonne provoque une fourchette
     */
    fork(column: number, color: Color): boolean {
        // On vérifie que la colonne n'est pas pleine
        if (!this.columnIsFull(column)) {
            const position: Position = this.getNextPosition(column);
            const alignments: Alignment[] = this.getAlignments(position, color);

            /** Alignement de 3 color et un vide directement remplissable */
            let okAlignmentCount: number = 0;

            for (const alignment of alignments) {
                if (this.alignmentIsWinningNextTurn(alignment, color)) {
                    okAlignmentCount++;
                }
            }
            return okAlignmentCount >= 2;
        }
        return false;
    }

    /**
     * - Renvoie un choix de colonne
     */
    botChoice(): number {
        // * Premier coup
        if (this.colorCount(this.botColor) === 0) {
            return this.firstShot();
        }

        // * On renvoie une colonne ou l'ordi gagne si on trouve
        for (let column = 0; column < this.getLength(); column++) {
            if (this.isWinningColumn(column, this.botColor)) {
                return column;
            }
        }

        // * On renvoie une colonne ou l'humain gagne si on trouve
        // * On renvoie une colonne ou l'ordi gagne si on trouve
        for (let column = 0; column < this.getLength(); column++) {
            if (this.isWinningColumn(column, this.humanColor)) {
                return column;
            }
        }

        // * On ne se place pas en dessous d'une case qui nous ferait perdre
        /** @type{Array.<number>} Liste des colonnes qui ne permettent pas à l'adversaire de gagner si l'on s'y met puis lui ensuite */
        const preLoosingColumns = this.winningSetupColumns(this.humanColor);

        /** @type{Array.<number>} Liste des colonnes qui permettent à l'adversaire de nous empêcher de gagner si l'on s'y met puis lui ensuite */
        const preWinningColumns = this.winningSetupColumns(this.botColor);

        // * Fourchettes

        // Si on peut provoquer une fourchette dans une colonne non pleine qui ne nous fait pas perdre, alors on le fait
        for (let column = 0; column < 7; column++) {
            if (
                !this.columnIsFull(column) &&
                !preLoosingColumns.includes(column) &&
                this.fork(column, this.botColor)
            ) {
                return column;
            }
        }

        // Si on peut éviter une fourchette de l'humain, alors on le fait
        for (let column = 0; column < 7; column++) {
            if (
                !this.columnIsFull(column) &&
                !preLoosingColumns.includes(column) &&
                this.fork(column, this.humanColor)
            ) {
                return column;
            }
        }

        // // * Alignements de trois avec un trou au milieu
        // // for (let column = 0; column < 7; column++) {
        // //     if (this.trouAuMilieu('yellow', column)) {
        // //         return column;
        // //     }
        // // }
        // // for (let column = 0; column < 7; column++) {
        // //     if (this.trouAuMilieu('red', column)) {
        // //         return column;
        // //     }
        // // }

        // // * Alignements de trois
        // if (false) {
        // 	for (let column = 0; column < 7; column++) {
        // 		const line = this.firstAvailableLine(column);
        // 		// Pour chaque direction H, D1 et D2
        // 		['H', 'D1', 'D2'].forEach((direction) => {
        // 			// Pour chaque décalage
        // 			for (let decalage = 0; decalage < 4; decalage++) {
        // 				// Si la combinaison est possible
        // 				if (
        // 					this.getCasePosition(
        // 						line,
        // 						column,
        // 						direction,
        // 						decalage,
        // 						0,
        // 						4
        // 					)
        // 				) {
        // 						`Alignement de trois pour this.getCasePosition(${line}, ${column}, ${direction}, ${decalage}, 0, 4)`
        // 					);
        // 					// Je note les positions des 4 cases
        // 					const cases = [];
        // 					for (let position = 0; position < 4; position++) {
        // 						cases.push(
        // 							this.getCasePosition(
        // 								line,
        // 								column,
        // 								direction,
        // 								decalage,
        // 								position,
        // 								4
        // 							)
        // 						);
        // 					}
        // 					// Si 3 sont de ma couleur et que la 4e n'est pas directement bouchable
        // 					let nbYellow = 0;
        // 					let nbEmpty = 0;
        // 					let videEstBouchable = false;
        // 					for (let i = 0; i < 4; i++) {
        // 						let color = this.getColor(
        // 							cases[i][0],
        // 							cases[i][1]
        // 						);
        // 						if (color === 'yellow') {
        // 							nbYellow++;
        // 						}
        // 						if (color === '') {
        // 							nbEmpty++;
        // 							if (
        // 								this.firstAvailableLine(cases[i][1]) ===
        // 								cases[i][0]
        // 							) {
        // 								videEstBouchable = true;
        // 							}
        // 						}
        // 					}
        // 						`Il y a ${nbYellow} jaunes et ${nbEmpty} vide : ${
        // 							videEstBouchable ? 'ok' : 'pas ok'
        // 						}`
        // 					);
        // 					if (
        // 						nbYellow === 3 &&
        // 						nbEmpty === 1 &&
        // 						!videEstBouchable
        // 					) {
        // 						// Alors je m'y met
        // 						return column;
        // 					}
        // 				}
        // 			}
        // 		});
        // 	}
        // }

        // // * Alignement de trois parmis quatres
        // for (let c = 0; c < 7; c++) {
        // 	let column = centralColumn(c);
        // 	if (
        // 		notLoosingColumns.includes(column) &&
        // 		notWinningColumns.includes(column) &&
        // 		this.align3(column, 'yellow')
        // 	) {
        // 		return column;
        // 	}
        // 	if (
        // 		notLoosingColumns.includes(column) &&
        // 		notWinningColumns.includes(column) &&
        // 		this.align3(column, 'red')
        // 	) {
        // 		return column;
        // 	}
        // }

        // // * Alignement de deux parmis quatres
        // for (let c = 0; c < 7; c++) {
        // 	let column = centralColumn(c);
        // 	if (
        // 		notLoosingColumns.includes(column) &&
        // 		notWinningColumns.includes(column) &&
        // 		this.align(column, 'yellow', 2)
        // 	) {
        // 		return column;
        // 	}
        // 	if (
        // 		notLoosingColumns.includes(column) &&
        // 		notWinningColumns.includes(column) &&
        // 		this.align(column, 'red', 2)
        // 	) {
        // 		return column;
        // 	}
        // }

        // // * On choisit une colonne aléatoire parmis les restantes qui ne nous font pas perdre au tour suivant
        // if (notLoosingColumns.length > 0) {
        // 	let choice;
        // 	choice = +Math.floor(Math.random() * notLoosingColumns.length);
        // 	if (this.debug) {
        // 	}
        // 	return notLoosingColumns[choice];
        // }

        // * Si la colonne du milieu n'est pas pleine, on s'y met
        if (
            !this.columnIsFull(3) &&
            !preLoosingColumns.includes(3) &&
            !preWinningColumns.includes(3)
        ) {
            return 3;
        }

        // * On choisit une colonne aléatoire non pleine
        let randomColumn;
        do {
            randomColumn = Math.floor(Math.random() * 7);
        } while (this.columnIsFull(randomColumn));
        return randomColumn;
    }
}
