import { Evenement } from './evenement.model';
import { LinkType } from './linkType.model';

/**
 * @public
 */
export interface Link {
    id: number;
    url: string;
    title: string;
    type: LinkType;
    event: Evenement;
}
