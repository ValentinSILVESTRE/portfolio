export interface LinkType {
    id: number;
    name: 'live' | 'replay' | 'info';
}
