import { Category } from './category.model';
import { Link } from './link.model';

/**
 * - Un évenement
 * @public
 */
export interface Evenement {
    id: number;
    title: string;
    description?: string;
    date: Date;
    parentEventId?: number;
    categories: Category[];
    subEvents: Evenement[];
    links: Link[];
}
