import { Position } from '../position.model';

export class Cell {
    constructor(
        private isMine: boolean = false,
        private isReveal: boolean = false,
        private isFlagged: boolean = false
    ) {}

    getIsMine(): boolean {
        return this.isMine;
    }
    setIsMine(isMine: boolean): void {
        this.isMine = isMine;
    }
    getIsReveal(): boolean {
        return this.isReveal;
    }
    setIsReveal(isReveal: boolean): void {
        this.isReveal = isReveal;
    }
    getIsFlagged(): boolean {
        return this.isFlagged;
    }
    setIsFlagged(isFlagged: boolean): void {
        this.isFlagged = isFlagged;
    }

    reveal(): void {
        this.setIsReveal(true);
    }

    toString(): string {
        return `{isMine : ${this.isMine}, isReveal : ${this.isReveal}}`;
    }
}
