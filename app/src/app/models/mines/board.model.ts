import { randomIntegers } from 'src/assets/functions/maths.functions';
import { playSound } from 'src/assets/functions/sound.functions';
import { grid } from '../grid';
import { Position } from '../position.model';
import { Cell } from './cell.model';

/**
 * - Le plateau de jeu de mines
 * @public
 */
export class Board extends grid<Cell> {
    private defaultCell: Cell = new Cell();
    private isFinish: boolean = false;
    private winSongUri = 'assets/sounds/super_max.mp4';
    private looseSongUri = 'assets/sounds/explosion.mp4';

    _isWin: boolean = false;
    _isLoose: boolean = false;

    /**
     * - Créer une instance de type board (mines)
     * @param mineCount - Le nombre de mines
     */
    constructor(heigth: number, length: number, private mineCount: number = 4) {
        super(heigth, length, new Cell());
    }

    override toString(): string {
        let text: string = `Mine board: ${this.cellCount()} cellules dont ${
            this.mineCount
        } mines\n`;
        for (let row = 0; row < this.getHeight(); row++) {
            let rowText = `[ `;
            for (let column = 0; column < this.getLength(); column++) {
                const position: Position = { x: column, y: row };
                rowText += `${column ? ' ' : ''}${
                    this.getValue(position).getIsMine() ? 'X' : '-'
                }`;
            }
            text += rowText + ' ]\n';
        }

        return text;
    }

    getIsFinish(): boolean {
        return this.isFinish;
    }

    setIsFinish(isFinish: boolean): void {
        this.isFinish = isFinish;
    }

    /**
     * - Renvoie le nombre de mines non flaggées
     */
    undiscoveredMinesCount(): number {
        let count: number = 0;
        this.getCells().forEach((cell) => {
            if (cell.getIsMine() && !cell.getIsFlagged()) {
                count++;
            }
        });
        return count;
    }

    /**
     * - Fonction appelée quand la partie est perdue
     */
    onLoose(): void {
        this._isLoose = true;
        playSound(this.looseSongUri);
        this.setIsFinish(true);
        // On revèle toutes les cases
        this.getCells().forEach((cell) => {
            cell.setIsReveal(true);
        });
    }

    /**
     * - Fonction appelée quand la partie est gagnée
     */
    onWin(): void {
        this._isWin = true;
        playSound(this.winSongUri, 16.5);
        this.setIsFinish(true);
        // On revèle toutes les cases
        this.getCells().forEach((cell) => {
            cell.setIsReveal(true);
        });
    }

    /**
     * - Retourne vrai si la partie est gagnée
     */
    isWin(): boolean {
        for (const cell of this.getCells()) {
            // La partie n'est pas gagnée si on trouve une cellule non révelée qui n'est pas une mine
            if (!(cell.getIsMine() || cell.getIsReveal())) {
                return false;
            }
        }
        return true;
    }

    toggleFlag(position: Position): void {
        const cell: Cell = this.getValue(position);
        this.setValue(
            position,
            new Cell(cell.getIsMine(), cell.getIsReveal(), !cell.getIsFlagged())
        );
    }

    revealReccursive(position: Position): void {
        const cell: Cell = this.getValue(position);

        // Si la partie n'est pas finie et que la cellule n'est pas flaggée
        if (!(this.isFinish || cell.getIsFlagged())) {
            // Alors on la revele
            this.reveal(position);

            if (cell.getIsMine()) {
                this.onLoose();
            } else {
                // Si elle n'est pas proche d'une mine
                if (!this.getCloseMineCount(position)) {
                    // Alors on revele reccursivement les cellules voisines
                    this.getNeighboursPositions(position).forEach(
                        (neighbourPosition) => {
                            const neighbourCell: Cell =
                                this.getValue(neighbourPosition);
                            if (
                                !(
                                    neighbourCell.getIsReveal() ||
                                    neighbourCell.getIsFlagged() ||
                                    neighbourCell.getIsMine()
                                )
                            ) {
                                if (this.getCloseMineCount(neighbourPosition)) {
                                    this.reveal(neighbourPosition);
                                } else {
                                    this.revealReccursive(neighbourPosition);
                                }
                            }
                        }
                    );
                }
                if (this.isWin()) {
                    this.onWin();
                }
            }
        }
    }

    /**
     * - On révele une case
     */
    reveal(position: Position): void {
        this.setValue(
            position,
            new Cell(this.getValue(position).getIsMine(), true)
        );
    }

    /**
     * - Réinitialise le plateau
     */
    reset(): void {
        this.getCells().forEach((cell) => {
            cell.setIsMine(this.defaultCell.getIsMine());
            cell.setIsReveal(this.defaultCell.getIsReveal());
            cell.setIsFlagged(this.defaultCell.getIsFlagged());
        });
    }

    /**
     * - On place les mines
     */
    initialize(firstClickPosition: Position): void {
        this.reset();
        if (
            0 < this.mineCount &&
            this.mineCount < this.cellCount() &&
            this.positionIsValid(firstClickPosition)
        ) {
            // On enlève les indexs voisins du firstShoot de la liste mine indexs

            /** - Liste des indexs, voisins du premier clique, où il ne peut pas y avoir de mine */
            const invalidIndexs: number[] = [
                this.getIndexOfPosition(firstClickPosition),
            ];
            this.getNeighboursPositions(firstClickPosition).forEach(
                (neighbourPosition) => {
                    invalidIndexs.push(
                        this.getIndexOfPosition(neighbourPosition)
                    );
                }
            );

            const minesIndex = randomIntegers(
                this.mineCount,
                this.cellCount() - 1,
                0,
                invalidIndexs
            );

            minesIndex.forEach((mineIndex) => {
                const position: Position = this.getPositionOfIndex(mineIndex);
                this.setValue(position, new Cell(true, false));
            });
        }
    }

    getCloseMineCount(position: Position): number {
        let count: number = 0;
        this.getNeighboursPositions(position).forEach((neighbourPosition) => {
            if (this.getValue(neighbourPosition).getIsMine()) {
                count++;
            }
        });
        return count;
    }
}
