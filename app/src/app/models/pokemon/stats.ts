import { StatName } from "@smogon/calc";

export const statNames: StatName[] = [
    'hp', 'atk', 'def', 'spa', 'spd', 'spe'
];
