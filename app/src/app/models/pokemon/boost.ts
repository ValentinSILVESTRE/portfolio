export declare type Boost = {
    hp: number;
    atk: number;
    def: number;
    spa: number;
    spd: number;
    spe: number;
};
