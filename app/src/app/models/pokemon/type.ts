export declare type Type =
    | 'steel'
    | 'fighting'
    | 'dragon'
    | 'water'
    | 'electric'
    | 'fairy'
    | 'fire'
    | 'ice'
    | 'bug'
    | 'normal'
    | 'grass'
    | 'poison'
    | 'psychic'
    | 'rock'
    | 'ground'
    | 'ghost'
    | 'dark'
    | 'flying';

export const typeNames: Type[] = [
    'steel',
    'fighting',
    'dragon',
    'water',
    'electric',
    'fairy',
    'fire',
    'ice',
    'bug',
    'normal',
    'grass',
    'poison',
    'psychic',
    'rock',
    'ground',
    'ghost',
    'dark',
    'flying',
];
