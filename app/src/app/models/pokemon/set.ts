import {
    AbilityName,
    ItemName,
    MoveName,
    NatureName,
    StatsTable,
} from '@smogon/calc/dist/data/interface';
import { Tier } from './tier';

export interface Set {
    abilities: string[];
    tier: Tier;
    set: {
        level: number;
        moves: MoveName[];
        item: ItemName;
        ability: AbilityName;
        nature: NatureName;
        evs: Partial<StatsTable>;
    };
}
