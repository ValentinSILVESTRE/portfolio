export declare type Status = {
    name: string;
    fullname: string;
    desc: string;
    type: string;
    display: string;
};
