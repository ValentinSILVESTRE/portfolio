export declare type Nature = {
    label: string;
    up: string;
    down: string;
    desc: string;
};
