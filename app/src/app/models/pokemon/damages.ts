export interface Damages {
    min: number;
    max: number;
    avg: number;
    moveName: string;
    moveType: string;
}
