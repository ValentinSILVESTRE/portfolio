import {
    AbilityName,
    GenderName,
    Generation,
    ItemName,
    MoveName,
    NatureName,
    Specie,
    SpeciesName,
    StatusName,
    TypeName,
} from '@smogon/calc/dist/data/interface';
import { Pokemon as Pkmn } from '@smogon/calc/dist/pokemon';
import { StatsTable } from '@smogon/calc';

export class Pokemon {
    constructor(
        private ability: AbilityName,
        private abilityOn: boolean | undefined,
        private boosts: StatsTable,
        private evs: StatsTable,
        private gen: Generation,
        private gender: GenderName | undefined,
        private isDynamaxed: boolean | undefined,
        private isGrounded: boolean,
        private item: ItemName | undefined,
        private ivs: StatsTable,
        private level: number,
        private moves: MoveName[],
        private name: SpeciesName,
        private nature: NatureName,
        private originalCurHP: number,
        private rawStats: StatsTable,
        private species: Specie,
        private stats: StatsTable,
        private status: StatusName | '',
        private toxicCounter: number,
        private types: [TypeName] | [TypeName, TypeName],
        private weightkg: number
    ) {}

    getData() {
        const pkmn = new Pkmn(this.gen, this.name);
        pkmn.types[1] = 'Normal';
        // pkmn.types = ['Normal'];
        return pkmn;
    }

    // ! ================================================================================================
    // !                                                                                                |
    // !                                            Getters                                             |
    // !                                                                                                |
    // ! ================================================================================================

    getAbility(): AbilityName {
        return this.ability;
    }
    getAbilityOn(): boolean | undefined {
        return this.abilityOn;
    }
    getBoosts(): StatsTable {
        return this.boosts;
    }
    getEvs(): StatsTable {
        return this.evs;
    }
    getGen(): Generation {
        return this.gen;
    }
    getGender(): GenderName | undefined {
        return this.gender;
    }
    getIsDynamaxed(): boolean | undefined {
        return this.isDynamaxed;
    }
    getIsGrounded(): boolean {
        return this.isGrounded;
    }
    getItem(): ItemName | undefined {
        return this.item;
    }
    getIvs(): StatsTable {
        return this.ivs;
    }
    getLevel(): number {
        return this.level;
    }
    getMoves(): MoveName[] {
        return this.moves;
    }
    getName(): SpeciesName {
        return this.name;
    }
    getNature(): NatureName {
        return this.nature;
    }
    getOriginalCurHP(): number {
        return this.originalCurHP;
    }
    getRawStats(): StatsTable {
        return this.rawStats;
    }
    getSpecies(): Specie {
        return this.species;
    }
    getStats(): StatsTable {
        return this.stats;
    }
    getStatus(): StatusName | '' {
        return this.status;
    }
    getToxicCounter(): number {
        return this.toxicCounter;
    }
    getTypes(): [TypeName] | [TypeName, TypeName] {
        return this.types;
    }
    getWeightkg(): number {
        return this.weightkg;
    }

    // ! ================================================================================================
    // !                                                                                                |
    // !                                            Setters                                             |
    // !                                                                                                |
    // ! ================================================================================================

    setAbility(ability: AbilityName): void {
        this.ability = ability;
    }
    setAbilityOn(abilityOn: boolean | undefined): void {
        this.abilityOn = abilityOn;
    }
    setBoosts(boosts: StatsTable): void {
        this.boosts = boosts;
    }
    setEvs(evs: StatsTable): void {
        this.evs = evs;
    }
    setGen(gen: Generation): void {
        this.gen = gen;
    }
    setGender(gender: GenderName | undefined): void {
        this.gender = gender;
    }
    setIsDynamaxed(isDynamaxed: boolean | undefined): void {
        this.isDynamaxed = isDynamaxed;
    }
    setIsGrounded(isGrounded: boolean): void {
        this.isGrounded = isGrounded;
    }
    setItem(item: ItemName | undefined): void {
        this.item = item;
    }
    setIvs(ivs: StatsTable): void {
        this.ivs = ivs;
    }
    setLevel(level: number): void {
        this.level = level;
    }
    setMoves(moves: MoveName[]): void {
        this.moves = moves;
    }
    setName(name: SpeciesName): void {
        this.name = name;
    }
    setNature(nature: NatureName): void {
        this.nature = nature;
    }
    setOriginalCurHP(originalCurHP: number): void {
        this.originalCurHP = originalCurHP;
    }
    setRawStats(rawStats: StatsTable): void {
        this.rawStats = rawStats;
    }
    setSpecies(species: Specie): void {
        this.species = species;
    }
    setStats(stats: StatsTable): void {
        this.stats = stats;
    }
    setStatus(status: StatusName | ''): void {
        this.status = status;
    }
    setToxicCounter(toxicCounter: number): void {
        this.toxicCounter = toxicCounter;
    }
    setTypes(types: [TypeName] | [TypeName, TypeName]): void {
        this.types = types;
    }
    setWeightkg(weightkg: number): void {
        this.weightkg = weightkg;
    }

    // ! ================================================================================================
    // !                                                                                                |
    // !                                             Autres                                             |
    // !                                                                                                |
    // ! ================================================================================================
}
