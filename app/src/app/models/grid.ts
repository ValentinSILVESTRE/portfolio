import { Position } from './position.model';

/**
 * - Grille à deux dimensions composée de T (type indéfinit)
 */
export class grid<T> {
    /** - La gille à deux dimensions composée de T (type indéfinit) */
    private _grid!: T[][];

    constructor(
        private _length: number = 20,
        private _height: number = 20,
        private defaultValue: any
    ) {
        this.createGrid();
    }

    /**
     * - Création de la grille vide
     */
    createGrid(): void {
        const grid: T[][] = [];
        this._grid = grid;
        for (let iRow = 0; iRow < this.getHeight(); iRow++) {
            const row: T[] = [];
            grid.push(row);
            for (let iColumn = 0; iColumn < this.getLength(); iColumn++) {
                row.push(this.defaultValue);
            }
        }
    }

    /**
     * - Réinitialise la grille
     */
    resetGrid(): void {
        for (let column = 0; column < this.getHeight(); column++) {
            for (let row = 0; row < this.getLength(); row++) {
                this.setValue({ x: row, y: column }, this.defaultValue);
            }
        }
    }

    // !================================================================================================|
    // !                                                                                                |
    // !                                       Getters - Setters                                        |
    // !                                                                                                |
    // !================================================================================================|

    getLength(): number {
        return this._length;
    }
    setLength(length: number): void {
        if (length > 0) {
            this._length = Math.floor(length);
        }
    }
    getHeight(): number {
        return this._height;
    }
    setHeight(height: number): void {
        if (height > 0) {
            this._height = Math.floor(height);
        }
    }
    getGrid(): T[][] {
        return this._grid;
    }
    setGrid(grid: T[][]): void {
        this._grid = grid;
    }
    getCells(): T[] {
        const cells: T[] = [];
        for (const line of this.getGrid()) {
            cells.push(...line);
        }
        return cells;
    }

    /**
     * - Nombre de cellules dans la grille
     */
    cellCount(): number {
        return this.getLength() * this.getHeight();
    }

    getValue(position: Position): T {
        if (this.positionIsValid(position)) {
            return this._grid[position.y][position.x];
        }
        throw new RangeError(
            `getValue(${position.x}, ${position.y}) est impossible !`
        );
    }

    setValue(position: Position, value: T): void {
        if (this.positionIsValid(position)) {
            this._grid[position.y][position.x] = value;
        }
    }

    // ! ===============================================================================================|
    // !                                                                                                |
    // !                                             Autres                                             |
    // !                                                                                                |
    // ! ===============================================================================================|

    /**
     * - Return true if the position is valid
     * @param position
     */
    positionIsValid(position: Position): boolean {
        return (
            0 <= position.x &&
            position.x < this.getLength() &&
            0 <= position.y &&
            position.y < this.getHeight()
        );
    }

    getPositionOfIndex(index: number): Position {
        return {
            x: index % this.getLength(),
            y: Math.floor(index / this.getLength()),
        };
    }

    getIndexOfPosition(position: Position): number {
        return position.x + this.getLength() * position.y;
    }

    /**
     * - Les positions valides de ses voisins
     * @param position - Position centrale
     */
    getNeighboursPositions(position: Position): Position[] {
        const neighboursPositions: Position[] = [];

        if (this.positionIsValid(position)) {
            for (let x = position.x - 1; x <= position.x + 1; x++) {
                for (let y = position.y - 1; y <= position.y + 1; y++) {
                    const neighbourPosition: Position = {
                        x: x,
                        y: y,
                    };
                    if (this.positionIsValid(neighbourPosition)) {
                        neighboursPositions.push(neighbourPosition);
                    }
                }
            }
        }

        return neighboursPositions.filter(
            (_position) =>
                !(_position.x === position.x && _position.y === position.y)
        );
    }
}
