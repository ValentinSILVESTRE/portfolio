import { grid } from '../grid';
import { Position } from '../position.model';

export class GridTaquin extends grid<number> {
    finish: boolean = false;

    /**
     * @param {number} size - La hauteur et largeure de la grille
     */
    constructor(private size: number = 3) {
        super(size, size, 0);
    }

    /**
     * - Renvoie un tableau à deux dimensions des entiers de 0 à size**2 - 1 triés
     * @returns {number[][]}
     */
    getInitGrid(): number[][] {
        const grid: number[][] = [];
        for (let iRow = 0; iRow < this.size; iRow++) {
            const line: number[] = [];
            for (let iColumn = 0; iColumn < this.size; iColumn++) {
                line.push(this.size * iRow + iColumn);
            }
            grid.push(line);
        }
        return grid;
    }

    /**
     * - Renvoie vrai si aucune cases adjacentes ne doit l'être au final
     */
    isPerfectlyMixed(): boolean {
        // Ligne
        for (let y = 0; y < this.size; y++) {
            for (let x = 0; x < this.size - 1; x++) {
                const caseValue = this.getValue({ y: y, x: x });
                const rightValue = this.getValue({
                    y: y,
                    x: x + 1,
                });
                if (
                    caseValue + 1 === rightValue &&
                    rightValue % this.size !== 0
                ) {
                    return false;
                }
            }
        }

        // Colonne
        for (let y = 0; y < this.size - 1; y++) {
            for (let x = 0; x < this.size; x++) {
                const caseValue = this.getValue({ y: y, x: x });
                const bottomValue = this.getValue({
                    y: y + 1,
                    x: x,
                });
                if (caseValue + this.size === bottomValue) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * - Fonction qui mélange la grille, en laissant le dernier élément à la fin
     */
    mix() {
        if (this.size === 2) {
            const randomBoolean = Boolean(Math.floor(Math.random() * 2));

            if (randomBoolean) {
                this.setGrid([
                    [1, 2],
                    [0, 3],
                ]);
            } else {
                this.setGrid([
                    [2, 0],
                    [1, 3],
                ]);
            }
        } else {
            // On mélange tant que la grille n'est pas solvable ou est déjà finie
            do {
                /** @type {number[]} - Liste triée des entiers de 0 à n*n - 1 */
                let integers: number[] = [];
                for (let i = 0; i < this.size ** 2 - 1; i++) {
                    integers.push(i);
                }

                /** @type {number[]} - Liste mélangées des entiers */
                const unorderedIntegers: number[] = [];

                // Tant que la liste triée n'est pas vide, on sors un entier aléatoirement qu'on met à la fin de la liste mélangée
                while (integers.length > 0) {
                    const i: number = Math.floor(
                        Math.random() * integers.length
                    );
                    unorderedIntegers.push(integers.splice(i, 1)[0]);
                }

                // On met le dernier entier à la fin
                unorderedIntegers.push(this.size ** 2 - 1);

                // On place les élément mélangés dans la grille
                for (let iRow = 0; iRow < this.size; iRow++) {
                    for (let iColumn = 0; iColumn < this.size; iColumn++) {
                        const position: Position = {
                            y: iRow,
                            x: iColumn,
                        };
                        this.setValue(
                            position,
                            unorderedIntegers[iRow * this.size + iColumn]
                        );
                    }
                }
            } while (
                !this.isSolvable() ||
                this.isFinish() ||
                !this.isPerfectlyMixed()
            );
        }
    }

    /**
     * - Renvoie vrai si la grille est totalement triée, faux sinon
     * @returns {Boolean}
     */
    isFinish(): boolean {
        for (let y = 0; y < this.size; y++) {
            for (let x = 0; x < this.size; x++) {
                const position: Position = { y: y, x: x };
                if (y * this.size + x !== this.getValue(position)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * - Renvoie vrai si la grille est solvable, son niveau de mélange est pair
     * @returns {Boolean}
     */
    isSolvable(): boolean {
        return this.mixinLevel() % 2 === 0;
    }

    /**
     *  - Renvoie le niveau de mélange de la grille (nombre de paires inversées)
     * @returns {number}
     */
    mixinLevel(): number {
        let res = 0;
        // Pour chaque paire d'éléments (i,j) inversés on ajoute 1 au résultat
        for (let i = 0; i < this.size ** 2 - 1; i++) {
            for (let j = i + 1; j < this.size ** 2; j++) {
                if (!this.sontOrdonnees(i, j)) {
                    res++;
                }
            }
        }
        return res;
    }

    /**
     *  - renvoie vrai si i et j sont dans le bon ordre, sinon faux
     * @param {number} m
     * @param {number} n
     * @returns {boolean} renvoie vrai si i et j sont dans le bon ordre, sinon faux
     */
    sontOrdonnees(m: number, n: number): boolean {
        if (0 <= m && m < this.size ** 2 && 0 <= n && n < this.size ** 2) {
            if (n < m) {
                return this.sontOrdonnees(n, m);
            }
            // ici i <= j
            return this.pos(m)! <= this.pos(n)!;
        }
        return false;
    }

    /**
     * - Renvoie la position de i dans la grille (entre 0 et n²-1)
     * @param {number} i - valeur recherchée dans la grille (entre 0 et n²-1)
     * @returns {number} position de i dans la grille (entre 0 et n²-1)
     */
    pos(i: number): number | null {
        for (let iRow = 0; iRow < this.size; iRow++) {
            for (let iColumn = 0; iColumn < this.size; iColumn++) {
                const position: Position = { y: iRow, x: iColumn };
                if (this.getValue(position) === i) {
                    return iRow * this.size + iColumn;
                }
            }
        }
        return null;
    }

    /**
     * - Échange les éléments de place dans la grille
     * @param {Position} positionA
     * @param {Position} positionB
     */
    switch(positionA: Position, positionB: Position): void {
        const valueA = this.getValue(positionA);
        this.setValue(positionA, this.getValue(positionB));
        this.setValue(positionB, valueA);
    }

    /**
     * - Renvoie vrai si la position est valide (row et column compris entre 0 et size ** 2 - 1), faux sinon
     * @param {Position} position
     */
    valueIsValid(value: number): boolean {
        return 0 <= value && value < this.size ** 2;
    }

    /**
     * - Renvoie la valeur de la case vide (size**2 - 1)
     */
    getEmptyCaseValue(): number {
        return this.size ** 2 - 1;
    }

    /**
     * - Renvoie la position de la case (vide par défaut) recherchée
     * @param {number} value - Valeur recherchée, par défaut la case vide (8 pour une grille 3×3)
     */
    getPosition(value: number = this.getEmptyCaseValue()): Position | null {
        for (let iRow = 0; iRow < this.size; iRow++) {
            for (let iColumn = 0; iColumn < this.size; iColumn++) {
                const position: Position = { y: iRow, x: iColumn };
                if (this.getValue(position) === value) {
                    return position;
                }
            }
        }
        return null;
    }

    /**
     * - Déplace la case vide vers la direction demandée
     * @param {string} direction - up, down, left or right
     */
    move(direction: 'up' | 'down' | 'right' | 'left') {
        /** @type {Position} - Position de la case vide */
        const emptyCasePosition: Position = this.getPosition()!;

        switch (direction) {
            case 'up':
                if (this.getPosition()!.y > 0) {
                    this.switch(emptyCasePosition, {
                        y: emptyCasePosition.y - 1,
                        x: emptyCasePosition.x,
                    });
                }
                break;

            case 'down':
                if (this.getPosition()!.y < this.size - 1) {
                    this.switch(emptyCasePosition, {
                        y: emptyCasePosition.y + 1,
                        x: emptyCasePosition.x,
                    });
                }
                break;

            case 'left':
                if (this.getPosition()!.x > 0) {
                    this.switch(emptyCasePosition, {
                        y: emptyCasePosition.y,
                        x: emptyCasePosition.x - 1,
                    });
                }
                break;

            case 'right':
                if (this.getPosition()!.x < this.size - 1) {
                    this.switch(emptyCasePosition, {
                        y: emptyCasePosition.y,
                        x: emptyCasePosition.x + 1,
                    });
                }
                break;

            default:
                console.error(`Impossible de faire move(${direction})`);
                break;
        }

        // Traitement de fin de partie
        if (this.isFinish()) {
            this.finish = true;
        }
    }

    /** - Déplace a cellule vers la case vide si c'est possible */
    moveCell(number: number): void {
        const cellPosition = this.getPosition(number)!;
        const emptyPosition = this.getPosition()!;

        // S'il ne s'agit pas de la case vide
        if (number !== this.size ** 2 - 1) {
            // Si la case est sur la même ligne
            if (cellPosition.y === emptyPosition.y) {
                // Si la case est à droite de la case vide
                if (emptyPosition.x < cellPosition.x) {
                    // On échange la case vide avec toutes celles entre les deux, jusqu'à la case
                    for (
                        let iColumn = emptyPosition.x + 1;
                        iColumn <= cellPosition.x;
                        iColumn++
                    ) {
                        const position = {
                            y: emptyPosition.y,
                            x: iColumn,
                        };
                        this.switch(this.getPosition()!, position);
                    }
                }

                // Si la case est à gauche de la case vide
                else {
                    // On échange la case vide avec toutes celles entre les deux, jusqu'à la case
                    for (
                        let iColumn = emptyPosition.x - 1;
                        iColumn >= cellPosition.x;
                        iColumn--
                    ) {
                        const position = {
                            y: emptyPosition.y,
                            x: iColumn,
                        };
                        this.switch(this.getPosition()!, position);
                    }
                }
            }

            // Si la case est sur la même colonne
            else if (cellPosition.x === emptyPosition.x) {
                // Si la case est en dessous de la case vide
                if (emptyPosition.y < cellPosition.y) {
                    // On échange la case vide avec toutes celles entre les deux, jusqu'à la case
                    for (
                        let iRow = emptyPosition.y + 1;
                        iRow <= cellPosition.y;
                        iRow++
                    ) {
                        const position = {
                            y: iRow,
                            x: emptyPosition.x,
                        };
                        this.switch(this.getPosition()!, position);
                    }
                }

                // Si la case est au dessus de la case vide
                else {
                    // On échange la case vide avec toutes celles entre les deux, jusqu'à la case
                    for (
                        let iRow = emptyPosition.y - 1;
                        iRow >= cellPosition.y;
                        iRow--
                    ) {
                        const position = {
                            y: iRow,
                            x: emptyPosition.x,
                        };
                        this.switch(this.getPosition()!, position);
                    }
                }
            }

            // Traitement de fin de partie
            this.finish = this.isFinish();
        }
    }
}
