import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventComponent } from './components/calendar/event/event.component';
import { MainCalendarComponent } from './components/calendar/main-calendar/main-calendar.component';
import { Connect4Component } from './components/connect4/connect4/connect4.component';
import { BoardComponent as GameOfLifeBoardComponent } from './components/gameoflife/board/board.component';
import { HomepageComponent } from './components/home/homepage/homepage.component';
import { MinesBoardComponent } from './components/mines/mines-board/mines-board.component';
import { MondrianComponent } from './components/mondrian/mondrian.component';
import { FightComponent } from './components/pokemon/fight/fight.component';
import { FormComponent } from './components/pokemon/form/form.component';
import { TaquinComponent } from './components/taquin/taquin.component';
import { EventPostComponent } from './components/calendar/event-post/event-post.component';
import { TestComponent } from './test/test.component';
import { EventUpdateComponent } from './components/calendar/event-update/event-update.component';
import { LinkPostComponent } from './components/calendar/link-post/link-post.component';
import { SearchComponent } from './components/calendar/search/search.component';
import { MainComponent } from './components/cryptography/main/main.component';
import { LoadingComponent } from './components/loading/loading.component';
import { CvComponent } from './components/cv/cv.component';
import { TableComponent } from './components/pokemon/table/table.component';
import { LoginComponent } from './components/login/login.component';
import { ImageChoiceComponent } from './components/taquin/image-choice/image-choice.component';

const routes: Routes = [
    {
        path: '',
        component: HomepageComponent,
        // component: FightComponent,
    },
    {
        path: 'calendar/search',
        component: SearchComponent,
    },
    {
        path: 'calendar/event/new',
        component: EventPostComponent,
    },
    {
        path: 'calendar/event/:id/edit',
        component: EventUpdateComponent,
    },
    {
        path: 'calendar/event/:id',
        component: EventComponent,
    },
    {
        path: 'calendar/link/new',
        component: LinkPostComponent,
    },
    {
        path: 'calendar',
        component: MainCalendarComponent,
    },
    {
        path: 'fight',
        component: FightComponent,
    },
    {
        path: 'form',
        component: FormComponent,
    },
    {
        path: 'gameoflife',
        component: GameOfLifeBoardComponent,
    },
    {
        path: 'mines',
        component: MinesBoardComponent,
    },
    {
        path: 'mondrian',
        component: MondrianComponent,
    },
    {
        path: 'puissance4',
        component: Connect4Component,
    },
    {
        path: 'taquin',
        component: TaquinComponent,
    },
    {
        path: 'taquin/choice',
        component: ImageChoiceComponent,
    },
    {
        path: 'test',
        component: TestComponent,
    },
    {
        path: 'cryptography',
        component: MainComponent,
    },
    {
        path: 'loading',
        component: LoadingComponent,
    },
    {
        path: 'cv',
        component: CvComponent,
    },
    {
        path: 'pokemon/table',
        component: TableComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
