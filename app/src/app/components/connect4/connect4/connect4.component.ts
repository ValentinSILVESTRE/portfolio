import { Component, OnInit } from '@angular/core';
import { Board } from 'src/app/models/connect4/board.model';
import { Color } from 'src/app/models/connect4/color';
import { Position } from 'src/app/models/position.model';

@Component({
    selector: 'app-connect4',
    templateUrl: './connect4.component.html',
    styleUrls: ['./connect4.component.scss', './input.component.scss'],
})
export class Connect4Component implements OnInit {
    private board: Board = new Board();
    currentPlayer: Color = Color.red;
    botColor: Color = Color.yellow;
    playingVsBot: boolean = true;
    botIsPlaying: boolean = false;
    winningIndexs: number[] = [];
    winner?: Color;

    constructor() {}

    ngOnInit(): void {}

    onNewGame(): void {
        this.currentPlayer = Color.red;
        this.board.reset();
        this.winner = undefined;
        this.winningIndexs = [];
    }

    onChangeOpponentType(): void {
        this.playingVsBot = !this.playingVsBot;

        // Si c'est au bot de jouer, alors il joue
        if (this.currentPlayer === this.botColor) {
            this.board.botPlay(this.currentPlayer);
            let winner: { color: Color; positions: Position[] } | null =
                this.board.getWinningData();
            winner = this.board.getWinningData();
            if (winner) {
                this.winner = winner.color;
                winner.positions.forEach((position) => {
                    this.winningIndexs.push(
                        position.y * this.board.getLength() + position.x
                    );
                });
            }
            this.switchPlayer();
        }
    }

    getBoard(): Board {
        return this.board;
    }

    /**
     * - On change le joueur
     */
    switchPlayer(): void {
        this.currentPlayer =
            this.currentPlayer === Color.red ? Color.yellow : Color.red;
    }

    /**
     * - Met la couleur dans la position et passe au joueur suivant si la case est vide
     * @param position - La position jouée
     */
    onPlay(cellIndex: number): void {
        /** Position choisie */
        const position: Position = {
            x: cellIndex % this.getBoard().getLength(),
            y: Math.floor(cellIndex / this.getBoard().getLength()),
        };

        // Cas où on peut cliquer sur cette case
        if (
            !this.board.columnIsFull(position.x) &&
            !this.board.getWinner() &&
            !this.botIsPlaying
        ) {
            this.board.play(position.x, this.currentPlayer);
            this.switchPlayer();

            let winner: { color: Color; positions: Position[] } | null =
                this.board.getWinningData();
            if (winner) {
                this.winner = winner.color;
                winner.positions.forEach((position) => {
                    this.winningIndexs.push(
                        position.y * this.board.getLength() + position.x
                    );
                });
            }

            // Si on affronte l'IA, alors elle joue
            if (this.playingVsBot) {
                this.botIsPlaying = true;
                // On simule un délai de réflexion
                setTimeout(() => {
                    this.board.botPlay(this.currentPlayer);
                    winner = this.board.getWinningData();
                    if (winner) {
                        this.winner = winner.color;
                        winner.positions.forEach((position) => {
                            this.winningIndexs.push(
                                position.y * this.board.getLength() + position.x
                            );
                        });
                    }
                    this.switchPlayer();
                    this.botIsPlaying = false;
                }, 1000);
            }
        }
    }

    // ! ==========================           BOT          ==============================================
}
