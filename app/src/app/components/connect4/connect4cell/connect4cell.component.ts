import { Component, Input } from '@angular/core';
import { Color } from 'src/app/models/connect4/color';

@Component({
    selector: 'app-connect4cell',
    templateUrl: './connect4cell.component.html',
    styleUrls: ['./connect4cell.component.scss'],
})
export class Connect4cellComponent {
    @Input() color!: Color;
    @Input() win!: boolean;
}
