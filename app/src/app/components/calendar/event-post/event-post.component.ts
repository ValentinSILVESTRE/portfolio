import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/calendar/category.model';
import { Evenement } from 'src/app/models/calendar/evenement.model';
import { Link } from 'src/app/models/calendar/link.model';
import { LinkType } from 'src/app/models/calendar/linkType.model';
import { CategoryService } from 'src/app/services/category.service';
import { EventService } from 'src/app/services/event.service';
import { LinkTypeService } from 'src/app/services/link-type.service';
import { LinkService } from 'src/app/services/link.service';

@Component({
    selector: 'app-event-post',
    templateUrl: './event-post.component.html',
    styleUrls: ['./event-post.component.scss'],
})
export class EventPostComponent implements OnInit {
    titleControl = new FormControl('');
    descriptionControl = new FormControl('');
    parentControl = new FormControl('');
    categoryControl = new FormControl('');
    now = new Date();
    nowString = `${this.now.getFullYear().toString().padStart(4, '0')}-${(
        this.now.getMonth() + 1
    )
        .toString()
        .padStart(2, '0')}-${this.now
        .getDate()
        .toString()
        .padStart(2, '0')}T${this.now
        .getHours()
        .toString()
        .padStart(2, '0')}:${this.now
        .getMinutes()
        .toString()
        .padStart(2, '0')}`;
    dateControl = new FormControl(this.nowString);
    parents: Evenement[] = [
        {
            id: 0,
            title: '',
            date: this.now,
            categories: [],
            subEvents: [],
            links: [],
        },
    ];
    categories: Category[] = [
        {
            id: 0,
            name: '',
        },
    ];
    links: Link[] = [];
    linkTypes: LinkType[] = [];
    linkControls: FormControl[][] = [];
    parentEventId?: number;

    constructor(
        private eventService: EventService,
        private categoryService: CategoryService,
        private linkService: LinkService,
        private linkTypeService: LinkTypeService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.getEvents();
        this.getCategories();
        this.getLinkType();

        this.route.queryParams.subscribe((params) => {
            // Si la route contient le paramètre parentEvent et que celui-ci est un nombre alors on l'enregistre
            if (
                (params['parentEvent'] || []).length &&
                !isNaN(+params['parentEvent'])
            ) {
                this.parentEventId = +params['parentEvent'];
                this.parentControl = new FormControl(this.parentEventId);
            }
        });
    }

    addLinkControl(title: string, url: string, linkType: number) {
        const linkControl: FormControl[] = [];
        linkControl.push(
            new FormControl(url),
            new FormControl(title),
            new FormControl(linkType)
        );
        this.linkControls.push(linkControl);
    }

    /**
     * - On supprime le liens de la liste, mais pas des liens de l'événement, pour cela il faut valider en cliquant sur mettre à jour
     */
    onDeleteLink(linkNumber: number): void {
        this.links.splice(linkNumber, 1)[0];
        this.linkControls.splice(linkNumber, 1);
    }

    getEvents(): void {
        const listSubscription = this.eventService
            .getAll()
            .subscribe((events) => {
                events.forEach((event) => this.parents.push(event));
                listSubscription.unsubscribe();

                // On trie les événements par noms croissants
                this.parents.sort((a, b) =>
                    b.title.toUpperCase() < a.title.toUpperCase() ? 1 : -1
                );
            });
    }

    getCategories(): void {
        const listSubscription = this.categoryService
            .getAll()
            .subscribe((categories) => {
                categories.forEach((category) =>
                    this.categories.push(category)
                );
                listSubscription.unsubscribe();

                // On trie les catégories par titres croissants
                this.categories.sort((a, b) =>
                    b.name.toUpperCase() < a.name.toUpperCase() ? 1 : -1
                );
            });
    }

    getLinkType(): void {
        const subscription = this.linkTypeService
            .getAll()
            .subscribe((_linkTypes) => {
                _linkTypes.forEach((linkType) => this.linkTypes.push(linkType));
                subscription.unsubscribe();
            });
    }

    onAddLink(): void {
        const newLink: Link = {
            event: this.eventService.getEventById(0)!,
            id: 0,
            title: '',
            type: this.linkTypeService.getById(1),
            url: '',
        };
        this.links.push(newLink);
        this.addLinkControl(newLink.title, newLink.url, newLink.type.id);
    }

    /**
     * - On créé un nouvel évenement
     * @param form - Le formulaire
     */
    onSubmit(form: NgForm): void {
        // * - On créé le nouvel évenement à partir du formulaire
        const newEvent: Evenement = {
            id: 0,
            title: this.titleControl.value,
            description: this.descriptionControl.value,
            date: this.dateControl.value,
            categories: this.categoryControl.value || [],
            subEvents: [],
            links: [],
        };

        if (this.parentControl.value) {
            newEvent.parentEventId = +this.parentControl.value;
        }

        // * - On l'envoie dans une requête POST
        const subscription = this.eventService
            .post(newEvent)
            .subscribe((createdEvent) => {
                // * - On traite les liens
                this.links.forEach((link, i) => {
                    link.event = createdEvent;
                    link.url = this.linkControls[i][0].value;
                    link.title = this.linkControls[i][1].value;
                    link.type = this.linkTypeService.getById(
                        +this.linkControls[i][2].value as 1 | 2 | 3
                    );
                    this.linkService.post(link).subscribe();
                });

                subscription.unsubscribe();

                // * - On redirige à l'acceuil
                this.router.navigate(['/calendar']);
            });
    }

    getEventsByCategory(categoryName: string): Evenement[] {
        const list: Evenement[] = this.parents.filter((event) =>
            event.categories.find((category) => category.name === categoryName)
        );

        // On ajoute les enfants qui héritent de la catégorie
        const listWithDescent = list
            .map((e) => this.eventService.getFamily(e))
            .flat();

        // On supprime les doublons
        let i = 0;
        while (i < listWithDescent.length) {
            const event = listWithDescent[i];
            let j = 0;
            while (j < i && listWithDescent[j].id !== event.id) {
                j++;
            }
            if (j < i) {
                listWithDescent.splice(j, 1);
            } else {
                i++;
            }
        }

        // On renvoie la liste trié par titres croissants
        return listWithDescent.sort((a, b) =>
            a.title.toUpperCase() < b.title.toUpperCase() ? -1 : 1
        );
    }
}
