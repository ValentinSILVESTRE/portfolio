import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/calendar/category.model';
import { Evenement } from 'src/app/models/calendar/evenement.model';
import { Link } from 'src/app/models/calendar/link.model';
import { CategoryService } from 'src/app/services/category.service';
import { EventService } from 'src/app/services/event.service';
import { LinkTypeService } from 'src/app/services/link-type.service';
import {
    areSameDate,
    areSameWeek,
    getStringDifference,
    isNow,
    maxDistance1Day,
} from 'src/assets/functions/date.functions';
import { playSound } from 'src/assets/functions/sound.functions';

@Component({
    selector: 'app-search',
    templateUrl: '../main-calendar/main-calendar.component.html',
    styleUrls: ['../main-calendar/main-calendar.component.scss'],
})
export class SearchComponent implements OnInit {
    events: Evenement[] = [];
    screenSize!: 'small' | 'medium' | 'large';
    dateFormat: 'absolute' | 'relative' = 'relative';
    nextEvents: Evenement[] = [];
    loading = true;
    found = false;
    search = true;
    queryControl = new FormControl('');
    query: string = '';
    eventsToDisplay: Evenement[] = [];

    constructor(
        private eventService: EventService,
        private linkTypeService: LinkTypeService,
        private categoryService: CategoryService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.setScreenSize();

        this.route.queryParams.subscribe((params) => {
            // Si la route ne contient pas le paramètre query ou que celui-ci est vide alors on redirigige à l'acceuil du calendrier
            if (!(params['query'] && params['query'].trim().length)) {
                this.router.navigate(['/calendar']);
            }

            // On récupère la recherche
            this.query = params['query'].trim();
            // On récupère uniquement les événements correspondants
            this.getEvents();
        });
    }

    _playSound(event: Evenement): void {
        setInterval(() => {
            if (isNow(new Date(event.date))) {
                playSound('assets/sounds/super_max.mp4', 16.5);
            }
        }, 1000);
    }

    getEventsByTitle(query: string): void {
        const listSubscription = this.eventService
            .getEventsByTitle(query)
            .subscribe({
                next: (data) => {
                    this.eventsToDisplay = data.sort((a, b) =>
                        b.date < a.date ? 1 : -1
                    );
                },
                error: (error) => {
                    this.loading = false;
                    listSubscription.unsubscribe();
                },
                complete: () => {
                    this.loading = false;
                    this.found = true;
                    listSubscription.unsubscribe();
                },
            });
    }

    getEvent(id: number): Evenement | null {
        const event = this.events.filter((e) => e.id == id)[0];
        return event || null;
    }

    stopPropagation(event: Event) {
        event.stopPropagation();
    }

    getEventCategories(event: Evenement): Category[] {
        // Si l'événement n'a pas de parent, alors on renvoie ses categories
        if (!event.parentEventId) return event.categories;

        const categories: Category[] = [
            ...event.categories,
            ...this.getEventCategories(this.getEvent(event.parentEventId)!),
        ];

        const uniqueCategories: Category[] = [];

        for (const category of categories) {
            // On cherche s'il y a category dans uniqueCategories
            let i = 0;
            while (
                i < uniqueCategories.length &&
                uniqueCategories[i].id !== category.id
            ) {
                i++;
            }
            // Si on est arrivé au bout de la liste, c'est qu'elle n'y est pas
            if (i === uniqueCategories.length) {
                uniqueCategories.push(category);
            }
        }

        return uniqueCategories;
    }

    getEvents(): void {
        const listSubscription = this.eventService.getAll().subscribe({
            next: (data) => {
                this.events = data;
                const now = new Date();
                const today = new Date(
                    now.getFullYear(),
                    now.getMonth(),
                    now.getDate()
                );
                const nextEvents = (this.eventsToDisplay = data
                    .sort((a, b) => (b.date < a.date ? 1 : -1))
                    .filter((event) => new Date(event.date) >= today));
                this.eventsToDisplay = nextEvents.filter(
                    (event) => !event.subEvents.length
                );

                // On joue le son quand un event commence
                for (const event of this.events) {
                    this._playSound(event);
                }
            },
            error: (error) => {
                this.loading = false;
                listSubscription.unsubscribe();
            },
            complete: () => {
                this.loading = false;
                this.found = true;
                listSubscription.unsubscribe();
                this.getEventsByTitle(this.query);
            },
        });
    }

    toggleDateFormat(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.dateFormat == 'absolute') this.dateFormat = 'relative';
        else this.dateFormat = 'absolute';
    }

    setScreenSize(): void {
        if (window.innerWidth < 600) this.screenSize = 'small';
        else if (window.innerWidth < 1200) this.screenSize = 'medium';
        else this.screenSize = 'large';
    }

    onResize(): void {
        this.setScreenSize();
    }

    onDelete(eventId: number, event: Event): void {
        event.preventDefault();
        event.stopPropagation();

        this.nextEvents = this.nextEvents.filter(
            (event) => event.id !== eventId
        );

        const deleteSubscription = this.eventService.delete(eventId).subscribe({
            next: () => {
                this.getEvents();
            },
            error: (error) => {
                console.error(
                    `Erreur dans onDelete(${eventId}) de main-calendar.component.ts`,
                    error
                );
                deleteSubscription.unsubscribe();
            },
            complete: () => {
                deleteSubscription.unsubscribe();
            },
        });
    }

    getMaterialIconsName(linkTypeName: string) {
        return this.linkTypeService.getMaterialIconsName(linkTypeName);
    }

    relativeDate(event: Evenement) {
        return getStringDifference(new Date(), new Date(event.date));
    }

    getAncestors(event: Evenement): Evenement[] {
        return this.eventService.getAncestors(event);
    }

    /**
     * - Renvoie les liens de l'événement et de ses parents réccursivement
     * @param event
     * @returns
     */
    getLinks(event: Evenement): Link[] {
        // Si l'événement n'a pas de parent, alors on renvoie ses liens
        if (!event.parentEventId) return event.links;
        // Sinon
        return [
            ...this.getLinks(this.getEvent(event.parentEventId)!),
            ...event.links,
        ];
    }

    getCategoryIcon(categoryName: string) {
        return this.categoryService.getIcon(categoryName);
    }

    getCategorySource(categoryName: string) {
        return this.categoryService.getImageSource(categoryName);
    }

    iconIsImage(categoryName: string): boolean {
        return this.categoryService.iconIsImage(categoryName);
    }

    isPassed(event: Evenement): boolean {
        return new Date(event.date) < new Date();
    }

    isToday(event: Evenement) {
        return areSameDate(new Date(event.date), new Date());
    }

    isTomorrow(event: Evenement) {
        return maxDistance1Day(new Date(event.date), new Date());
    }

    isThisWeek(event: Evenement) {
        return areSameWeek(new Date(event.date), new Date());
    }
}
