import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/calendar/category.model';
import { Evenement } from 'src/app/models/calendar/evenement.model';
import { Link } from 'src/app/models/calendar/link.model';
import { LinkType } from 'src/app/models/calendar/linkType.model';
import { CategoryService } from 'src/app/services/category.service';
import { EventService } from 'src/app/services/event.service';
import { LinkTypeService } from 'src/app/services/link-type.service';
import { LinkService } from 'src/app/services/link.service';

@Component({
    selector: 'app-event-update',
    templateUrl: './event-update.component.html',
    styleUrls: ['./event-update.component.scss'],
})
export class EventUpdateComponent implements OnInit {
    titleControl?: FormControl;
    descriptionControl?: FormControl;
    parentControl?: FormControl;
    now = new Date();
    dateControl?: FormControl;
    parents: Evenement[] = [
        {
            id: 0,
            title: '',
            date: new Date(),
            categories: [],
            subEvents: [],
            links: [],
        },
    ];
    event?: Evenement;
    id!: number;
    categoryControl!: FormControl;
    categories: Category[] = [
        {
            id: 0,
            name: '',
        },
    ];
    checkedCategoriesCount!: number;
    links: Link[] = [];
    linkTypes: LinkType[] = [];
    linkControls: FormControl[][] = [];
    linksToDelete: number[] = [];
    loading = true;
    found = false;

    constructor(
        private eventService: EventService,
        private router: Router,
        private categoryService: CategoryService,
        private linkTypeService: LinkTypeService,
        private linkService: LinkService,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.getEvents();
        this.getCategories();
        this.getLinkType();
        const routeParams = this.route.snapshot.paramMap;
        this.id = +routeParams.get('id')!;
        const getSubscription = this.eventService.get(this.id).subscribe({
            next: (event) => {
                this.event = event;
                const categoriesId: any[] = [];
                event.categories.forEach((category) => {
                    categoriesId.push(category.id.toString());
                });
                this.titleControl = new FormControl(event.title);
                this.descriptionControl = new FormControl(event.description);
                this.parentControl = new FormControl(event.parentEventId);
                this.categoryControl = new FormControl(categoriesId);
                this.setCheckedCategoriesCount();
                this.dateControl = new FormControl(
                    event.date.toString().slice(0, 16)
                );
                this.links = event.links;
                this.links.map((link) => (link.event = event));

                for (const link of this.links) {
                    this.addLinkControl(link.title, link.url, link.type.id);
                }
            },
            complete: () => {
                this.loading = false;
                this.found = true;
                getSubscription.unsubscribe();
            },
            error: (error) => {
                this.loading = false;
                getSubscription.unsubscribe();
            },
        });
    }

    addLinkControl(title: string, url: string, linkType: number) {
        const linkControl: FormControl[] = [];
        linkControl.push(
            new FormControl(url),
            new FormControl(title),
            new FormControl(linkType)
        );
        this.linkControls.push(linkControl);
    }

    setCheckedCategoriesCount(): void {
        if (this.categoryControl) {
            this.checkedCategoriesCount = this.categoryControl.value.length;
        }
    }

    getEvents(): void {
        const subscription = this.eventService.getAll().subscribe((events) => {
            events.forEach((event) => this.parents.push(event));
            subscription.unsubscribe();

            // On trie les événements par noms croissants
            this.parents.sort((a, b) =>
                b.title.toUpperCase() < a.title.toUpperCase() ? 1 : -1
            );
        });
    }

    getCategories(): void {
        const subscription2 = this.categoryService
            .getAll()
            .subscribe((categories) => {
                categories.forEach((category) =>
                    this.categories.push(category)
                );
                subscription2.unsubscribe();

                // On trie les catégories par titres croissants
                this.categories.sort((a, b) =>
                    b.name.toUpperCase() < a.name.toUpperCase() ? 1 : -1
                );
            });
    }

    /**
     * - On créé un nouvel évenement
     * @param form - Le formulaire
     */
    onSubmit(form: NgForm): void {
        // * - On créé le nouvel évenement à partir du formulaire
        const updatedEvent: Evenement = {
            id: this.id,
            title: this.titleControl!.value,
            description: this.descriptionControl!.value,
            date: this.dateControl!.value,
            categories: this.categoryControl.value || [],
            subEvents: [],
            links: [],
        };

        if (this.parentControl?.value) {
            updatedEvent.parentEventId = +this.parentControl.value;
        }

        // * - On supprime les liens à supprimer
        for (const linkId of this.linksToDelete) {
            this.linkService.delete(linkId).subscribe();
        }

        const updateLinks = this.links.filter((link) => link.id !== 0);
        updateLinks.forEach((link, i) => {
            link.url = this.linkControls[i][0].value;
            link.title = this.linkControls[i][1].value;
            link.type = this.linkTypeService.getById(
                +this.linkControls[i][2].value as 1 | 2 | 3
            );
            this.linkService.update(link).subscribe();
        });

        const newLinks = this.links.filter((link) => link.id === 0);
        newLinks.forEach((link, i) => {
            link.url = this.linkControls[i + updateLinks.length][0].value;
            link.title = this.linkControls[i + updateLinks.length][1].value;
            link.type = this.linkTypeService.getById(
                +this.linkControls[i + updateLinks.length][2].value as 1 | 2 | 3
            );
            this.linkService.post(link).subscribe();
        });

        // * - On l'envoie dans une requête POST
        this.eventService.update(updatedEvent).subscribe((_) => {
            // * - On redirige à l'acceuil
            this.router.navigate(['/calendar']);
        });
    }

    /**
     * - On supprime le liens de la liste, mais pas des liens de l'événement, pour cela il faut valider en cliquant sur mettre à jour
     */
    onDeleteLink(linkNumber: number): void {
        const linkToDelete = this.links.splice(linkNumber, 1)[0];
        this.linkControls.splice(linkNumber, 1);
        this.linksToDelete.push(linkToDelete.id);
    }

    getLinkType(): void {
        const subscription = this.linkTypeService
            .getAll()
            .subscribe((_linkTypes) => {
                _linkTypes.forEach((linkType) => this.linkTypes.push(linkType));
                subscription.unsubscribe();
            });
    }

    getEventsByCategory(categoryName: string): Evenement[] {
        const list: Evenement[] = this.parents.filter((event) =>
            event.categories.find((category) => category.name === categoryName)
        );

        // On ajoute les enfants qui héritent de la catégorie
        const listWithDescent = list
            .map((e) => this.eventService.getFamily(e))
            .flat();

        // On supprime les doublons
        let i = 0;
        while (i < listWithDescent.length) {
            const event = listWithDescent[i];
            let j = 0;
            while (j < i && listWithDescent[j].id !== event.id) {
                j++;
            }
            if (j < i) {
                listWithDescent.splice(j, 1);
            } else {
                i++;
            }
        }

        // On renvoie la liste trié par titres croissants
        return listWithDescent.sort((a, b) =>
            a.title.toUpperCase() < b.title.toUpperCase() ? -1 : 1
        );
    }

    onAddLink(): void {
        const newLink: Link = {
            event: this.event!,
            id: 0,
            title: '',
            type: this.linkTypeService.getById(1),
            url: '',
        };
        this.links.push(newLink);
        this.addLinkControl(newLink.title, newLink.url, newLink.type.id);
    }
}
