import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Evenement } from 'src/app/models/calendar/evenement.model';
import { Link } from 'src/app/models/calendar/link.model';
import { LinkType } from 'src/app/models/calendar/linkType.model';
import { EventService } from 'src/app/services/event.service';
import { LinkTypeService } from 'src/app/services/link-type.service';
import { LinkService } from 'src/app/services/link.service';

@Component({
    selector: 'app-link-post',
    templateUrl: './link-post.component.html',
    styleUrls: ['./link-post.component.scss'],
})
export class LinkPostComponent implements OnInit {
    titleControl = new FormControl('');
    urlControl = new FormControl('');
    eventControl = new FormControl('');
    linkTypeControl = new FormControl(1);
    events: Evenement[] = [];
    linkTypes: LinkType[] = [];
    eventId?: number;

    constructor(
        private linkService: LinkService,
        private linkTypeService: LinkTypeService,
        private eventService: EventService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.route.queryParams.subscribe((params) => {
            // Si la route ne contient pas le paramètre event ou que celui-ci n'est pas un nombre alors on redirigige à l'acceuil du calendrier
            if (!(params['event'] || []).length || isNaN(+params['event']))
                this.router.navigate(['/calendar']);
            this.eventId = +params['event'];
        });

        this.getEvents();
        this.getLinkType();
    }

    getEvents(): void {
        const subscription = this.eventService.getAll().subscribe((_events) => {
            _events.forEach((event) => this.events.push(event));
            subscription.unsubscribe();
        });
    }

    getLinkType(): void {
        const subscription = this.linkTypeService
            .getAll()
            .subscribe((_linkTypes) => {
                _linkTypes.forEach((linkType) => this.linkTypes.push(linkType));
                subscription.unsubscribe();
            });
    }

    onSubmit(form: NgForm): void {
        let event: Evenement;
        const subscription = this.eventService
            .get(this.eventId ? this.eventId : +this.eventControl.value)
            .subscribe((_event) => {
                event = _event;

                this.linkTypeService
                    .get(+this.linkTypeControl.value)
                    .subscribe((_linkType) => {
                        const linkType = _linkType;

                        // * - On créé le nouveau lien à partir du formulaire
                        const newLink: Link = {
                            id: 0,
                            title: this.titleControl.value,
                            url: this.linkService.formatUrl(
                                this.urlControl.value
                            ),
                            event: event,
                            type: linkType,
                        };

                        // * - On l'envoie dans une requête POST
                        const addSubscription = this.linkService
                            .post(newLink)
                            .subscribe((_) => {
                                // * - On redirige à l'acceuil
                                this.router.navigate(['/calendar']);
                                addSubscription.unsubscribe();
                            });
                    });

                subscription.unsubscribe();
            });
    }
}
