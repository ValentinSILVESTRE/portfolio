import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/calendar/category.model';
import { Evenement } from 'src/app/models/calendar/evenement.model';
import { Link } from 'src/app/models/calendar/link.model';
import { CategoryService } from 'src/app/services/category.service';
import { EventService } from 'src/app/services/event.service';
import { LinkTypeService } from 'src/app/services/link-type.service';
import {
    getStringDifference,
    isNow,
} from 'src/assets/functions/date.functions';
import { playSound } from 'src/assets/functions/sound.functions';

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss'],
})
export class EventComponent implements OnInit {
    @Input() id!: number;
    event?: Evenement;
    parentEvent?: Evenement;
    nextEvents: Evenement[] = [];
    passedEvents: Evenement[] = [];
    loading = true;
    found = false;

    constructor(
        private route: ActivatedRoute,
        private eventService: EventService,
        private linkTypeService: LinkTypeService,
        private categoryService: CategoryService,
        private router: Router
    ) {
        // Force le rechargement quand un paramètre de la route change
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    ngOnInit(): void {
        const routeParams = this.route.snapshot.paramMap;
        this.id = +routeParams.get('id')!;
        const getSubscription = this.eventService.get(this.id).subscribe({
            next: (event) => {
                this.event = event;
                this._playSound();
                this.getParentEvent();
                const now = new Date();
                const today = new Date(
                    now.getFullYear(),
                    now.getMonth(),
                    now.getDate()
                );
                this.nextEvents = event.subEvents
                    .sort((a, b) => (b.date < a.date ? 1 : -1))
                    .filter((e) => new Date(e.date) >= today);
                this.passedEvents = event.subEvents
                    .sort((a, b) => (a.date < b.date ? 1 : -1))
                    .filter((e) => new Date(e.date) < today);
            },
            error: (error) => {
                this.loading = false;
                getSubscription.unsubscribe();
            },
            complete: () => {
                this.loading = false;
                this.found = true;
                getSubscription.unsubscribe();
            },
        });
    }

    _playSound(): void {
        setInterval(() => {
            if (isNow(new Date(this.event!.date))) {
                playSound('assets/sounds/super_max.mp4', 16.5);
            }
        }, 1000);
    }

    getMaterialIconsName(linkTypeName: string): string {
        return this.linkTypeService.getMaterialIconsName(linkTypeName);
    }

    hasDescription(): boolean {
        if (!(this.event && this.event.description)) return false;
        return (
            this.event.description.split('').filter((e) => !e.match(/\s/g))
                .length > 0
        );
    }

    onDelete(): void {
        const deleteSubscription = this.eventService
            .delete(this.id)
            .subscribe(() => {
                this.router.navigate(['/calendar']);
                deleteSubscription.unsubscribe();
            });
    }

    hasChildren(): boolean {
        if (!this.event) {
            return false;
        }
        return this.event.subEvents.length > 0;
    }

    getParentEvent(): void {
        if (this.event && this.event.parentEventId) {
            const parentId = this.event.parentEventId;
            const subscription = this.eventService.get(parentId).subscribe({
                next: (_parentEvent) => {
                    this.parentEvent = _parentEvent;
                },
                complete: () => {
                    subscription.unsubscribe();
                },
                error: () => {
                    subscription.unsubscribe();
                },
            });
        }
    }

    getAncestors(event: Evenement): Evenement[] {
        return this.eventService.getAncestors(event);
    }

    iconIsImage(categoryName: string): boolean {
        return this.categoryService.iconIsImage(categoryName);
    }

    getCategoryIcon(categoryName: string) {
        return this.categoryService.getIcon(categoryName);
    }

    getCategorySource(categoryName: string) {
        return this.categoryService.getImageSource(categoryName);
    }

    getEventCategories(event: Evenement): Category[] {
        // Si l'événement n'a pas de parent, alors on renvoie ses categories
        if (!event.parentEventId) return event.categories;

        const categories: Category[] = [
            ...event.categories,
            ...this.getEventCategories(this.getEvent(event.parentEventId)!),
        ];

        const uniqueCategories: Category[] = [];

        for (const category of categories) {
            // On cherche s'il y a category dans uniqueCategories
            let i = 0;
            while (
                i < uniqueCategories.length &&
                uniqueCategories[i].id !== category.id
            ) {
                i++;
            }
            // Si on est arrivé au bout de la liste, c'est qu'elle n'y est pas
            if (i === uniqueCategories.length) {
                uniqueCategories.push(category);
            }
        }

        return uniqueCategories;
    }

    getEvent(eventId: number): Evenement | null {
        return this.eventService.getEventById(eventId);
    }

    relativeDate(event: Evenement) {
        return getStringDifference(new Date(), new Date(event.date));
    }

    isPassed(): boolean {
        if (this.event) return this.eventService.isPassed(this.event);
        return false;
    }

    stopPropagation(event: Event) {
        event.stopPropagation();
    }

    /**
     * - Renvoie les liens de l'événement et de ses parents réccursivement
     * @param event
     * @returns
     */
    getLinks(event: Evenement): Link[] {
        // Si l'événement n'a pas de parent, alors on renvoie ses liens
        if (!event.parentEventId) return event.links;
        // Sinon
        return [
            ...this.getLinks(this.getEvent(event.parentEventId)!),
            ...event.links,
        ];
    }
}
