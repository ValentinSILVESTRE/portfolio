import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-gameoflife-cell',
    templateUrl: './gameoflifeCell.component.html',
    styleUrls: ['./gameoflifeCell.component.scss'],
})
export class GameOfLifeCellComponent implements OnInit {
    /** - État de vie de la cellule */
    @Input() isAlive!: boolean;

    constructor() {}

    ngOnInit(): void {}
}
