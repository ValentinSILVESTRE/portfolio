import { Component, OnInit } from '@angular/core';
import { Stat } from 'src/app/models/gameoflife/stat';
import { Position } from 'src/app/models/position.model';
import { Board } from 'src/app/models/gameoflife/board.model';

@Component({
    selector: 'app-gameoflifeBoard',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
    /** - Le plateau de jeu */
    private board: Board = new Board();
    /** - Vrai si le jeu est en cours d'exécution, faux sinon */
    private isPlaying: boolean = false;
    /** - Délai entre deux évolutions (millisecondes) */
    public speed: number = 400;

    constructor() {}

    ngOnInit(): void {}

    onSpeedChange() {
        this.toggleIsPlaying();
        setTimeout(() => {
            this.togglePlay();
        }, 500);
    }

    getBoard(): Board {
        return this.board;
    }

    getIsPlaying(): boolean {
        return this.isPlaying;
    }

    setIsPlaying(isPlaying: boolean): void {
        this.isPlaying = isPlaying;
    }

    toggleIsPlaying(): void {
        this.setIsPlaying(!this.getIsPlaying());
    }

    getCells(): Stat[] {
        return this.getBoard().getCells();
    }

    /**
     * - On fait évoluer la grille
     */
    togglePlay(): void {
        this.toggleIsPlaying();
        const interval = setInterval(() => {
            if (this.getIsPlaying()) {
                if (!this.evolve()) {
                    clearInterval(interval);
                    this.setIsPlaying(false);
                }
            } else {
                clearInterval(interval);
            }
        }, this.speed);
    }

    /**
     * - Fais évoluer
     * @returns Renvoie vrai si la grille a été modifiée, faux sinon */
    evolve(): boolean {
        return this.board.evolve();
    }

    /**
     * - Inverse la valeur de la cellule
     * @param cellIndex - Index de la cellule à modifier
     */
    toggle(cellIndex: number): void {
        if (
            0 <= cellIndex &&
            cellIndex < this.board.getHeight() * this.board.getLength()
        ) {
            const position: Position = {
                x: cellIndex % this.board.getLength(),
                y: Math.floor(cellIndex / this.board.getLength()),
            };
            this.board.toggleValue(position);
        }
    }
}
