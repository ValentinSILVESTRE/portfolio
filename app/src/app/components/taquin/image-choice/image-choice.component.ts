import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaquinService } from 'src/app/services/taquin.service';

@Component({
    selector: 'app-image-choice',
    templateUrl: './image-choice.component.html',
    styleUrls: ['./image-choice.component.scss'],
})
export class ImageChoiceComponent implements OnInit {
    imageUrls = [
        'assets/images/taquin/game-images/2.jpg',
        'assets/images/taquin/game-images/5.jpg',
        'assets/images/taquin/game-images/7.jpg',
        'assets/images/taquin/game-images/8.jpg',
        'assets/images/taquin/game-images/14.jpg',
        'assets/images/taquin/game-images/16.jpg',
        'assets/images/taquin/game-images/17.jpg',
        'assets/images/taquin/game-images/18.jpg',
        'assets/images/taquin/game-images/19.jpg',
        'assets/images/taquin/game-images/20.jpg',
        'assets/images/taquin/game-images/22.jpg',
        'assets/images/taquin/game-images/23.jpg',
        'assets/images/taquin/game-images/24.jpg',
        'assets/images/taquin/game-images/25.jpg',
        'assets/images/taquin/game-images/26.jpg',
        'assets/images/taquin/game-images/28.jpg',
        'assets/images/taquin/game-images/35.jpg',
        'assets/images/taquin/game-images/37.jpg',
        'assets/images/taquin/game-images/38.jpg',
        'assets/images/taquin/game-images/39.jpg',
        'assets/images/taquin/game-images/40.jpg',
        'assets/images/taquin/game-images/41.jpg',
        'assets/images/taquin/game-images/42.jpg',
        'assets/images/taquin/game-images/44.jpg',
        'assets/images/taquin/game-images/48.jpg',
        'assets/images/taquin/game-images/49.jpg',
        'assets/images/taquin/game-images/50.jpg',
        'assets/images/taquin/game-images/51.jpg',
        'assets/images/taquin/game-images/52.jpg',
        'assets/images/taquin/game-images/53.jpg',
        'assets/images/taquin/game-images/56.jpg',
        'assets/images/taquin/game-images/58.jpg',
        'assets/images/taquin/game-images/59.jpg',
        'assets/images/taquin/game-images/60.jpg',
        'assets/images/taquin/game-images/62.jpg',
        'assets/images/taquin/game-images/64.jpg',
        'assets/images/taquin/game-images/65.jpg',
        'assets/images/taquin/game-images/67.jpg',
        'assets/images/taquin/game-images/68.jpg',
        'assets/images/taquin/game-images/80.jpg',
        'assets/images/taquin/game-images/82.jpg',
        'assets/images/taquin/game-images/88.jpg',
        'assets/images/taquin/game-images/90.jpg',
        'assets/images/taquin/game-images/93.jpg',
        'assets/images/taquin/game-images/95.jpg',
        'assets/images/taquin/game-images/96.jpg',
        'assets/images/taquin/game-images/97.jpg',
        'assets/images/taquin/game-images/98.jpg',
        'assets/images/taquin/game-images/99.jpg',
    ];

    constructor(private taquinService: TaquinService, private router: Router) {}

    ngOnInit(): void {}

    onImageClick(url: string) {
        this.taquinService.imageUrl = url;
        this.router.navigate(['/taquin']);
    }
}
