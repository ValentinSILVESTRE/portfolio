import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Position } from 'src/app/models/position.model';
import { GridTaquin } from 'src/app/models/taquin/gridTaquin.model';
import { TaquinService } from 'src/app/services/taquin.service';
import { playSound } from 'src/assets/functions/sound.functions';

@Component({
    selector: 'app-taquin',
    templateUrl: './taquin.component.html',
    styleUrls: ['./taquin.component.scss'],
})
export class TaquinComponent implements OnInit {
    size: number = 3;
    list!: number[];
    imageUrl!: string;
    grid!: GridTaquin;
    finish!: boolean;
    difficultyControl!: FormControl;
    moving: boolean = false;
    loading: boolean = false;
    showOriginal: boolean = false;

    onDifficultyChange() {
        this.ngOnInit(+this.difficultyControl.value);
        document.getElementById('difficulty')!.blur();
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private taquinService: TaquinService
    ) {
        this.imageUrl = taquinService.imageUrl;
    }

    ngOnInit(size: number = this.size): void {
        // On attend que tout charge
        this.loading = true;
        setTimeout(() => {
            this.loading = false;
        }, 1300);

        this.size = size;
        this.finish = false;
        this.difficultyControl = new FormControl(this.size);

        this.grid = new GridTaquin(this.size);
        this.list = [];
        for (let i = 0; i < this.size ** 2; i++) {
            this.list.push(i);
        }

        // On mélange la grille
        this.grid.mix();
        this.drawCanvas();
    }

    replay() {
        this.ngOnInit(this.size);
    }

    onFinish(): void {
        this.finish = true;
        playSound('assets/sounds/super_max.mp4', 16.5);

        const game = document.getElementsByClassName(
            'game'
        )[0] as HTMLCanvasElement;
        game.classList.add('finish');
    }

    /**
     * - Créé les canvas et les définis comme enfants de leur container
     */
    drawCanvas(): void {
        setTimeout(() => {
            const image = document.getElementById(
                'source-image'
            ) as HTMLImageElement;
            const canvasList = Array.from(
                document.getElementsByTagName('canvas')
            ) as HTMLCanvasElement[];

            for (let y = 0; y < this.size; y++) {
                for (let x = 0; x < this.size; x++) {
                    const canvas = canvasList[x + y * this.size]!;
                    const ctx = canvas.getContext('2d')!;

                    ctx.drawImage(
                        image,
                        (x * image.naturalWidth) / this.size,
                        (y * image.naturalHeight) / this.size,
                        image.naturalWidth / this.size,
                        image.naturalHeight / this.size,
                        0,
                        0,
                        canvas.width,
                        canvas.height
                    );
                }
            }
            this.canvasPositionning();
        }, 1300);
    }

    /**
     * - On positionne les tuiles en leur définissant l'attribut ordre correspondant à la grille
     */
    canvasPositionning() {
        for (let y = 0; y < this.size; y++) {
            for (let x = 0; x < this.size; x++) {
                const position: Position = { x, y };
                /** - Position du canvas dans la grille */
                const order: number = x + y * this.size;
                document
                    .getElementById(
                        'a' + this.grid.getValue(position).toString()
                    )!
                    .setAttribute('order', order.toString());
            }
        }
    }

    isLast(i: number): boolean {
        return i == this.size ** 2 - 1;
    }

    getGridTemplate(): string {
        return `repeat(${this.size}, 1fr)`;
    }

    onClick(i: number): void {
        const emptyCellPosition = this.grid.getPosition()!;
        const clickedCellPosition = this.grid.getPosition(i)!;

        if (this.showOriginal) return;

        // Si la cellule est différente et alignée
        if (
            (clickedCellPosition.x !== emptyCellPosition.x &&
                clickedCellPosition.y === emptyCellPosition.y) ||
            (clickedCellPosition.x === emptyCellPosition.x &&
                clickedCellPosition.y !== emptyCellPosition.y)
        ) {
            // A gauche
            if (clickedCellPosition.x < emptyCellPosition.x) {
                this.moving = true;
                const canvasList = document.getElementsByClassName(`canvas`);
                const canvasLeftList: any[] = [];
                const emptyPosition = this.grid.getPosition()!;

                for (let i = 0; i < canvasList.length; i++) {
                    const canvas = canvasList[i];
                    const order = +canvas.getAttribute('order')!;
                    const position = {
                        x: order % this.size,
                        y: Math.floor(order / this.size),
                    };

                    // On ne garde que ceux dont l'ordre correspond à une position à gauche entre la cliquée et la vide
                    if (
                        position.y === emptyPosition.y &&
                        position.x < emptyPosition.x &&
                        clickedCellPosition.x <= position.x
                    ) {
                        canvasLeftList.push(canvas);
                    }
                }

                // On effectue les transitions css
                for (let i = 0; i < canvasLeftList.length; i++) {
                    const canvas = canvasLeftList[i];
                    canvas.classList.add('moveRight');
                }

                setTimeout(() => {
                    for (let i = 0; i < canvasLeftList.length; i++) {
                        const canvas = canvasLeftList[i];
                        canvas.classList.remove('moveRight');
                    }

                    this.grid.moveCell(i);
                    this.canvasPositionning();
                    this.moving = false;

                    // Si on termine la partie
                    if (this.grid.finish) {
                        this.onFinish();
                    }
                }, 300);
            }

            // A Droite
            if (clickedCellPosition.x > emptyCellPosition.x) {
                this.moving = true;
                const canvasList = document.getElementsByClassName(`canvas`);
                const canvasRightList: any[] = [];
                const emptyPosition = this.grid.getPosition()!;

                for (let i = 0; i < canvasList.length; i++) {
                    const canvas = canvasList[i];
                    const order = +canvas.getAttribute('order')!;
                    const position = {
                        x: order % this.size,
                        y: Math.floor(order / this.size),
                    };

                    // On ne garde que ceux dont l'ordre correspond à une position à droite entre la cliquée et la vide
                    if (
                        position.y === emptyPosition.y &&
                        position.x > emptyPosition.x &&
                        clickedCellPosition.x >= position.x
                    ) {
                        canvasRightList.push(canvas);
                    }
                }

                // On effectue les transitions css
                for (let i = 0; i < canvasRightList.length; i++) {
                    const canvas = canvasRightList[i];
                    canvas.classList.add('moveLeft');
                }

                setTimeout(() => {
                    for (let i = 0; i < canvasRightList.length; i++) {
                        const canvas = canvasRightList[i];
                        canvas.classList.remove('moveLeft');
                    }

                    this.grid.moveCell(i);
                    this.canvasPositionning();
                    this.moving = false;

                    // Si on termine la partie
                    if (this.grid.finish) {
                        this.onFinish();
                    }
                }, 300);
            }

            // En dessous
            if (clickedCellPosition.y > emptyCellPosition.y) {
                this.moving = true;
                const canvasList = document.getElementsByClassName(`canvas`);
                const canvasBottomList: any[] = [];
                const emptyPosition = this.grid.getPosition()!;

                for (let i = 0; i < canvasList.length; i++) {
                    const canvas = canvasList[i];
                    const order = +canvas.getAttribute('order')!;
                    const position = {
                        x: order % this.size,
                        y: Math.floor(order / this.size),
                    };

                    // On ne garde que ceux dont l'ordre correspond à une position en dessous entre la cliquée et la vide
                    if (
                        position.y > emptyPosition.y &&
                        position.x === emptyPosition.x &&
                        clickedCellPosition.y >= position.y
                    ) {
                        canvasBottomList.push(canvas);
                    }
                }

                // On effectue les transitions css
                for (let i = 0; i < canvasBottomList.length; i++) {
                    const canvas = canvasBottomList[i];
                    canvas.classList.add('moveUp');
                }

                setTimeout(() => {
                    for (let i = 0; i < canvasBottomList.length; i++) {
                        const canvas = canvasBottomList[i];
                        canvas.classList.remove('moveUp');
                    }

                    this.grid.moveCell(i);
                    this.canvasPositionning();
                    this.moving = false;

                    // Si on termine la partie
                    if (this.grid.finish) {
                        this.onFinish();
                    }
                }, 300);
            }

            // Au dessus
            if (clickedCellPosition.y < emptyCellPosition.y) {
                this.moving = true;
                const canvasList = document.getElementsByClassName(`canvas`);
                const canvasTopList: any[] = [];
                const emptyPosition = this.grid.getPosition()!;

                for (let i = 0; i < canvasList.length; i++) {
                    const canvas = canvasList[i];
                    const order = +canvas.getAttribute('order')!;
                    const position = {
                        x: order % this.size,
                        y: Math.floor(order / this.size),
                    };

                    // On ne garde que ceux dont l'ordre correspond à une position au dessus entre la cliquée et la vide
                    if (
                        position.y < emptyPosition.y &&
                        position.x === emptyPosition.x &&
                        clickedCellPosition.y <= position.y
                    ) {
                        canvasTopList.push(canvas);
                    }
                }

                // On effectue les transitions css
                for (let i = 0; i < canvasTopList.length; i++) {
                    const canvas = canvasTopList[i];
                    canvas.classList.add('moveDown');
                }

                setTimeout(() => {
                    for (let i = 0; i < canvasTopList.length; i++) {
                        const canvas = canvasTopList[i];
                        canvas.classList.remove('moveDown');
                    }

                    this.grid.moveCell(i);
                    this.canvasPositionning();
                    this.moving = false;

                    // Si on termine la partie
                    if (this.grid.finish) {
                        this.onFinish();
                    }
                }, 300);
            }
        }
    }

    positionToIndex(position: Position, size: number): number {
        return position.x + size * position.y;
    }

    // Méthode appelée quand on appuie sur une touche directionnelle
    @HostListener('window:keydown', ['$event'])
    handleKeydown(event: KeyboardEvent): void {
        // Si on modifie la difficulté au clavier, alors on ne déplace pas de case, idem si on affiche l'original
        if (
            document.getElementById('difficulty')! === document.activeElement ||
            this.showOriginal
        ) {
            return;
        }

        if (
            !this.moving &&
            !this.grid.finish &&
            ['ArrowUp', 'ArrowDown', 'ArrowRight', 'ArrowLeft'].includes(
                event.key
            )
        ) {
            const emptyCellOrder = this.positionToIndex(
                this.grid.getPosition()!,
                this.size
            );
            switch (event.key) {
                case 'ArrowUp':
                    // Si on n'est pas sur la ligne du haut
                    if (emptyCellOrder >= this.size) {
                        // On récupère le canvas au dessus de la case vide, càd ordre de la case vide - size
                        this.moving = true;
                        const canvas = document.querySelectorAll(
                            `[order="${emptyCellOrder - this.size}"]`
                        )[0];
                        // On effectue la transition css
                        canvas.classList.add('moveDown');
                        setTimeout(() => {
                            canvas.classList.remove('moveDown');
                            this.grid.move('up');
                            this.canvasPositionning();
                            this.moving = false;

                            // Si on termine la partie
                            if (this.grid.finish) {
                                this.onFinish();
                            }
                        }, 300);
                    }
                    break;

                case 'ArrowDown':
                    // Si on n'est pas sur la ligne du bas
                    if (emptyCellOrder < this.size * (this.size - 1)) {
                        // On récupère le canvas en dessous de la case vide, càd ordre de la case vide + size
                        this.moving = true;
                        const canvas = document.querySelectorAll(
                            `[order="${emptyCellOrder + this.size}"]`
                        )[0];
                        // On effectue la transition css
                        canvas.classList.add('moveUp');
                        setTimeout(() => {
                            canvas.classList.remove('moveUp');
                            this.grid.move('down');
                            this.canvasPositionning();
                            this.moving = false;

                            // Si on termine la partie
                            if (this.grid.finish) {
                                this.onFinish();
                            }
                        }, 300);
                    }
                    break;

                case 'ArrowLeft':
                    // Si on n'est pas sur la colonne de gauche
                    if (emptyCellOrder % this.size) {
                        this.moving = true;
                        // On récupère le canvas à gauche de la case vide
                        const canvas = document.querySelectorAll(
                            `[order="${emptyCellOrder - 1}"]`
                        )[0];
                        // On effectue la transition css
                        canvas.classList.add('moveRight');
                        setTimeout(() => {
                            canvas.classList.remove('moveRight');
                            this.grid.move('left');
                            this.canvasPositionning();
                            this.moving = false;

                            // Si on termine la partie
                            if (this.grid.finish) {
                                this.onFinish();
                            }
                        }, 300);
                    }
                    break;

                case 'ArrowRight':
                    // Si on n'est pas sur la colonne de gauche
                    if ((emptyCellOrder + 1) % this.size) {
                        this.moving = true;
                        // On récupère le canvas à gauche de la case vide
                        const canvas = document.querySelectorAll(
                            `[order="${emptyCellOrder + 1}"]`
                        )[0];
                        // On effectue la transition css
                        canvas.classList.add('moveLeft');
                        setTimeout(() => {
                            canvas.classList.remove('moveLeft');
                            this.grid.move('right');
                            this.canvasPositionning();
                            this.moving = false;

                            // Si on termine la partie
                            if (this.grid.finish) {
                                this.onFinish();
                            }
                        }, 300);
                    }
                    break;
            }
        }
    }

    onShowImage() {
        this.showOriginal = !this.showOriginal;
    }
}
