import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
    @Input() title!: string;
    @Input() calendar?: boolean;
    now = Date();
    screenSize!: 'small' | 'medium' | 'large';
    queryControl = new FormControl('');

    constructor(private router: Router) { }

    ngOnInit(): void {
        if (this.calendar) {
            setInterval(() => {
                this.now = Date();
            }, 1000);
        }

        this.setScreenSize();
    }

    setScreenSize(): void {
        if (window.innerWidth < 600) this.screenSize = 'small';
        else if (window.innerWidth < 1200) this.screenSize = 'medium';
        else this.screenSize = 'large';
    }

    onResize(): void {
        this.setScreenSize();
    }

    onSearchSubmit() {
        const query: string = this.queryControl.value.trim();
        this.queryControl.reset();
        if (query.length) {
            this.router.navigate(['/calendar/search'], {
                queryParams: { query: query },
            });
        }
    }
}
