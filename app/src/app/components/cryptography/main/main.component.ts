import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { offsetDecryption as _offsetDecryption } from 'src/assets/functions/encryption.functions';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
    offsetControl: FormControl = new FormControl(0);
    offsetEncryptedMessageControl: FormControl = new FormControl('');

    constructor() {}

    ngOnInit(): void {}

    getOffsetDecryptedMessage() {
        return this.offsetDecryption(
            +this.offsetControl.value,
            this.offsetEncryptedMessageControl.value
        );
    }

    offsetDecryption(offset: number, encryptedMessage: string) {
        return _offsetDecryption(offset, encryptedMessage);
    }
}
