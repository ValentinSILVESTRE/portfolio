import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-type',
    templateUrl: './type.component.html',
    styleUrls: ['./type.component.scss'],
})
export class TypeComponent implements OnInit {
    @Input() name!: string;
    @Input() isFirst!: boolean;
    @Input() isLast!: boolean;

    constructor() {}

    ngOnInit(): void {}
}
