import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Field, Pokemon } from '@smogon/calc';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
    selector: 'app-fight',
    templateUrl: './fight.component.html',
    styleUrls: ['./fight.component.scss'],
})
export class FightComponent implements OnInit {
    /** - Liste des pokemons */
    pokemons!: Pokemon[][];
    /** - Le champ de bataille */
    field!: Field;

    constructor(
        private router: Router,
        private pokemonService: PokemonService
    ) {}

    ngOnInit(): void {
        this.pokemons = this.pokemonService.pokemons;
        this.field = this.pokemonService.field;

        // Si une des deux équipes n'est pas saisie alors on redirige au formulaire de sélection
        if (
            !this.pokemonService.teams[0].length ||
            !this.pokemonService.teams[1].length
        ) {
            this.router.navigate(['/form']);
        }
    }

    /** - Méthode qui définit l'attaquant */
    onSelectPokemon(teamIndex: number, pokemonIndex: number): void {
        this.pokemonService.setAttacker(teamIndex, pokemonIndex);
    }

    setSpeed(speed: number): void {
        this.pokemonService.attackerSpeed = speed;
    }

    /** - Méthode qui déselectionne l'attaquant */
    onUnselectPokemon(): void {
        this.pokemonService.unselectAttacker();
    }

    /** - Méthode qui empêche d'ouvrir le menu avec un clique droit */
    preventContextMenu(e: any) {
        if (!this.pokemonService.debug) {
            e.preventDefault();
        }
    }

    /** - Met à jour le terrain */
    updateField(): void {
        this.pokemonService.field = this.field;
    }

    /** - Renvoie la description du terrain */
    getTerrainDesc(short?: boolean): string | undefined {
        if (short) {
            switch (this.field.terrain) {
                case 'Electric':
                    return `Grounded : no sleep (no wake up),
    Electric attack : 150%,
Camouflage : Electric type,
Nature Power = Thunderbolt,
Secret Power = 30% chance to cause paralysis`;

                case 'Grassy':
                    return `Grounded : heal 1/16 each turns
Against grounded : Bulldoze, Earthquake, and Magnitude : 50%
Grass attack : 150%,
Camouflage : Grass type,
Nature Power = Energy Ball,
Secret Power = 30% chance to cause sleep`;

                case 'Misty':
                    return `Against grounded :
    Dragon attacks : 50%
    non-volatile status condition or confusion : 0%
Camouflage : Fairy type,
Nature Power = Moonblast,
Secret Power = 30% chance to lower Special Attack`;

                case 'Psychic':
                    return `Grounded : immune to priority attacks
    Psychic attack : 150%,
Camouflage : Psychic type,
Nature Power = Psychic,
Secret Power = 30% chance to lower the target's Speed`;
            }
        }

        switch (this.field.terrain) {
            case 'Electric':
                return 'Electric attacks made by grounded Pokemon is multiplied by 1.5 and grounded Pokemon cannot fall asleep; Pokemon already asleep do not wake up. Grounded Pokemon cannot become affected by Yawn or fall asleep from its effect. Camouflage transforms the user into an Electric type, Nature Power becomes Thunderbolt, and Secret Power has a 30% chance to cause paralysis.';

            case 'Grassy':
                return 'Grass attacks used by grounded Pokemon is multiplied by 1.5, the power of Bulldoze, Earthquake, and Magnitude used against grounded Pokemon is multiplied by 0.5, and grounded Pokemon have 1/16 of their maximum HP, rounded down, restored at the end of each turn, including the last turn. Camouflage transforms the user into a Grass type, Nature Power becomes Energy Ball, and Secret Power has a 30% chance to cause sleep.';

            case 'Misty':
                return 'Dragon attacks used against grounded Pokemon is multiplied by 0.5 and grounded Pokemon cannot be inflicted with a non-volatile status condition nor confusion. Grounded Pokemon can become affected by Yawn but cannot fall asleep from its effect. Camouflage transforms the user into a Fairy type, Nature Power becomes Moonblast, and Secret Power has a 30% chance to lower Special Attack by 1 stage.s Misty Terrain.';

            case 'Psychic':
                return "Psychic attacks made by grounded Pokemon is multiplied by 1.5 and grounded Pokemon cannot be hit by moves with priority greater than 0, unless the target is an ally. Camouflage transforms the user into a Psychic type, Nature Power becomes Psychic, and Secret Power has a 30% chance to lower the target's Speed by 1 stage.e current terrain is Psychic Terrain.";

            default:
        }
        return '';
    }
}
