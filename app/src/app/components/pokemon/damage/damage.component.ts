import { Component, Input, OnInit } from '@angular/core';
import { Damages } from 'src/app/models/pokemon/damages';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
    selector: 'app-damage',
    templateUrl: './damage.component.html',
    styleUrls: ['./damage.component.scss'],
})
export class DamageComponent implements OnInit {
    /** Les dégats */
    @Input() damages!: Damages;
    /** - La position dans le template */
    @Input() isLeft!: boolean;

    constructor(private pokemonService: PokemonService) { }

    ngOnInit(): void { }
}
