import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Field, Pokemon } from '@smogon/calc';
import {
    AbilityName,
    GenderName,
    ItemName,
    SpeciesName,
    StatName,
    StatusName,
    Terrain,
    TypeName,
    Weather,
} from '@smogon/calc/dist/data/interface';
import { calcStat } from '@smogon/calc/dist/index';
import { PokemonService } from 'src/app/services/pokemon.service';
import { abilities } from 'src/assets/data/pokemon/abilities';
import { pokedex } from 'src/assets/data/pokemon/pokedex';
import { learnsets } from 'src/assets/data/pokemon/learnsets';
import { items } from 'src/assets/data/pokemon/items';
import { moves } from 'src/assets/data/pokemon/moves';
import { Damages } from 'src/app/models/pokemon/damages';
import { Nature } from 'src/app/models/pokemon/nature';
import { Status } from 'src/app/models/pokemon/status';
import { Boost } from 'src/app/models/pokemon/boost';
import { MoveService } from 'src/app/services/move.service';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
    debug!: boolean;
    /** - Index de l'équipe du pokemon */
    @Input() teamIndex!: number;
    /** - Index du pokemon */
    @Input() pokemonIndex!: number;
    /** - Le pokemon */
    @Input() pokemon!: Pokemon;
    /** - Le champ */
    @Input() field!: Field;
    // @Output() speedEvent: new EventEmitter<number>;
    @Output() speed: EventEmitter<any> = new EventEmitter();
    /** - Nombre d'ev pouvant être dépensés */
    remainingEvs!: number;
    /** - Vrai s'il s'agit du pokemon attaquant, faux sinon */
    selected: boolean = false;
    /** - Vrai si on est mode edition */
    editMode: boolean = false;
    /** - Liste des objets disponibles */
    itemList: { name: string; desc: string }[] = items;
    /** Liste des noms des statistiques des natures */
    natureStatsName: string[] = ['atk', 'def', 'spa', 'spd', 'spe', 'neutral'];
    /** Liste des status */
    statusList!: Status[];
    /** Vaut vrai si le pokemon est au sol, il gravite */
    isGrounded: boolean = false;
    /** - Vitesse du pokemon qui attaque */
    attackerSpeed?: number;
    /** - Forme du pokemon */
    form?: string;
    /** - Faux tant qu'aucun set n'a été attribué */
    hasSet = false;

    constructor(
        private pokemonService: PokemonService,
        private moveService: MoveService
    ) {}

    ngOnInit(): void {
        this.statusList = this.pokemonService.statusList;
        this.debug = this.pokemonService.debug;
        this.attackerSpeed = this.pokemonService.attacker?.stats.spe;
        this.update();
        this.hasSet =
            this.pokemonService.hasSets[this.teamIndex][this.pokemonIndex];
    }

    hasMultipleForms(): boolean {
        return this.pokemon.name.substring(0, 9) === 'Aegislash';
    }

    sendSpeed(): void {
        this.speed.emit(this.getStat('spe', true));
    }

    reverse(): boolean {
        return this.teamIndex === 1;
    }

    reset(pokemon?: Pokemon) {
        this.pokemonService.reset(this.teamIndex, this.pokemonIndex, pokemon);
    }

    /**
     * - Renvoie le nom affichable du move
     * @param name - Nom formaté du move recherché
     * @returns
     */
    moveName(name: string): string {
        return moves[name].name;
    }

    imgUri() {
        const IMG_NAME = this.pokemon.name
            .replace('Aegislash-Shield', 'aegislash')
            .replace('Urshifu-*', 'Urshifu')
            .replace('Ho-oh', 'hooh')
            .replace('Tapu-', 'tapu')
            .replace('Kommo-o', 'kommoo')
            .replace('Charizard-mega-', 'charizard-mega')
            .replace(/[’.%\s]/g, '')
            .toLowerCase();

        return `https://play.pokemonshowdown.com/sprites/ani/${IMG_NAME}.gif`;
    }

    /**
     * - Renvoie le nom complet de la statistique
     * @param stat - Nom diminué de la statistique
     * @returns
     */
    getStatFullName(stat: string): string {
        switch (stat.toLowerCase()) {
            case 'atk':
                return 'Attack';
            case 'def':
                return 'Defense';
            case 'spa':
                return 'Special Attack';
            case 'spd':
                return 'Special Defense';
            case 'spe':
                return 'Speed';
            case 'hp':
                return 'Health';
            case 'neutral':
                return 'Neutral';

            default:
                console.error(`getStatFullName(${stat}) est impossible.`);
                return '';
        }
    }

    /** - Renvoie la liste des natures */
    getNatures(): Nature[] {
        return this.pokemonService.natures;
    }

    /**
     * - Renvoie la liste des natures correspondant au boost
     * @param boost - atk | def | spa | spd | spe | neutral
     */
    getNaturesByBoost(boost: string): Nature[] {
        const list: Nature[] = [];
        this.pokemonService.natures.forEach((nature) => {
            if (
                nature.up === boost ||
                (boost === 'neutral' && nature.up === '')
            ) {
                list.push(nature);
            }
        });
        return list;
    }

    getNaturesBoost(): string[] {
        return ['atk', 'def', 'spa', 'spd', 'spe', 'neutral'];
    }

    /**
     * - Renvoie la nature recherchée
     * @param name - Nom de la nature avec une majuscule
     */
    getNatureByName(name: string): Nature {
        let i = 0;
        while (this.pokemonService.natures[i]) {
            const nature = this.pokemonService.natures[i];
            if (nature.label === name) {
                return nature;
            }
            i++;
        }
        return this.pokemonService.natures[0];
    }

    getStatus(): Status {
        let i = 0;
        while (this.pokemonService.statusList[i]) {
            const status = this.pokemonService.statusList[i];
            if (status.name === this.pokemon.status) {
                return status;
            }
            i++;
        }
        return this.pokemonService.statusList[0];
    }

    /** - Renvoie la liste des attaques que le pokemon apprend */
    getLearnMoves(): string[] {
        return Object.keys(learnsets[this.getFormatName()].learnset);
    }

    getAllMoves(): string[] {
        return this.pokemonService.getAllMoves();
    }

    /** - Permute le mode édition <=> visualisation */
    switchMode(): void {
        this.editMode = !this.editMode;
    }

    /** - En mouse over, le pokemon et son équipe deviennent les attaquants */
    mouseenter(): void {
        this.sendSpeed();
        this.selected = true;
    }

    /** - En mouse over, le pokemon et son équipe deviennent les attaquants */
    mouseclick(click: any): void {
        if (click.button && click.button === 2) {
            this.pokemonService.toggleLock();
        }
    }

    /** - En mouse leave, le pokemon n'attaque plus */
    mouseleave(): void {
        this.selected = false;
    }

    /**
     * - Renvoie l'index de la meilleure attaque que l'attaquant (service) peut nous faire
     */
    getBestMoveIndex(): number {
        let bestIndex = 0;
        if (this.pokemonService.attacker) {
            const attacker: Pokemon = this.pokemonService.attacker;
            for (let i = 1; i < attacker.moves.length; i++) {
                let bestDegats = this.pokemonService.calc(
                    attacker.moves[bestIndex],
                    attacker,
                    this.pokemon
                ).avg;
                let newDegats = this.pokemonService.calc(
                    attacker.moves[i],
                    attacker,
                    this.pokemon
                ).avg;
                if (newDegats > bestDegats) {
                    bestIndex = i;
                }
            }
        }
        return bestIndex;
    }

    /** Renvoie si le pokemon gravite, s'il a atterit */
    getGravity(): boolean {
        return this.isGrounded;
    }

    /**
     * - Renvoie l'index de notre meilleure attaque contre l'attaquant
     */
    getDefenderBestMoveIndex(): number {
        let bestIndex = 0;
        if (this.pokemonService.attacker) {
            const attacker: Pokemon = this.pokemonService.attacker;
            let field: Field = new Field();
            field.isGravity = this.getGravity();
            for (let i = 1; i < this.pokemon.moves.length; i++) {
                let bestDegats = this.pokemonService.calc(
                    this.pokemon.moves[bestIndex],
                    this.pokemon,
                    attacker
                ).avg;
                let newDegats = this.pokemonService.calc(
                    this.pokemon.moves[i],
                    this.pokemon,
                    attacker
                ).avg;
                if (newDegats > bestDegats) {
                    bestIndex = i;
                }
            }
        }
        return bestIndex;
    }

    /** - Renvoie l'attaquant, en supposant qu'il existe */
    getOpponent(): Pokemon {
        return this.pokemonService.attacker!;
    }

    /** - Renvoie vrai si l'équipe attaquante est différente de la notre, faux sinon */
    isAttacked(): boolean {
        return this.pokemonService.attackingTeamIndex !== this.teamIndex;
    }

    /** - Renvoie l'index du move utilisé par l'attaquant */
    getMoveIndex(): number {
        if (this.pokemonService.moveIndex !== undefined) {
            return this.pokemonService.moveIndex;
        }
        return this.getBestMoveIndex();
    }

    toogleRoost(): void {
        // Si le pokemon est au sol, alors il lévite ou s'envole
        if (this.getGravity()) {
            // Si le pokemon lévitait, il perd sa capacité
            if (this.pokemon.ability === 'No ability') {
                this.pokemon.ability = 'Levitate' as AbilityName;
            }
            // Sinon il récupère ses types
            else {
                const pkmn = this.pokemonService.getPokemon(this.pokemon.name);
                this.pokemon.types = [...pkmn.types];
            }
        }
        // Si le pokemon est en l'air, alors il atterit
        else {
            // Si le pokemon lévite, il reprend sa capacité
            if (this.pokemon.ability === 'Levitate') {
                this.pokemon.ability = 'No ability' as AbilityName;
            }
            // Si le pokemon n'a que le type vol, alors il le perd et devient de type normal
            if (
                this.pokemon.types.length === 1 &&
                this.pokemon.types[0] === 'Flying'
            ) {
                this.pokemon.types[0] = 'Normal';
            }
            // Si le pokemon a plusieurs types, dont le type vol alors il le perd
            if (this.pokemon.types.length > 1) {
                if (this.pokemon.types[0] === 'Flying') {
                    this.pokemon.types[0] = '???';
                } else {
                    this.pokemon.types[1] = '???';
                }
            }
        }
        this.isGrounded = !this.isGrounded;
        this.update();
    }

    /** - Renvoie les dégats causés par l'attaquant */
    getAttakerDamages(): Damages {
        let moveIndex = this.getDefenderBestMoveIndex();
        return this.pokemonService.calc(
            this.pokemon.moves[moveIndex],
            this.pokemon,
            this.getOpponent()
        );
    }

    /** - Renvoie les dégats causés par le défenseur */
    getDefenderDamages(): Damages {
        let moveIndex = this.getMoveIndex();
        return this.pokemonService.calc(
            this.getOpponent().moves[moveIndex],
            this.getOpponent(),
            this.pokemon
        );
    }

    /** - Renvoie l'url de l'objet à afficher */
    getItemUrl(): string {
        if (
            (this.pokemon.item !== 'Eviolite' &&
                this.pokemon.item?.substring(this.pokemon.item!.length - 3) ===
                    'ite') ||
            this.pokemon.item
                ?.substring(this.pokemon.item!.length - 5)
                .toLowerCase() === 'ite-y' ||
            this.pokemon.item
                ?.substring(this.pokemon.item!.length - 5)
                .toLowerCase() === 'ite-x'
        ) {
            return 'assets/images/pokemon/items/mega stone.png';
        }
        if (this.pokemon.item === 'Safety Goggles') {
            return 'assets/images/pokemon/items/Safety_Goggles.webp';
        }
        if (this.pokemon.item === 'Electric Seed') {
            return 'assets/images/pokemon/items/electric_seed.png';
        }
        if (this.pokemon.item === 'Assault Vest') {
            return 'assets/images/pokemon/items/assault-vest.png';
        }
        if (this.pokemon.item === 'Heavy-Duty Boots') {
            return 'assets/images/pokemon/items/boots.png';
        }
        if (this.pokemon.item === "King's Rock") {
            return 'https://play.pokemonshowdown.com/sprites/itemicons/kings-rock.png';
        }
        if (this.pokemon.item === 'Weakness Policy') {
            return 'assets/images/pokemon/items/weaknesspolicy.png';
        }
        if (this.pokemon.item === 'Throat Spray') {
            return 'assets/images/pokemon/items/throat spray.png';
        }
        if (this.pokemon.item === 'Terrain Extender') {
            return 'assets/images/pokemon/items/terrain extender.png';
        }

        return `https://play.pokemonshowdown.com/sprites/itemicons/${this.pokemon
            .item!.toLowerCase()
            .replace('black glasses', 'blackglasses')
            .replaceAll(' ', '-')}.png`;
    }

    /** - Renvoie le nombre pouvant êtres utilisés */
    getRemainingsEvs(): number {
        let res: number = 508;
        this.pokemonService.statsName.forEach((statName) => {
            const ev = this.pokemon.evs[statName];
            res -= ev;
        });
        return res;
    }

    /** - Renvoie la liste des types du pokemon */
    getTypes(): string[] {
        const types: string[] = [];
        this.pokemon.types.forEach((type: TypeName) => {
            if (type !== '???') {
                types.push(type);
            }
        });
        return types;
    }

    /** - Renvoie du capacité du pokemon */
    getAbility(): string {
        if (this.pokemon.ability) {
            return this.pokemon.ability as string;
        }
        return '';
    }

    getFormatName(): string {
        return this.pokemonService.formatPokemonName(this.pokemon.name);
    }

    /** - Renvoie la liste des capacités du pokemon */
    getAbilities(): string[] {
        const _abilities = [] as string[];
        for (const key in pokedex[this.pokemon.species.id].abilities) {
            _abilities.push(pokedex[this.pokemon.species.id].abilities[key]);
        }
        return _abilities;
    }

    /** - Renvoie la description de la capacité du pokemon */
    getAbilityDesc(): string {
        return abilities[
            this.pokemon
                .ability!.toLowerCase()
                // .replaceAll(' ', '')
                // .replaceAll("'", '')
                // .replaceAll("-", '')
                .replace(/[\s'-]/g, '')
        ].shortDesc as string;
    }

    /** - Renvoie la description de l'objet passé en paramètre */
    getItemDesc(itemName: string): string {
        let i = 0;
        while (this.itemList[i]) {
            const item = this.itemList[i];
            if (item.name === itemName && item.desc) {
                return item.desc;
            }
            i++;
        }
        return '';
    }

    /**
     * - Renvoie la valeur de la statistique en prennatn en compte les evs, etc ...
     * @param stat - Nom de la statistique
     * @param withBoosts - Prise en compte des boosts
     */
    statValue(stat: StatName, withBoosts: boolean): number {
        let multiplier: number = 1;
        if (withBoosts) {
            multiplier = this.getBoostMultiplier(stat);
        }
        return (
            multiplier *
            calcStat(
                this.pokemonService.gen,
                stat,
                this.pokemon.species.baseStats[stat],
                this.pokemon.ivs[stat],
                this.pokemon.evs[stat],
                this.pokemon.level,
                this.pokemon.nature
            )
        );
    }

    getBoostMultiplier(stat: StatName): number {
        const boostValue = this.pokemon.boosts[stat];
        if (boostValue === 0) {
            return 1;
        } else if (boostValue > 0) {
            return (2 + boostValue) / 2;
        } else {
            return 2 / (2 - boostValue);
        }
    }

    /**
     * - Renvoie la valeur de la statistique en prenant en compte les ev, iv
     * @param {string} stat
     * @returns {Number}
     */
    getStat(stat: StatName, withBoosts: boolean = false): number {
        let base = this.statValue(stat, withBoosts);
        const weather: Weather | undefined = this.field.weather;
        const terrain: Terrain | undefined = this.field.terrain;

        // Gestion des objets
        if (stat === 'spe' && this.pokemon.item === 'Choice Scarf') {
            base *= 1.5;
        }
        if (stat === 'spe' && this.pokemon.item === 'Iron Ball') {
            base *= 0.5;
        }
        if (
            ['atk', 'spa'].includes(stat) &&
            this.pokemon.name === 'Pikachu' &&
            this.pokemon.item === 'Light Ball'
        ) {
            base *= 2;
        }

        // Gestion du status
        const status = this.pokemon.status as string;
        if (status === 'brn' && stat === 'atk') {
            base /= 2;
        }
        if (status === 'par' && stat === 'spe') {
            base /= 2;
        }

        // Gestion des capacités
        switch (this.pokemon.ability) {
            case 'Chlorophyll':
                // Vitesse doublée sous le soleil
                if (stat === 'spe' && weather === 'Sun') {
                    base *= 2;
                }
                break;

            case 'Surge Surfer':
                if (stat === 'spe' && terrain === 'Electric') {
                    base *= 2;
                }
                break;

            case 'Sand Rush':
                // Vitesse doublée sous la tempete de sable
                if (stat === 'spe' && weather === 'Sand') {
                    base *= 2;
                }
                break;

            case 'Guts':
                // Attaque augmenté de 50% si brulé, paralisé ou empoinsonné
                if (stat === 'atk') {
                    if (status === 'burned') {
                        base *= 3;
                    }
                    if (
                        status === 'paralysed' ||
                        status === 'poisoned' ||
                        status === 'toxic'
                    ) {
                        base *= 1.5;
                    }
                }
                break;

            case 'Huge Power':
                // Attaque doublée
                if (stat === 'atk') {
                    base *= 2;
                }
                break;

            case 'Hustle':
                // Attaque augmentée de 50%
                if (stat === 'atk') {
                    base *= 1.5;
                }
                break;

            case 'Marvel Scale':
                // Défense augmenté de 50% si problème de status
                if (stat === 'def' && !this.isHealthy()) {
                    base *= 1.5;
                }
                break;

            case 'Solar Power':
                // Attaque spéciale *1.5 sous le soleil
                if (stat === 'spa' && weather === 'Sun') {
                    base *= 1.5;
                }
                break;

            case 'Slush Rush':
                // Vitesse doublée sous la grêle
                if (stat === 'spe' && weather === 'Hail') {
                    base *= 2;
                }
                break;

            case 'Swift Swim':
                // Vitesse doublée sous la pluie
                if (stat === 'spe' && weather === 'Rain') {
                    base *= 2;
                }
                break;

            default:
                break;
        }

        return base;
    }

    /** - Soigne le pokemon */
    heal(): void {
        this.pokemon.status = '';
    }

    /** - Renvoie vrai si le pokemon n'a pas de problème de status */
    isHealthy(): boolean {
        return this.pokemon.status === '';
    }

    /**
     * - Définit le boost de la statistique
     * @param stat - Statistique à modifier
     * @param value - Valeur du boost
     */
    setBoost(stat: StatName, value: number): void {
        if (['atk', 'def', 'spa', 'spd', 'spe'].includes(stat)) {
            this.pokemon.boosts[stat] = Math.floor(value);
            if (this.pokemon.boosts[stat] < 0) {
                this.pokemon.boosts[stat] = Math.max(
                    -6,
                    this.pokemon.boosts[stat]
                );
            }
            if (this.pokemon.boosts[stat] > 0) {
                this.pokemon.boosts[stat] = Math.min(
                    6,
                    this.pokemon.boosts[stat]
                );
            }
            this.update();
        }
    }

    /** - Gestion du click sur la capacité */
    abilityClicked() {
        this.addBoosts(this.getAbilityBoost());

        switch (this.pokemon.ability) {
            case 'Levitate':
                this.toogleRoost();
                break;

            case 'Electric Surge':
                if (this.pokemonService.field.terrain === 'Electric') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Electric';
                }
                break;

            case 'Grassy Surge':
                if (this.pokemonService.field.terrain === 'Grassy') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Grassy';
                }
                break;

            case 'Misty Surge':
                if (this.pokemonService.field.terrain === 'Misty') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Misty';
                }
                break;

            case 'Psychic Surge':
                if (this.pokemonService.field.terrain === 'Psychic') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Psychic';
                }
                break;

            case 'Drizzle':
                if (this.pokemonService.field.weather === 'Rain') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Rain';
                }
                break;

            case 'Drought':
                if (this.pokemonService.field.weather === 'Sun') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Sun';
                }
                break;

            case 'Sand Stream':
                if (this.pokemonService.field.weather === 'Sand') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Sand';
                }
                break;

            case 'Snow Warning':
                if (this.pokemonService.field.weather === 'Hail') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Hail';
                }
                break;
        }
        this.update();
    }

    /** - Gestion du click sur l'objet */
    itemClicked() {
        switch (this.pokemon.item) {
            case 'Flame Orb':
                this.pokemon.status = 'brn';
                break;
        }
        this.addBoosts(this.getItemBoost());
        this.looseItem();
        this.update();
    }

    isFaster(): boolean {
        return (
            this.pokemonService.attackerSpeed !== undefined &&
            this.pokemonService.attackingTeamIndex !== this.teamIndex &&
            this.pokemonService.attackerSpeed < this.getStat('spe', true)
        );
    }
    isSlower(): boolean {
        return (
            this.pokemonService.attackerSpeed !== undefined &&
            this.pokemonService.attackingTeamIndex !== this.teamIndex &&
            this.pokemonService.attackerSpeed > this.getStat('spe', true)
        );
    }
    hasSameSpeed(): boolean {
        return (
            this.pokemonService.attackerSpeed !== undefined &&
            this.pokemonService.attackingTeamIndex !== this.teamIndex &&
            this.pokemonService.attackerSpeed === this.getStat('spe', true)
        );
    }

    looseItem(): void {
        this.pokemon.item = '' as ItemName;
    }

    /** - Gestion du click sur un move */
    moveClicked(moveName: string) {
        this.addBoosts(this.moveService.getMoveBoosts(moveName));

        switch (moveName) {
            case 'Roost':
                this.toogleRoost();
                break;

            case 'Growth':
                if (
                    this.field.weather &&
                    (this.field.weather == 'Sun' ||
                        this.field.weather == 'Harsh Sunshine')
                ) {
                    this.addBoosts(this.moveService.getMoveBoosts(moveName));
                }
                break;

            case 'Electric Terrain':
                if (this.pokemonService.field.terrain === 'Electric') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Electric';
                }
                break;

            case 'Grassy Terrain':
                if (this.pokemonService.field.terrain === 'Grassy') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Grassy';
                }
                break;

            case 'Misty Terrain':
                if (this.pokemonService.field.terrain === 'Misty') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Misty';
                }
                break;

            case 'Psychic Terrain':
                if (this.pokemonService.field.terrain === 'Psychic') {
                    this.pokemonService.field.terrain = undefined;
                } else {
                    this.pokemonService.field.terrain = 'Psychic';
                }
                break;

            case 'Sunny Day':
                if (this.pokemonService.field.weather === 'Sun') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Sun';
                }
                break;

            case 'Hail':
                if (this.pokemonService.field.weather === 'Hail') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Hail';
                }
                break;

            case 'Rain Dance':
                if (this.pokemonService.field.weather === 'Rain') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Rain';
                }
                break;

            case 'Sandstorm':
                if (this.pokemonService.field.weather === 'Sand') {
                    this.pokemonService.field.weather = undefined;
                } else {
                    this.pokemonService.field.weather = 'Sand';
                }
                break;
        }
    }

    /**
     * - Renvoie la meilleure statistique du pokemon en ignorant les boosts.
     */
    getBestStat(): StatName {
        let bestStat: StatName = 'atk';
        const list: StatName[] = ['def', 'spa', 'spd', 'spe'];
        list.forEach((stat: StatName) => {
            if (this.getStat(stat) > this.getStat(bestStat)) {
                bestStat = stat;
            }
        });
        return bestStat;
    }

    /**
     * - Renvoie les boost affecté par la capacité
     */
    getAbilityBoost(): Boost {
        const boost: Boost = {
            hp: 0,
            atk: 0,
            def: 0,
            spa: 0,
            spd: 0,
            spe: 0,
        };

        switch (this.pokemon.ability) {
            case 'Beast Boost':
                if (boost.hasOwnProperty(this.getBestStat())) {
                    boost[this.getBestStat()] = 1;
                }
                break;

            case 'Competitive':
                boost.spa = 2;
                break;

            case 'Justified':
                boost.atk = 1;
                break;

            case 'Defiant':
                boost.atk = 2;
                break;

            case 'Moxie':
                boost.atk = 1;
                break;

            case 'Sap Sipper':
                boost.atk = 1;
                break;

            case 'Speed Boost':
                boost.spe = 1;
                break;

            case 'Storm Drain':
                boost.spa = 1;
                break;

            case 'Unburden':
                boost.spe = 2;
                break;

            case 'Weak Armor':
                boost.def = -1;
                boost.spe = 2;
                break;
        }

        return boost;
    }

    /**
     * - Incrémente le boost de la statistique
     * @param stat - Statistique à modifier
     * @param value - Valeur du boost
     */
    addBoost(stat: StatName, value: number): void {
        if (['atk', 'def', 'spa', 'spd', 'spe'].includes(stat)) {
            if (this.pokemon.ability! !== 'Contrary') {
                this.pokemon.boosts[stat] += Math.floor(value);
            } else {
                this.pokemon.boosts[stat] -= Math.floor(value);
            }

            if (this.pokemon.boosts[stat] < 0) {
                this.pokemon.boosts[stat] = Math.max(
                    -6,
                    this.pokemon.boosts[stat]
                );
            }
            if (this.pokemon.boosts[stat] > 0) {
                this.pokemon.boosts[stat] = Math.min(
                    6,
                    this.pokemon.boosts[stat]
                );
            }
            this.update();
        }
    }

    /**
     * - Incrémente les boosts
     * @param boost - Valeur des boosts
     */
    addBoosts(boost: Boost): void {
        Object.keys(boost).forEach((stat) => {
            if (stat !== 'hp') {
                this.addBoost(stat as StatName, boost[stat as StatName]);
            }
        });
    }

    /**
     * - Renvoie les boosts provoqués par l'objet
     */
    getItemBoost() {
        const boost: Boost = {
            hp: 0,
            atk: 0,
            def: 0,
            spa: 0,
            spd: 0,
            spe: 0,
        };

        switch (this.pokemon.item) {
            case 'Weakness Policy':
                boost.atk = 2;
                boost.spa = 2;
                break;

            case 'Electric Seed':
                boost.def = 1;
                break;
        }

        return boost;
    }

    /**
     * - Mise à jour des evs de la statistique
     * @param stat - Nom de la statistique
     */
    updateEV(stat: StatName): void {
        const remainingsEvs = this.getRemainingsEvs();
        if (remainingsEvs < 0) {
            this.pokemon.evs[stat] += remainingsEvs;
            if (this.pokemon.evs[stat] < 0) {
                this.pokemon.evs[stat] = 0;
            }
        }
        this.update();
    }

    /** - Mise à jour des données du pokemon dans le service */
    update(): void {
        const pkmn = new Pokemon(this.pokemonService.gen, this.pokemon.name, {
            name: this.pokemon.name as SpeciesName,
            level: this.pokemon.level,
            ability: this.pokemon.ability,
            item: this.pokemon.item,
            gender: this.pokemon.gender as GenderName,
            nature: this.pokemon.nature,
            ivs: this.pokemon.ivs,
            evs: this.pokemon.evs,
            boosts: this.pokemon.boosts,
            originalCurHP: this.pokemon.originalCurHP,
            status: this.pokemon.status as StatusName,
            moves: this.pokemon.moves,
        });

        this.pokemonService.update(this.teamIndex, this.pokemonIndex, pkmn);
    }

    nextForm() {
        let specie: SpeciesName = this.pokemon.name as SpeciesName;

        // this.pokemonService.update(this.teamIndex, this.pokemonIndex, pkmn);
        // this.pokemonService.nextForm(this.teamIndex, this.pokemonIndex);
        // this.update();
        // console.table(this.pokemon);
        switch (specie) {
            case 'Aegislash-Shield':
                specie = 'Aegislash-Blade' as SpeciesName;
                break;
            case 'Aegislash-Blade':
                specie = 'Aegislash-Shield' as SpeciesName;
                break;

            default:
                console.error(`nextForm(${this.pokemon.name}) undefined`);
                break;
        }

        const pkmn = new Pokemon(this.pokemonService.gen, specie, {
            name: specie,
            level: this.pokemon.level,
            ability: this.pokemon.ability,
            item: this.pokemon.item,
            gender: this.pokemon.gender as GenderName,
            nature: this.pokemon.nature,
            ivs: this.pokemon.ivs,
            evs: this.pokemon.evs,
            boosts: this.pokemon.boosts,
            originalCurHP: this.pokemon.originalCurHP,
            status: this.pokemon.status as StatusName,
            moves: this.pokemon.moves,
        });

        // this.pokemonService.reset(this.teamIndex, this.pokemonIndex, pkmn);
        this.pokemon = pkmn;
        // console.table(this.pokemon);
        // console.table(this.pokemon.moves);
        // console.table(pkmn);
        // console.table(pkmn.moves);

        this.pokemonService.update(this.teamIndex, this.pokemonIndex, pkmn);
    }
}
