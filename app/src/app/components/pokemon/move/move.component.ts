import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '@smogon/calc';
import { TypeName } from '@smogon/calc/dist/data/interface';
import { moves } from 'src/assets/data/pokemon/moves';
import { MoveService } from 'src/app/services/move.service';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
    selector: 'app-move',
    templateUrl: './move.component.html',
    styleUrls: ['./move.component.scss'],
})
export class MoveComponent implements OnInit {
    /** - Nom du move */
    @Input() name!: string;
    /** - Index de l'équipe du move */
    @Input() teamIndex!: number;
    /** - Index de l'équipe du move */
    @Input() pokemonIndex!: number;
    /** - Index du pokemon du move */
    @Input() moveIndex!: number;
    /** - Pokemon du move */
    @Input() pokemon!: Pokemon;
    /** - Données du move */
    data!: any;
    /** - Est un move de contact */
    hasContact!: boolean;

    constructor(
        private moveService: MoveService,
        private pokemonService: PokemonService
    ) {}

    ngOnInit(): void {
        this.data = moves[this.formatedName()];
        this.hasContact = this.data.flags.contact;
    }

    /** - Renvoie le nom du move formatté */
    formatedName(): string {
        return this.moveService.formatName(this.name);
    }

    /** - Renvoie le type du move */
    getType(): TypeName {
        return this.data.type;
    }

    /** - Renvoie la puissance du move */
    getPower(): number {
        return this.data.basePower;
    }

    /** - Renvoie les PP du move */
    getPP(): number {
        if (this.data.pp === 1) return 2;
        if (this.data.pp === 5) return 8;
        if (this.data.pp === 10) return 16;
        if (this.data.pp === 15) return 24;
        if (this.data.pp === 20) return 32;
        if (this.data.pp === 25) return 40;
        if (this.data.pp === 30) return 48;
        if (this.data.pp === 35) return 56;
        // Dernier cas possible: 40 => 64
        return 64;
    }

    /** - Renvoie la description du move */
    getDesc(): string {
        let desc = this.data.shortDesc;
        if (+this.data.accuracy !== 100) {
            desc += `\nAccuracy : ${+this.data['accuracy']}%`;
        }
        desc += `\n${this.getPP()} PP`;
        if(this.hasContact) desc += `\nContact`
        return desc;
    }

    /** - Renvoie la categorie du move */
    getCategory(): string {
        return this.data.category;
    }

    /** - Renvoie l'url de la categorie du move à afficher */
    getCategoryUrl(): string {
        return `assets/images/pokemon/categories/${this.getCategory()}.svg`;
    }

    /** - En mouse over, le move, son pokemon et son équipe deviennent les attaquants */
    mouseenter(): void {
        this.pokemonService.setAttacker(
            this.teamIndex,
            this.pokemonIndex,
            this.moveIndex
        );
    }

    /** - En mouse leave, le move n'attaque plus */
    mouseleave(): void {
        this.pokemonService.unselectMove();
    }
}
