import { Component, OnInit } from '@angular/core';
import { Pokemon } from '@smogon/calc';
import {
    Ability,
    AbilityName,
    StatName,
    TypeName,
} from '@smogon/calc/dist/data/interface';
import { statNames } from 'src/app/models/pokemon/stats';
import { Type, typeNames } from 'src/app/models/pokemon/type';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Router } from '@angular/router';
import { PokemonTypeService } from 'src/app/services/pokemonType.service';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
    /** - Liste des pokemons */
    pokemons!: Pokemon[];
    /** - Liste des pokemons */
    teamB!: Pokemon[];
    /** - Liste des types */
    types = typeNames;
    /** - Liste des stats */
    stats = statNames;

    constructor(
        private router: Router,
        private pokemonService: PokemonService,
        private pokemonType: PokemonTypeService
    ) { }

    ngOnInit(): void {
        this.pokemons = this.pokemonService.pokemons[0];
        this.teamB = this.pokemonService.pokemons[1];

        // Si une des deux équipes n'est pas saisie alors on redirige au formulaire de sélection
        if (
            !this.pokemonService.teams[0].length ||
            !this.pokemonService.teams[1].length
        ) {
            this.router.navigate(['/form']);
        }
    }

    multiplier(
        offensiveType: Type | TypeName,
        defensiveTypes: Type[] | TypeName[],
        ability: AbilityName | undefined
    ): number {
        if (
            (ability === 'Water Absorb' && offensiveType.toLowerCase() === 'water') ||
            (ability === 'Storm Drain' && offensiveType.toLowerCase() === 'water') ||
            (ability === 'Flash Fire' && offensiveType.toLowerCase() === 'fire') ||
            (ability === 'Lightning Rod' && offensiveType.toLowerCase() === 'electric') ||
            (ability === 'Motor Drive' && offensiveType.toLowerCase() === 'electric') ||
            (ability === 'Volt Absorb' && offensiveType.toLowerCase() === 'electric') ||
            (ability === 'Sap Sipper' && offensiveType.toLowerCase() === 'grass') ||
            (ability === 'Levitate' && offensiveType.toLowerCase() === 'ground')
        )
            return 0;
        if (defensiveTypes.length === 1) {
            return this.pokemonType.multiplier(
                offensiveType.toLowerCase() as Type,
                defensiveTypes[0].toLowerCase() as Type
            );
        }

        return (
            this.pokemonType.multiplier(
                offensiveType.toLowerCase() as Type,
                defensiveTypes[0].toLowerCase() as Type
            ) *
            this.pokemonType.multiplier(
                offensiveType.toLowerCase() as Type,
                defensiveTypes[1].toLowerCase() as Type
            )
        );
    }

    getBaseStat(index: number, team: number, statName: StatName): number {
        if (team === 0)
            return this.pokemons[index].species.baseStats[statName];
        return this.teamB[index].species.baseStats[statName];
    }

    getGlobalDefense(index: number, team: number): number {
        if (team === 0)
            return this.pokemons[index].species.baseStats.hp * (this.pokemons[index].species.baseStats.def + this.pokemons[index].species.baseStats.spd) / 200;
        return this.teamB[index].species.baseStats.hp * (this.teamB[index].species.baseStats.def + this.teamB[index].species.baseStats.spd) / 200;
    }

    getAverageStats(index: number, team: number): number {
        if (team === 0)
            return Math.max(this.pokemons[index].species.baseStats.atk, this.pokemons[index].species.baseStats.spa) + this.pokemons[index].species.baseStats.spe + this.getGlobalDefense(index, 0);
        return Math.max(this.teamB[index].species.baseStats.atk, this.teamB[index].species.baseStats.spa) + this.teamB[index].species.baseStats.spe + this.getGlobalDefense(index, 1);
    }


    badAttack(index: number, team: number): 'atk' | 'spa' {
        if (team === 0) {
            if (this.pokemons[index].species.baseStats.atk >= this.pokemons[index].species.baseStats.spa)
                return 'spa';
            return 'atk';
        } else {
            if (this.teamB[index].species.baseStats.atk >= this.teamB[index].species.baseStats.spa)
                return 'spa';
            return 'atk';
        }
    }
}
