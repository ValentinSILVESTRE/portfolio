import { Component, Input, OnInit } from '@angular/core';
import { Pokemon, StatName } from '@smogon/calc';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
    selector: 'app-boost',
    templateUrl: './boost.component.html',
    styleUrls: ['./boost.component.scss'],
})
export class BoostComponent implements OnInit {
    @Input() pokemon!: Pokemon;
    @Input() teamIndex!: number;
    @Input() pokemonIndex!: number;
    boostStatsName: string[] = [...this.pokemonService.statsName];

    constructor(private pokemonService: PokemonService) {}

    ngOnInit(): void {
        // On supprime la statistique hp qui ne peut pas être boosté
        this.boostStatsName.shift();
    }

    incrementBoost(stat: string | StatName): void {
        this.pokemonService.addBoost(
            stat as StatName,
            1,
            this.pokemonService.pokemons[this.teamIndex][this.pokemonIndex]
        );
    }

    decrementBoost(stat: string | StatName): void {
        this.pokemonService.addBoost(
            stat as StatName,
            -1,
            this.pokemonService.pokemons[this.teamIndex][this.pokemonIndex]
        );
    }

    getBoosts() {
        return this.pokemon.boosts;
    }

    getBoost(statName: string) {
        return this.pokemon.boosts[statName as StatName];
    }

    getBoostedStats(): string[] {
        let res: string[] = [];
        this.boostStatsName.forEach((stat) => {
            if (this.getBoost(stat)) {
                res.push(stat);
            }
        });
        return res;
    }

    getUpBoostedStats(): string[] {
        let res: string[] = [];
        this.boostStatsName.forEach((stat) => {
            if (this.getBoost(stat) > 0) {
                res.push(stat);
            }
        });
        return res;
    }

    getDownBoostedStats(): string[] {
        let res: string[] = [];
        this.boostStatsName.forEach((stat) => {
            if (this.getBoost(stat) < 0) {
                res.push(stat);
            }
        });
        return res;
    }
}
