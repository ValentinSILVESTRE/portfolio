import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sub-image',
    templateUrl: './sub-image.component.html',
    styleUrls: ['./sub-image.component.scss'],
})
export class SubImageComponent implements OnInit {
    @Input() id!: number;

    constructor() {}

    ngOnInit(): void {}
}
