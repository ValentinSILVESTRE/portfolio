import { Component, Input, OnInit } from '@angular/core';
import { Project } from 'src/app/models/homepage/project.model';
import { ProjectService } from 'src/app/services/project.service';

@Component({
    selector: 'app-project-preview',
    templateUrl: './project-preview.component.html',
    styleUrls: ['./project-preview.component.scss'],
})
export class ProjectPreviewComponent implements OnInit {
    @Input() project!: Project;

    constructor(private projectService: ProjectService) {}

    ngOnInit(): void {}

    onOpenPopup(): void {
        if (this.project.id !== undefined) {
            this.projectService.setActivePopupId(this.project.id);
        }
    }
}
