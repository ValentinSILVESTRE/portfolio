import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from 'src/app/models/homepage/project.model';
import { getTagIconSource, Tag } from 'src/app/models/homepage/tag';
import { ProjectService } from 'src/app/services/project.service';

@Component({
    selector: 'app-project-popup',
    templateUrl: './project-popup.component.html',
    styleUrls: ['./project-popup.component.scss'],
})
export class ProjectPopupComponent implements OnInit {
    @Input() project!: Project;
    @Output() closePopup = new EventEmitter();

    constructor(
        private projectService: ProjectService,
        private router: Router
    ) {}

    ngOnInit(): void {}

    getTagIconSource(tag: Tag): string {
        return getTagIconSource(tag);
    }

    onClosePopup(): void {
        this.projectService.removeActivePopupId();
        this.closePopup.emit(true);
    }
}
