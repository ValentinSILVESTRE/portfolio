import { Component, HostListener, OnInit } from '@angular/core';
import { Project } from 'src/app/models/homepage/project.model';
import { ProjectService } from 'src/app/services/project.service';

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnInit {
    projects!: Project[];
    isActivePopup: boolean = false;

    constructor(public projectService: ProjectService) {}

    ngOnInit(): void {
        this.projects = this.projectService.projects;
        this.projectService.removeActivePopupId();
    }

    onClosePopup(): void {
        this.isActivePopup = false;
        this.projectService.removeActivePopupId();
    }

    onOpenPopup(): void {
        this.isActivePopup = true;
    }

    // Méthode appelée quand on appui sur escape
    @HostListener('window:keydown.escape', ['$event'])
    onEscapeClick() {
        this.onClosePopup();
    }
}
