import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-mine',
    templateUrl: './mine.component.html',
    styleUrls: ['./mine.component.scss', './mine.css'],
})
export class MineComponent implements OnInit {
    @Input() isMine!: boolean;
    @Input() isReveal!: boolean;
    @Input() isFlagged!: boolean;
    @Input() closeMineCount!: number;
    @Input() mineIndex!: number;

    constructor() {}

    ngOnInit(): void {}
}
