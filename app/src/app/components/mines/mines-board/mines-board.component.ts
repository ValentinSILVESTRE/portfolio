import { Component, OnInit } from '@angular/core';
import { Board } from 'src/app/models/mines/board.model';
import { Cell } from 'src/app/models/mines/cell.model';
import { Position } from 'src/app/models/position.model';

@Component({
    selector: 'app-mines-board',
    templateUrl: './mines-board.component.html',
    styleUrls: ['./mines-board.component.scss'],
})
export class MinesBoardComponent implements OnInit {
    board!: Board;
    isInitialized: boolean = false;
    mineCount: number = 10;
    height: number = 8;
    length: number = 8;

    constructor() {}

    ngOnInit(): void {
        this.setDifficulty('easy');
        this.board = new Board(this.height, this.length, this.mineCount);
    }

    setDifficulty(difficulty: 'easy' | 'medium' | 'hard'): void {
        switch (difficulty) {
            case 'easy':
                this.height = 8;
                this.length = 8;
                this.mineCount = 10;
                break;

            case 'medium':
                this.height = 16;
                this.length = 16;
                this.mineCount = 40;
                break;

            default:
                this.height = 30;
                this.length = 16;
                this.mineCount = 99;
                break;
        }
        this.onNewGame();
    }

    getGridTemplateColumns(): string {
        return `repeat(${this.board.getLength()}, 1fr)`;
    }
    getGridTemplateRows(): string {
        return `repeat(${this.board.getHeight()}, 1fr)`;
    }

    getHeightCss(): string {
        if (this.board.getHeight() >= this.board.getLength()) {
            return '80vmin';
        }
        return (
            (
                (80 * this.board.getHeight()) /
                this.board.getLength()
            ).toString() + 'vmin'
        );
    }
    getWidthCss(): string {
        if (this.board.getHeight() <= this.board.getLength()) {
            return '80vmin';
        }
        return (
            (
                (80 * this.board.getLength()) /
                this.board.getHeight()
            ).toString() + 'vmin'
        );
    }

    onClick(mineIndex: number): void {
        this.initialize(mineIndex);
        this.board.revealReccursive(this.board.getPositionOfIndex(mineIndex));
    }

    onAuxclick(e: MouseEvent, mineIndex: number): void {
        if (
            !this.board.getIsFinish() &&
            !this.board
                .getValue(this.board.getPositionOfIndex(mineIndex))
                .getIsReveal()
        ) {
            this.board.toggleFlag(this.board.getPositionOfIndex(mineIndex));
        }
    }

    /** - Méthode qui empêche d'ouvrir le menu avec un clique droit */
    preventContextMenu(e: MouseEvent) {
        e.preventDefault();
    }

    onTest(): void {
        const position: Position = { x: 1, y: 1 };
        this.board.setValue(position, new Cell(true, true));
    }

    initialize(mineIndex: number): void {
        if (!this.isInitialized) {
            this.isInitialized = true;
            this.board.initialize(this.board.getPositionOfIndex(mineIndex));
        }
    }

    onNewGame(): void {
        this.isInitialized = false;
        this.board = new Board(this.height, this.length, this.mineCount);
    }

    isMine(mineIndex: number): boolean {
        return this.board
            .getValue(this.board.getPositionOfIndex(mineIndex))
            .getIsMine();
    }
}
