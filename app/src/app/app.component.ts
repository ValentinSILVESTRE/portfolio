import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    title = 'Portfolio de Valentin SILVESTRE';
    description = 'Découvrez mon portfolio';
    keywords = 'Portfolio, Angular, Symfony, Fullstack';
    constructor(private titleService: Title, private metaService: Meta) {}

    ngOnInit(): void {
        this.titleService.setTitle(this.title);
        this.metaService.addTags([
            { name: 'description', content: this.description },
            { name: 'keywords', content: this.keywords },
        ]);
    }
}
