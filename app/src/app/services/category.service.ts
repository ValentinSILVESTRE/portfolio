import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from '../models/calendar/category.model';
import { ResourceService } from './resource.service';
import { ServerService } from './server.service';

@Injectable({
    providedIn: 'root',
})
export class CategoryService extends ResourceService<Category> {
    constructor(http: HttpClient, serverService: ServerService) {
        super(http, 'category', serverService);
    }

    iconIsImage(categoryName: string): boolean {
        return ['starcraft', 'warcraft', 'age of empire'].includes(
            categoryName.toLowerCase()
        );
    }

    getIcon(categoryName: string): string {
        switch (categoryName.toLowerCase()) {
            case 'sport':
                return 'sports_football';
            case 'rugby':
                return 'sports_rugby';
            case 'handball':
                return 'sports_handball';
            case 'football':
                return 'sports_soccer';
            case 'basket':
                return 'sports_basketball';
            case 'formule 1':
                return 'sports_motorsports';
            case 'jeux vidéos':
                return 'sports_esports';
            case 'starcraft':
                return 'assets/icons/sc2.png';
            case 'warcraft':
                return 'assets/icons/wc3.png';
            case 'age of empire':
                return 'assets/icons/aoe.png';
            case 'politique':
                return 'account_balance';
            case 'divertissement':
                return 'casino';
            case 'informatique':
                return 'code';
            case 'professionnel':
                return 'work';
            case 'autre':
                return 'star_rate';
            case 'randonnées':
                return 'hiking';
            case 'anniversaires':
                return 'celebration';

            default:
                console.error(
                    `categoryService.getMaterialIcon(${categoryName}) n'a pas trouvé`
                );
                return 'report_problem';
        }
    }

    getImageSource(categoryName: string): string {
        switch (categoryName.toLowerCase()) {
            case 'starcraft':
                return 'assets/icons/sc2.png';
            case 'warcraft':
                return 'assets/icons/wc3.png';
            case 'age of empire':
                return 'assets/icons/aoe.png';
        }
        return '';
    }
}
