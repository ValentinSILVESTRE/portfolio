import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isNow } from 'src/assets/functions/date.functions';
import { playSound } from 'src/assets/functions/sound.functions';
import { Evenement } from '../models/calendar/evenement.model';
import { ResourceService } from './resource.service';
import { ServerService } from './server.service';

@Injectable({
    providedIn: 'root',
})
export class EventService extends ResourceService<Evenement> {
    events: Evenement[] = [];
    constructor(http: HttpClient, serverService: ServerService) {
        super(http, 'event', serverService);
        this.getEvents();
        this.playSound();
    }

    protected getEvents(): void {
        const listSubscription = this.getAll().subscribe({
            next: (data) => {
                this.events = data;
            },
            error: (error) => {
                console.error(
                    `Erreur dans getEvents() de event-service.ts`,
                    error
                );
                listSubscription.unsubscribe();
            },
            complete: () => {
                listSubscription.unsubscribe();
            },
        });
    }

    playSound(): void {
        // setInterval(() => {
        //     console.log(new Date());
        //     for (const event of this.events) {
        //         if (isNow(new Date(event.date))) {
        //             playSound('assets/sounds/non.mp4');
        //         }
        //     }
        // }, 1000);
    }

    getAncestors(event: Evenement): Evenement[] {
        const ancestors: Evenement[] = [];
        let currentEvent = event;
        while (currentEvent.parentEventId) {
            const parentEvent = this.getEventById(currentEvent.parentEventId)!;
            ancestors.push(parentEvent);
            currentEvent = parentEvent;
        }
        return ancestors.reverse();
    }

    getAllFuturesEvents(): Observable<Evenement[]> {
        return this.http.get<Evenement[]>(`${this.url}/future`);
    }

    getEventsByTitle(query: string) {
        return this.http.get<Evenement[]>(`${this.url}/${query}`);
    }

    override update(event: Evenement): Observable<Evenement> {
        return this.http.put<Evenement>(
            `${this.url}/${event.id}`,
            {
                title: event.title,
                description: event.description,
                date: event.date,
                parentEventId: event.parentEventId,
                categories: event.categories,
            },
            this.httpOptions
        );
    }

    /**
     * - Renvoie la liste des event: racine + descendance réccursivement
     * @param event - La racine
     */
    getFamily(event: Evenement): Evenement[] {
        return [event, ...event.subEvents.map((e) => this.getFamily(e))].flat();
    }

    getEventById(id: number): Evenement | null {
        return this.events.find((event) => event.id === id) || null;
    }

    isPassed(event: Evenement): boolean {
        return new Date(event.date) < new Date();
    }
}
