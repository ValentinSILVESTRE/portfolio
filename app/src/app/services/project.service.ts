import { Injectable } from '@angular/core';
import { Project } from '../models/homepage/project.model';
import { Tag } from '../models/homepage/tag';

@Injectable({
    providedIn: 'root',
})
export class ProjectService {
    activePopupId?: number;
    projects: Project[] = [
        {
            name: 'mhb',
            description: `Stage de fin de formation.
            Outil collaboratif permettant de supperviser l'avancée du projet RSE de l'entreprise.`,
            url: 'https://crm.valentin-silvestre.com',
            externalUrl: true,
            imageUrl: 'assets/images/projects/screenshots/mhb.png',
            imageUrlBis: 'assets/images/projects/screenshots/mhb.png',
            imageAlt: `Page d'acceuil du projet RSE`,
            isFinish: true,
            tags: [Tag.Symfony, Tag.Php, Tag.Javascript, Tag.Html, Tag.Css],
            links: [
                {
                    title: 'Vidéo de présentation',
                    url: 'https://www.youtube.com/watch?v=dTtWr5UaZzw',
                    hasIcon: true,
                    iconSrc: 'assets/icons/youtube.png',
                    iconAlt: 'Logo de Youtube',
                },
            ],
        },
        {
            name: 'calendrier',
            description: `Calendrier`,
            url: '/calendar',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/calendar.png',
            imageUrlBis: 'assets/images/projects/screenshots/calendar.png',
            imageAlt: 'Calendrier avec des événements affichés en ligne',
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.Symfony,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.Php,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'taquin',
            description: `Puzzle où il faut déplacer les pièces pour recomposer une image.
            Jouable à la souris ou avec les touches directionnelles.`,
            url: '/taquin',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/taquin.png',
            imageUrlBis: 'assets/images/projects/screenshots/taquin.png',
            imageAlt: 'Image découpée en 9 morceaux à recomposer',
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'pokemon',
            description: `Application permettant de calculer les dégats que se font les pokemons.
            Inspiré du site https://calc.pokemonshowdown.com/ et utilisant les packages de pokemon showdown, notement pour les calculs de dégats et utilise l'api http://play.pokemonshowdown.com/data/ pour récupérer les sets (attaques, capacité, nature et répartition des evs) les plus utilisés.`,
            url: '/form',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/pokemon.png',
            imageUrlBis: 'assets/images/projects/screenshots/pokemon.png',
            imageAlt: `Deux équipes de pokemons s'affrontants`,
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.Symfony,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.Php,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'puissance 4',
            description: `Jeu de société à deux joueurs où le but est d'aligner 4 pions de notre couleur avant l'adversaire.

            Possibilité de jouer contre une IA.`,
            url: '/puissance4',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/puissance4.png',
            imageUrlBis:
                'assets/images/projects/screenshots/puissance4-bis.png',
            imageAlt: 'Jeu de puissance 4',
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.IA,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'mondrian',
            description:
                'Une reproduction du tableau de Piet Mondrian en utilisant la propriété css display : grid',
            url: '/mondrian',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/mondrian.png',
            imageUrlBis: 'assets/images/projects/screenshots/mondrian-bis.png',
            imageAlt: 'Deux dessins composés de rectangles colorés',
            isFinish: true,
            tags: [Tag.Html, Tag.Css],
            links: [],
        },
        {
            name: 'mines',
            description: `Jeux vidéos où le but est déminer le terrain.
                On révele une case en cliquant dessus et si ce n'est pas une mine, alors il est indiqué le nombre cases adjacentes contenant une mine.
                
                Le clique droit permet d'indiquer que la case contient une mine`,
            url: '/mines',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/mines.png',
            imageUrlBis: 'assets/images/projects/screenshots/mines-bis.png',
            imageAlt:
                'Grille avec des mines ou le nombre de mines sur les cases adjacentes',
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'Jeu de la vie',
            description: `Le jeu de la vie est un automate cellulaire imaginé par John Horton Conway en 1970.
            Le plateau est composé de cellules vivantes (en clair) et mortes (en sombre), et à chaque tour leur statut évolue en fonction de leur nombre de voisin :
            une cellule morte possédant exactement trois cellules voisines vivantes devient vivante (elle naît).
            une cellule vivante possédant deux ou trois cellules voisines vivantes le reste, sinon elle meurt.
            `,
            url: '/gameoflife',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/gameOfLife.png',
            imageUrlBis:
                'assets/images/projects/screenshots/gameOfLife-bis.png',
            imageAlt: 'Grille avec quelques cases foncés et un bouton start',
            isFinish: true,
            tags: [
                Tag.Angular,
                Tag.TypeScript,
                Tag.Javascript,
                Tag.Html,
                Tag.Css,
            ],
            links: [],
        },
        {
            name: 'cryptographie',
            description: '',
            url: '/cryptography',
            externalUrl: false,
            imageUrl: 'assets/images/projects/screenshots/cryptography.png',
            imageUrlBis: 'assets/images/projects/screenshots/cryptography.png',
            imageAlt: 'Un message chiffré et le meme décodé',
            isFinish: true,
            tags: [Tag.Html, Tag.Css, Tag.Javascript],
            links: [],
        },
    ];

    constructor() {
        this.projects.forEach((project, projectIndex) => {
            project.id = projectIndex;
        });
    }

    hasActivePopupId(): boolean {
        return typeof this.activePopupId === 'number';
    }

    getActivePopupId(): number | undefined {
        if (this.hasActivePopupId()) {
            return this.activePopupId;
        }
        return undefined;
    }

    setActivePopupId(id: number): this {
        this.activePopupId = id;
        return this;
    }

    removeActivePopupId(): this {
        this.activePopupId = undefined;
        return this;
    }

    getProject(index: number): Project {
        return this.projects[index];
    }
}
