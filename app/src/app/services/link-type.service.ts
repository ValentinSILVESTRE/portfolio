import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LinkType } from '../models/calendar/linkType.model';
import { ResourceService } from './resource.service';
import { ServerService } from './server.service';

@Injectable({
    providedIn: 'root',
})
export class LinkTypeService extends ResourceService<LinkType> {
    linkTypes: LinkType[] = [];
    constructor(http: HttpClient, serverService: ServerService) {
        super(http, 'linktype', serverService);
        this.getLinkTypes();
    }

    getMaterialIconsName(linkTypeName: string): string {
        switch (linkTypeName) {
            case 'live':
                return 'live_tv';
            case 'replay':
                return 'fast_rewind';
            case 'info':
                return 'info';

            default:
                return 'warning';
        }
    }

    protected getLinkTypes(): void {
        const listSubscription = this.getAll().subscribe({
            next: (data) => {
                this.linkTypes = data;
            },
            error: (error) => {
                console.error(
                    `Erreur dans getLinkTypes() de link-type-service.ts`,
                    error
                );
                listSubscription.unsubscribe();
            },
            complete: () => {
                listSubscription.unsubscribe();
            },
        });
    }

    getByValue(linkTypeName: 'live' | 'replay' | 'info'): LinkType {
        return this.linkTypes.filter(
            (linkType) => linkType.name === linkTypeName
        )[0];
    }

    getById(linkTypeId: 1 | 2 | 3): LinkType {
        return this.linkTypes.filter(
            (linkType) => linkType.id === linkTypeId
        )[0];
    }
}
