import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class ServerService {
    constructor() {}

    /** - Url du server */
    url = 'http://127.0.0.1:8000';
    // url = 'https://api.valentin-silvestre.com';
}
