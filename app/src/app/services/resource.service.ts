import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Created } from '../models/created.model';
import { Resource } from '../models/resource.model';
import { ServerService } from './server.service';

@Injectable({
    providedIn: 'root',
})
export class ResourceService<T extends Resource> {
    constructor(
        protected http: HttpClient,
        @Inject(String) private endpoint: string,
        private serverService: ServerService
    ) {}
    protected url = `${this.serverService.url}/${this.endpoint}`;
    /** Options http autorisants le CORS notament */
    protected httpOptions = {
        headers: new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            Authorization: 'my-auth-token',
        }),
    };

    getAll(): Observable<T[]> {
        return this.http.get<T[]>(`${this.url}`);
    }
    get(id: number): Observable<T> {
        return this.http.get<T>(`${this.url}/${id}`, this.httpOptions);
    }
    post(resource: Resource): Observable<T> {
        return this.http.post<T>(`${this.url}`, resource, this.httpOptions);
    }
    update(resource: Resource): Observable<T> {
        return this.http.put<T>(
            `${this.url}/${resource.id}`,
            resource,
            this.httpOptions
        );
    }
    delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.url}/${id}`, this.httpOptions);
    }
}
