import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    constructor(private http: HttpClient) {}

    login(email: string, password: string) {
        // return (
        //     this.http
        //         .post<User>('/api/login', { email, password })
        //         // this is just the HTTP call,
        //         // we still need to handle the reception of the token
        //         .shareReplay()
        // );
    }

    private loginUrl = 'http://localhost:8000/login_check';

    public auth(username: string, password: string): Observable<any> {
        const body = {
            username: username,
            password: password,
        };
        return this.http.post(this.loginUrl, body);
    }

    public getToken() {
        const jwt = localStorage.getItem('jwt');
        return jwt;
    }
}
