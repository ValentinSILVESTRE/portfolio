import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Link } from '../models/calendar/link.model';
import { ResourceService } from './resource.service';
import { ServerService } from './server.service';

@Injectable({
    providedIn: 'root',
})
export class LinkService extends ResourceService<Link> {
    constructor(http: HttpClient, serverService: ServerService) {
        super(http, 'link', serverService);
    }

    override post(link: Link): Observable<Link> {
        return this.http.post<Link>(
            `${this.url}`,
            {
                url: this.formatUrl(link.url),
                title: link.title,
                type: link.type.id,
                event: link.event.id,
            },
            this.httpOptions
        );
    }

    override update(link: Link): Observable<Link> {
        return this.http.put<Link>(
            `${this.url}/${link.id}`,
            {
                url: this.formatUrl(link.url),
                title: link.title || '',
                type: link.type.id,
                event: link.event.id,
            },
            this.httpOptions
        );
    }

    formatUrl(url: string): string {
        if (!url.match(/^https?:\/\//)) return 'https://' + url;
        return url;
    }
}
