import { Injectable } from '@angular/core';
import { moves } from '../../assets/data/pokemon/moves';
import { Boost } from '../models/pokemon/boost';

@Injectable({
    providedIn: 'root',
})
export class MoveService {
    constructor() {}

    /**
     * - Renvoie le nom du move formatté
     * @param moveName - Nom du move à formatter
     * @returns
     */
    formatName(moveName: string): string {
        return moveName
            .toLowerCase()
            .replaceAll("'", '')
            .replaceAll(' ', '')
            .replaceAll('-', '');
    }

    getMove(moveName: string): any {
        return moves[this.formatName(moveName)];
    }

    getMoveBoosts(moveName: string): Boost {
        const move: any = this.getMove(moveName);
        let moveBoosts: Boost = {
            hp: 0,
            atk: 0,
            def: 0,
            spa: 0,
            spd: 0,
            spe: 0,
        };

        if (move.boosts) {
            moveBoosts = move.boosts;
        }
        if (move.self && move.self.boosts) {
            moveBoosts = move.self.boosts;
        }
        if (
            move.secondary &&
            move.secondary.self &&
            move.secondary.self.boosts
        ) {
            moveBoosts = move.secondary.self.boosts;
        }

        return moveBoosts;
    }
}
