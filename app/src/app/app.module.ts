import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestComponent } from './test/test.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';

import { nl2brPipe } from './pipes/nl2br.pipe';

import { FightComponent } from './components/pokemon/fight/fight.component';
import { PokemonComponent } from './components/pokemon/pokemon/pokemon.component';
import { TypeComponent } from './components/pokemon/type/type.component';
import { MoveComponent } from './components/pokemon/move/move.component';
import { DamageComponent } from './components/pokemon/damage/damage.component';
import { StatusComponent } from './components/pokemon/status/status.component';
import { BoostComponent } from './components/pokemon/boost/boost.component';
import { FormComponent } from './components/pokemon/form/form.component';
import { HomepageComponent } from './components/home/homepage/homepage.component';
import { ProjectPreviewComponent } from './components/home/project-preview/project-preview.component';
import { MondrianComponent } from './components/mondrian/mondrian.component';
import { HeaderComponent } from './components/header/header.component';
import { BoardComponent } from './components/gameoflife/board/board.component';
import { GameOfLifeCellComponent } from './components/gameoflife/gameoflifeCell/gameoflifeCell.component';
import { Connect4Component } from './components/connect4/connect4/connect4.component';
import { Connect4cellComponent } from './components/connect4/connect4cell/connect4cell.component';
import { MineComponent } from './components/mines/mine/mine.component';
import { MinesBoardComponent } from './components/mines/mines-board/mines-board.component';
import { TaquinComponent } from './components/taquin/taquin.component';
import { ProjectPopupComponent } from './components/home/project-popup/project-popup.component';
import { MainCalendarComponent } from './components/calendar/main-calendar/main-calendar.component';
import { FooterComponent } from './components/footer/footer.component';
import { SubImageComponent } from './components/sub-image/sub-image.component';
import { EventComponent } from './components/calendar/event/event.component';
import { EventPostComponent } from './components/calendar/event-post/event-post.component';
import { EventUpdateComponent } from './components/calendar/event-update/event-update.component';
import { LinkPostComponent } from './components/calendar/link-post/link-post.component';
import { SearchComponent } from './components/calendar/search/search.component';
import { MainComponent } from './components/cryptography/main/main.component';
import { LoadingComponent } from './components/loading/loading.component';
import { CvComponent } from './components/cv/cv.component';
import { TableComponent } from './components/pokemon/table/table.component';
import { LoginComponent } from './components/login/login.component';
import { ImageChoiceComponent } from './components/taquin/image-choice/image-choice.component';

@NgModule({
    declarations: [
        AppComponent,
        FightComponent,
        PokemonComponent,
        TypeComponent,
        MoveComponent,
        DamageComponent,
        StatusComponent,
        BoostComponent,
        FormComponent,
        TestComponent,
        HomepageComponent,
        ProjectPreviewComponent,
        MondrianComponent,
        HeaderComponent,
        BoardComponent,
        GameOfLifeCellComponent,
        Connect4Component,
        Connect4cellComponent,
        MineComponent,
        MinesBoardComponent,
        TaquinComponent,
        ProjectPopupComponent,
        nl2brPipe,
        MainCalendarComponent,
        FooterComponent,
        SubImageComponent,
        EventComponent,
        EventPostComponent,
        EventUpdateComponent,
        LinkPostComponent,
        SearchComponent,
        MainComponent,
        LoadingComponent,
        CvComponent,
        TableComponent,
        LoginComponent,
        ImageChoiceComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        HttpClientJsonpModule,
        FormsModule,
        MatChipsModule,
        MatDividerModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatIconModule,
        MatButtonModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor() {}
}
