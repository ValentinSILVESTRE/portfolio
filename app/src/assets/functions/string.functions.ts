/**
 * - Renvoie une nouvelle chaîne de caractère avec une majuscule au début
 * @param {String} text:string - Chaîne de caractères
 * @returns {String}
 */
export function capitalize(text: string): string {
    let res = text;
    let t = res.split('');
    t[0] = t[0].toUpperCase();
    res = t.join('');
    return res;
}
