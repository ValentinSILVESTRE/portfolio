/**
 * - Joue le son
 * @param {String} url - lien du son
 * @param {number|null} duration - durée du son à jouer
 * @param {number} startAt - nombre de secondes à ignorer avant de jouer le son
 */
export function playSound(
    url: string,
    duration: number = 0,
    startAt: number = 0
) {
    const audio = new Audio(url);
    audio.addEventListener('loadeddata', () => {
        if (0 <= startAt && startAt < audio.duration) {
            audio.currentTime = startAt;
        }
        audio.volume = 0.3;
        audio.play();

        if (duration > 0 && startAt + duration <= audio.duration) {
            setInterval(() => {
                audio.pause();
            }, 1000 * duration);
        }
    });
}
