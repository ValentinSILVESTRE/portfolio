export const offsetDecryption = (offset: number, encryptedMessage: string) => {
    return encryptedMessage
        .replace(/([a-z])/g, (c) =>
            String.fromCharCode(
                'a'.charCodeAt(0) +
                    ((c.charCodeAt(0) -
                        'a'.charCodeAt(0) -
                        (offset % 26) +
                        26) %
                        26)
            )
        )
        .replace(/([A-Z])/g, (c) =>
            String.fromCharCode(
                'A'.charCodeAt(0) +
                    ((c.charCodeAt(0) -
                        'A'.charCodeAt(0) -
                        (offset % 26) +
                        26) %
                        26)
            )
        );
};
