<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LinkRepository::class)]
class Link
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['link', 'event'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    #[Groups(['link', 'event'])]
    private $title;

    #[ORM\Column(type: 'string', length: 400)]
    #[Groups(['link', 'event'])]
    private $url;

    #[ORM\ManyToOne(targetEntity: LinkType::class, inversedBy: 'links')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['link', 'event'])]
    private $type;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'links')]
    #[Groups(['link'])]
    private $event;

    public function hidrate(array $array)
    {
        foreach ($array as $attibute => $value) {
            $setter = $this->getSetter($attibute);
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }

    public function getGetter(string $attribute): string
    {
        return 'get' . ucfirst($attribute);
    }

    public function getSetter(string $attribute): string
    {
        return 'set' . ucfirst($attribute);
    }

    public function __toString(): String
    {
        switch ($this->getType()->getName()) {
            case 'live':
                return 'live_tv';

            case 'info':
                return 'info';

            case 'replay':
                return 'reset_tv';

            default:
                return 'live_tv';
        };
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getType(): ?LinkType
    {
        return $this->type;
    }

    public function setType(?LinkType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function removeEvent(): self
    {
        $this->setEvent(null);

        return $this;
    }
}
