<?php

namespace App\Entity;

use App\Repository\EventRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['event', 'category', 'link'])]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    #[Groups(['event', 'category'])]
    private $title;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['event', 'category'])]
    private $date;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['event', 'category'])]
    private $description;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'subEvents')]
    private $parentEvent;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['event', 'category'])]
    private $parentEventId;

    #[ORM\OneToMany(mappedBy: 'parentEvent', targetEntity: self::class, cascade: ["persist", "remove"])]
    #[Groups(['event', 'category'])]
    private $subEvents;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Link::class, cascade: ["persist", "remove"])]
    #[Groups(['event', 'category'])]
    private $links;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'events')]
    #[Groups(['event'])]
    private $categories;

    public function __construct()
    {
        $this->subEvents = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function hidrate(array $array)
    {
        foreach ($array as $attibute => $value) {
            $setter = $this->getSetter($attibute);
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }

    public function getGetter(string $attribute): string
    {
        return 'get' . ucfirst($attribute);
    }

    public function getSetter(string $attribute): string
    {
        return 'set' . ucfirst($attribute);
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParentEvent(): ?self
    {
        return $this->parentEvent;
    }

    public function setParentEvent(?self $parentEvent): self
    {
        $this->parentEvent = $parentEvent;

        return $this;
    }

    public function removeParentEvent(): self
    {
        $this->setParentEvent(null);

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSubEvents(): Collection
    {
        return $this->subEvents;
    }

    public function addSubEvent(self $subEvent): self
    {
        if (!$this->subEvents->contains($subEvent)) {
            $this->subEvents[] = $subEvent;
            $subEvent->setParentEvent($this);
        }

        return $this;
    }

    public function removeSubEvent(self $subEvent): self
    {
        if ($this->subEvents->removeElement($subEvent)) {
            // set the owning side to null (unless already changed)
            if ($subEvent->getParentEvent() === $this) {
                $subEvent->setParentEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        if (count($this->getCategories()) > 0) {
            return $this->getCategories()[0];
        } elseif ($this->getParentEvent() && count($this->getParentEvent()->getCategories()) > 0) {
            return $this->getParentEvent()->getCategories()[0];
        } else {
            return null;
        }
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Liste combinée des liens de l'événement et de ceux de ses parents.
     * @return Link[]
     */
    public function getLinks()
    {
        $res = [];
        foreach ($this->links as $link) {
            $res[] = $link;
        }
        // return array_merge($res, $this->getParentLinks());
        return $this->links;
    }

    /**
     * @return Link[]
     */
    public function getParentLinks()
    {
        if ($this->getParentEvent()) {
            $res = [];
            foreach ($this->getParentEvent()->getLinks() as $link) {
                $res[] = $link;
            }
            return array_merge($res, $this->getParentEvent()->getParentLinks());
        } else {
            return [];
        }
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setEvent($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->removeElement($link)) {
            // set the owning side to null (unless already changed)
            if ($link->getEvent() === $this) {
                $link->setEvent(null);
            }
        }

        return $this;
    }


    public function isPassed()
    {
        $now = new DateTime();
        return $this->getDate() < $now;
    }

    /**
     * Revoie vrai si l'événement s'est déroulé avant aujourd'hui.
     */
    public function beforeToday()
    {
        $now = new DateTime();

        return $this->getDate() < $now &&
            ($this->getDate()->format('Y') !== $now->format('Y')
                || $this->getDate()->format('n') !== $now->format('n')
                || $this->getDate()->format('j') !== $now->format('j')
            );
    }

    public function getAbsoluteDate()
    {
        return date('Y-m-d H:i', $this->getDate()->getTimestamp());
    }

    public function getParentEventId(): ?int
    {
        return $this->parentEventId;
    }

    public function setParentEventId(?int $parentEventId): self
    {
        $this->parentEventId = $parentEventId;

        return $this;
    }

    /**
     * - Renvoie vrai si l'instance courante a pour enfant (à n'importe quel niveau) un Event dont l'id est celui passé en paramètre
     */
    public function isParentOf(int $childEventId): bool
    {
        // dd($this, $this->getSubEvents());
        // S'il n'a pas d'enfant on renvoie faux
        if (count($this->getSubEvents()) == 0) return false;

        // On cherche dans ses enfants direct et récursivement
        for ($i = 0; $i < count($this->getSubEvents()); $i++) {
            $childEvent = $this->getSubEvents()[$i];
            if (
                $childEvent->getId() == $childEventId ||
                $childEvent->isParentOf($childEventId)
            ) return true;
        }

        // Si on n'a pas trouvé, alors on renvoie faux
        return false;
    }
}
