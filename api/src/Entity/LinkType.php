<?php

namespace App\Entity;

use App\Repository\LinkTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: LinkTypeRepository::class)]
class LinkType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['link', 'linkType', 'event'])]
    private $id;

    #[ORM\Column(type: 'string', length: 20)]
    #[Groups(['link', 'linkType', 'event'])]
    private $name;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Link::class, cascade: ["persist", "remove"])]
    private $links;

    public function __construct($name)
    {
        $this->links = new ArrayCollection();
        $this->setName($name);
    }

    public function __toString(): String
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setType($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->removeElement($link)) {
            // set the owning side to null (unless already changed)
            if ($link->getType() === $this) {
                $link->setType(null);
            }
        }

        return $this;
    }
}
