<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['event', 'category'])]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    #[Groups(['event', 'category'])]
    private $name;

    #[ORM\ManyToMany(targetEntity: Event::class, mappedBy: 'categories')]
    #[Groups(['category'])]
    private $events;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'subCategories')]
    private $parentCategory;

    #[ORM\OneToMany(mappedBy: 'parentCategory', targetEntity: self::class)]
    #[Groups(['category'])]
    private $subCategories;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->subCategories = new ArrayCollection();
    }

    public function hidrate(array $array)
    {
        foreach ($array as $attibute => $value) {
            $setter = $this->getSetter($attibute);
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }

    public function getGetter(string $attribute): string
    {
        return 'get' . ucfirst($attribute);
    }

    public function getSetter(string $attribute): string
    {
        return 'set' . ucfirst($attribute);
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addCategory($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            $event->removeCategory($this);
        }

        return $this;
    }


    /**
     * - Renvoie vrai si l'instance courante a pour enfant (à n'importe quel niveau) une catégorie dont l'id est celui passé en paramètre
     */
    public function isParentOf(int $childEventId): bool
    {
        // S'il n'a pas d'enfant on renvoie faux
        if (count($this->getSubCategories()) == 0) return false;

        // On cherche dans ses enfants direct et récursivement
        for ($i = 0; $i < count($this->getSubCategories()); $i++) {
            $childEvent = $this->getSubCategories()[$i];
            if (
                $childEvent->getId() == $childEventId ||
                $childEvent->isParentOf($childEventId)
            ) return true;
        }

        // Si on n'a pas trouvé, alors on renvoie faux
        return false;
    }

    public function getParentCategory(): ?self
    {
        return $this->parentCategory;
    }

    public function setParentCategory(?self $parentCategory): self
    {
        $this->parentCategory = $parentCategory;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getSubCategories(): Collection
    {
        return $this->subCategories;
    }

    public function addSubCategory(self $subCategory): self
    {
        if (!$this->subCategories->contains($subCategory)) {
            $this->subCategories[] = $subCategory;
            $subCategory->setParentCategory($this);
        }

        return $this;
    }

    public function removeSubCategory(self $subCategory): self
    {
        if ($this->subCategories->removeElement($subCategory)) {
            // set the owning side to null (unless already changed)
            if ($subCategory->getParentCategory() === $this) {
                $subCategory->setParentCategory(null);
            }
        }

        return $this;
    }
}
