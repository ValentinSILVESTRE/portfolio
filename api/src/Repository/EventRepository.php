<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * Find all Events in repository order by date
     * @return Event[] Returns an array of Event objects
     */
    public function findByTitle(string $query)
    {
        $sort = 'ASC';

        return $this->createQueryBuilder('e')
            ->where('e.title LIKE :query')
            ->setParameter('query', "%$query%")
            ->orderBy('e.date', $sort)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find all futures Events in repository order by date
     * @return Event[] Returns an array of Event objects
     */
    public function findAllFutures($option = [])
    {
        $sort = 'ASC';
        if ($option !== [] && $option['sort'] === 'newer') {
            $sort = 'DESC';
        }

        return $this->createQueryBuilder('e')
            ->where('e.date >= :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('e.date', $sort)
            ->getQuery()
            ->getResult();
    }

    /**
     * Find all past Events in repository order by date
     * @return Event[] Returns an array of Event objects
     */
    public function findPast($option = [])
    {
        $sort = 'ASC';
        if ($option !== [] && $option['sort'] === 'newer') {
            $sort = 'DESC';
        }

        return $this->createQueryBuilder('e')
            ->where('e.date < :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('e.date', $sort)
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
