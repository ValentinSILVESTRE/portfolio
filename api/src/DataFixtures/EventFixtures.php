<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture  implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
        ];
    }


    public function load(ObjectManager $manager)
    {
        $eventRepository = $manager->getRepository(Event::class);
        $categoryRepository = $manager->getRepository(Category::class);
        $eventsData = [
            [
                'title' => 'Portes ouvertes ESGI',
                'date' => new \DateTime('2021-11-17 17:30:00'),
            ],
            [
                'title' => 'DreamHack Finals - Ro4 & Final',
                'date' => new \DateTime('2021-11-14 15:00:00'),
            ],
            [
                'title' => 'OM - OL',
                'date' => new \DateTime('2021-11-21 20:45:00'),
            ],
            [
                'title' => 'Seville - Madrid',
                'date' => new \DateTime('2021-11-28 21:00:00'),
            ],
            [
                'title' => 'Liverpool - Arsenal',
                'date' => new \DateTime('2021-11-20 18:30:00'),
            ],
            [
                'title' => 'Chelsea - Manchester United',
                'date' => new \DateTime('2021-11-28 17:30:00'),
            ],
            [
                'title' => 'Manchester United - Arsenal',
                'date' => new \DateTime('2021-12-02 21:15:00'),
            ],
            [
                'title' => 'Dortmund - Bayern',
                'date' => new \DateTime('2021-12-04 18:30:00'),
            ],
            [
                'title' => 'Manchester City - Paris',
                'date' => new \DateTime('2021-11-24 21:00:00'),
            ],
            [
                'title' => 'Paris - Brugge',
                'date' => new \DateTime('2021-12-07 18:45:00'),
            ],
            [
                'title' => 'Real Madrid - Inter',
                'date' => new \DateTime('2021-12-07 21:00:00'),
            ],
            [
                'title' => 'Milan - Liverpool',
                'date' => new \DateTime('2021-12-07 21:00:00'),
            ],
            [
                'title' => 'Bayern - Barcelona',
                'date' => new \DateTime('2021-12-08 21:00:00'),
            ],
            [
                'title' => 'Montpellier - Aix',
                'date' => new \DateTime('2021-11-14 17:00:00'),
            ],
            [
                'title' => 'Grand Prix - Brésil',
                'date' => new \DateTime('2021-11-14 18:00:00'),
            ],
            [
                'title' => 'LGRDV - Marion Marechal',
                'date' => new \DateTime('2021-11-14 10:00:00'),
            ],
            [
                'title' => 'Sprint',
                'date' => new \DateTime('2021-11-13 20:10:00'),
            ],
            [
                'title' => 'Katowice',
                'date' => new \DateTime('2022-02-19 13:00:00'),
            ],
            [
                'title' => 'Clem - Solar - Dark - Cure',
                'date' => new \DateTime('2021-11-19 13:00:00'),
            ],
            [
                'title' => 'TSL - Ro32 - Part 1',
                'date' => new \DateTime('2021-11-26 13:00:00'),
            ],
            [
                'title' => 'DreamHack - Last Chance',
                'date' => new \DateTime('2021-01-13 13:00:00'),
            ],
            [
                'title' => 'France - Géorgie',
                'date' => new \DateTime('2021-11-14 14:00:00'),
            ],
            [
                'title' => 'Pays de Galles - Fidgi',
                'date' => new \DateTime('2021-11-14 16:15:00'),
            ],
            [
                'title' => 'Six nations',
                'date' => new \DateTime('2022-02-05 15:15:00'),
            ],
            [
                'title' => 'Barcelone - Kielce',
                'date' => new \DateTime('2021-11-18 20:45:00'),
            ],
            [
                'title' => 'Débats LR',
                'date' => new \DateTime('2021-11-14 20:45:00'),
            ],
            [
                'title' => 'Randonnée Soudorgues',
                'date' => new \DateTime('2021-11-20 09:30:00'),
            ],
            [
                'title' => 'Pologne - Hongrie',
                'date' => new \DateTime('2021-11-15 20:45:00'),
            ],
            [
                'title' => 'Pokemon diamants étincelants',
                'date' => new \DateTime('2021-11-19 00:00:00'),
            ],
            [
                'title' => 'NV - DPG',
                'date' => new \DateTime('2021-11-16 13:00:00'),
            ],
            [
                'title' => 'TL - GGG',
                'date' => new \DateTime('2021-11-17 14:30:00'),
            ],
            [
                'title' => 'Alpha X - Psistorm',
                'date' => new \DateTime('2021-11-18 14:30:00'),
            ],
            [
                'title' => 'Maru - MaNa - Elazer - Bunny',
                'date' => new \DateTime('2021-11-19 13:00:00'),
            ],
            [
                'title' => 'Serral - Classic - Buyn - Reynor',
                'date' => new \DateTime('2021-11-20 13:00:00'),
            ],
            [
                'title' => 'Rogue - HeRoMaRinE - Ragnarok - Zest',
                'date' => new \DateTime('2021-11-20 13:00:00'),
            ],
            [
                'title' => 'Ro8',
                'date' => new \DateTime('2021-11-21 11:00:00'),
            ],
            [
                'title' => 'GP - Qatar',
                'date' => new \DateTime('2021-11-21 15:00:00'),
            ],
            [
                'title' => 'Qualifications',
                'date' => new \DateTime('2021-11-20 15:00:00'),
            ],
            [
                'title' => 'Chambéry - Montpellier',
                'date' => new \DateTime('2021-11-21 17:00:00'),
            ],
            [
                'title' => 'GP - Arabie Saodite',
                'date' => new \DateTime('2021-12-05 18:30:00'),
            ],
            [
                'title' => 'GP - Abu Dhabi',
                'date' => new \DateTime('2021-12-12 14:00:00'),
            ],
            [
                'title' => 'Lambo - Rogue - Serral - Special',
                'date' => new \DateTime('2021-11-29 12:00:00'),
            ],
            [
                'title' => 'Bunny - Clem - Ragnarok - Time',
                'date' => new \DateTime('2021-11-24 12:00:00'),
            ],
            [
                'title' => 'Cure - Dark - Reynor - Zest',
                'date' => new \DateTime('2021-12-01 12:00:00'),
            ],
            [
                'title' => 'Ro16 - Part 1',
                'date' => new \DateTime('2021-11-22 09:00:00'),
            ],
            [
                'title' => 'Ro16 - Part 2',
                'date' => new \DateTime('2021-11-25 09:00:00'),
            ],
            [
                'title' => 'Randonnée Corconnes',
                'date' => new \DateTime('2021-11-30 13:00:00'),
            ],
            [
                'title' => 'NV - AFreeks',
                'date' => new \DateTime('2021-11-23 12:00:00'),
            ],
            [
                'title' => 'TL - SR',
                'date' => new \DateTime('2021-11-24 12:00:00'),
            ],
            [
                'title' => 'Alpha X - Kaizi',
                'date' => new \DateTime('2021-11-24 14:00:00'),
            ],
            [
                'title' => 'Psistorm - DPG',
                'date' => new \DateTime('2021-11-24 16:00:00'),
            ],
            [
                'title' => 'Kiev - Bayern München',
                'date' => new \DateTime('2021-11-23 18:45:00'),
            ],
            [
                'title' => 'Athletico Madrid - Milan',
                'date' => new \DateTime('2021-11-24 21:00:00'),
            ],
            [
                'title' => 'Bunny - Clem - Ragnarok - Time',
                'date' => new \DateTime('2021-11-25 12:00:00'),
            ],
            [
                'title' => 'Kielce- Barcelona',
                'date' => new \DateTime('2021-11-24 18:45:00'),
            ],
            [
                'title' => 'Montpellier - Elverum',
                'date' => new \DateTime('2021-11-24 20:45:00'),
            ],
            [
                'title' => 'Vardar - Szeged',
                'date' => new \DateTime('2021-11-25 18:45:00'),
            ],
            [
                'title' => 'Porto - Paris',
                'date' => new \DateTime('2021-11-25 20:45:00'),
            ],
            [
                'title' => 'DreamHack Finals - Group A',
                'date' => new \DateTime('2021-11-25 13:00:00'),
            ],
            [
                'title' => 'DreamHack Finals - Group B',
                'date' => new \DateTime('2021-11-25 18:15:00'),
            ],
            [
                'title' => 'DreamHack Finals - Group C',
                'date' => new \DateTime('2021-11-26 13:00:00'),
            ],
            [
                'title' => 'DreamHack Finals - Group D',
                'date' => new \DateTime('2021-11-26 18:15:00'),
            ],
            [
                'title' => 'TSL - Ro32 - Part 2',
                'date' => new \DateTime('2021-11-27 13:00:00'),
            ],
            [
                'title' => 'DreamHack Finals - Ro8',
                'date' => new \DateTime('2021-11-27 15:30:00'),
            ],
            [
                'title' => 'Semifinal + Final',
                'date' => new \DateTime('2021-11-28 15:30:00'),
            ],
            [
                'title' => 'Ro32 - Looser bracket',
                'date' => new \DateTime('2021-11-28 13:00:00'),
            ],
            [
                'title' => 'ro8',
                'date' => new \DateTime('2021-11-29 09:00:00'),
            ],
            [
                'title' => 'Semifinal + final',
                'date' => new \DateTime('2021-12-02 09:00:00'),
            ],
            [
                'title' => 'Porto - Atlético',
                'date' => new \DateTime('2021-12-07 21:00:00'),
            ],
            [
                'title' => 'Montpellier - Zagreb',
                'date' => new \DateTime('2021-12-01 20:45:00'),
            ],
            [
                'title' => 'Aalborg - Szeged',
                'date' => new \DateTime('2021-12-01 18:45:00'),
            ],
            [
                'title' => 'Vesprém - Bucarest',
                'date' => new \DateTime('2021-12-02 18:45:00'),
            ],
            [
                'title' => 'Paris - Kielce',
                'date' => new \DateTime('2021-12-02 20:45:00'),
            ],
            [
                'title' => 'TL - DPG',
                'date' => new \DateTime('2021-12-01 14:00:00'),
            ],
            [
                'title' => 'Meeting Zemmour',
                'date' => new \DateTime('2021-12-05 13:00:00'),
            ],
            [
                'title' => 'Semifinals',
                'date' => new \DateTime('2021-08-12 12:00:00'),
            ],
            [
                'title' => 'Finals',
                'date' => new \DateTime('2021-12-09 12:00:00'),
            ],
            [
                'title' => 'Ro16',
                'date' => new \DateTime('2021-12-03 13:00:00'),
            ],
            [
                'title' => 'Ro24 - Looser bracket',
                'date' => new \DateTime('2021-12-04 13:00:00'),
            ],
            [
                'title' => 'Quarterfinals + Ro24 Loosers',
                'date' => new \DateTime('2021-12-05 13:00:00'),
            ],
            [
                'title' => 'Meeting Hidalgo',
                'date' => new \DateTime('2021-12-12 14:00:00'),
            ],
            [
                'title' => 'Semifinals + Ro12 Loosers',
                'date' => new \DateTime('2021-12-10 13:00:00'),
            ],
            [
                'title' => 'Ro8 + Ro6 + Ro4 Loosers',
                'date' => new \DateTime('2021-12-11 12:15:00'),
            ],
            [
                'title' => 'Finals',
                'date' => new \DateTime('2021-12-12 15:00:00'),
            ],
            [
                'title' => 'Semifinals',
                'date' => new \DateTime('2021-12-08 12:00:00'),
            ],
            [
                'title' => 'Szeged - Kiel',
                'date' => new \DateTime('2021-12-09 20:45:00'),
            ],
            [
                'title' => 'Flensburg - Vesprem',
                'date' => new \DateTime('2021-12-09 18:45:00'),
            ],
            [
                'title' => 'Paris - Barcelone',
                'date' => new \DateTime('2021-12-09 20:45:00'),
            ],
            [
                'title' => 'Qualifiations - Abu Dhabi',
                'date' => new \DateTime('2021-12-11 14:00:00'),
            ],
            [
                'title' => 'Real - Athletico',
                'date' => new \DateTime('2021-12-12 21:00:00'),
            ],
            [
                'title' => 'Zemmour - Hannouna',
                'date' => new \DateTime('2021-12-16 21:00:00'),
            ],
            [
                'title' => 'Randonnée Aigaliers',
                'date' => new \DateTime('2021-12-20 13:30:00'),
            ],
            [
                'title' => 'Randonnée Lauret',
                'date' => new \DateTime('2021-12-22 10:30:00'),
            ],
            [
                'title' => 'France - Suède',
                'date' => new \DateTime('2021-12-15 20:30:00'),
            ],
            [
                'title' => 'France - Denmark',
                'date' => new \DateTime('2021-12-17 17:30:00'),
            ],
            [
                'title' => 'Finale',
                'date' => new \DateTime('2021-12-19 17:30:00'),
            ],
            [
                'title' => 'Paris - Chambéry',
                'date' => new \DateTime('2021-12-18 16:00:00'),
            ],
            [
                'title' => 'Montpellier - Nantes',
                'date' => new \DateTime('2021-12-18 18:30:00'),
            ],
            [
                'title' => 'Finale',
                'date' => new \DateTime('2021-12-19 16:00:00'),
            ],
            [
                'title' => 'Tottenham - Liverpool',
                'date' => new \DateTime('2021-12-19 17:30:00'),
            ],
            [
                'title' => 'Psistorm - GGG',
                'date' => new \DateTime('2021-12-16 13:00:00'),
            ],
            [
                'title' => 'Alpha X - Team GP',
                'date' => new \DateTime('2021-12-16 14:30:00'),
            ],
            [
                'title' => 'Europe open qualifier',
                'date' => new \DateTime('2021-12-16 19:00:00'),
            ],
            [
                'title' => 'Europe closed qualifier',
                'date' => new \DateTime('2021-12-17 19:00:00'),
            ],
            [
                'title' => 'Nation\'s League',
                'date' => new \DateTime('2022-06-02 21:00:00'),
                'categories' => [4],
            ],
            [
                'title' => 'Tunisie - Algérie',
                'date' => new \DateTime('2021-12-18 18:00:00'),
            ],
            [
                'title' => 'Coupe du monde',
                'date' => new \DateTime('2022-11-21 21:00:00'),
                'categories' => [4],
            ],
            [
                'title' => 'America Open Qualifier',
                'date' => new \DateTime('2021-12-19 01:00:00'),
            ],
            [
                'title' => 'America Closed Qualifier',
                'date' => new \DateTime('2021-12-20 01:00:00'),
            ],
            [
                'title' => 'Euro',
                'date' => new \DateTime('2022-01-13 18:00:00'),
            ],
            [
                'title' => 'Golden League',
                'date' => new \DateTime('2021-11-27 21:00:00'),
            ],
            [
                'title' => 'Déclaration Covid',
                'date' => new \DateTime('2021-12-27 19:00:00'),
            ],
            [
                'title' => 'Alpha Christmas',
                'date' => new \DateTime('2021-12-29 05:00:00'),
            ],
            [
                'title' => 'Wardi Christmas',
                'date' => new \DateTime('2021-12-10 13:00:00'),
            ],
            [
                'title' => 'Happy - Colorful',
                'date' => new \DateTime('2021-12-28 13:00:00'),
            ],
            [
                'title' => 'Happy - Sui',
                'date' => new \DateTime('2021-12-30 12:00:00'),
            ],
            [
                'title' => 'Winter Series',
                'date' => new \DateTime('2022-01-04 18:00:00'),
            ],
            [
                'title' => 'Wandering Warriors Cup',
                'date' => new \DateTime('2022-01-03 13:00:00'),
            ],
            [
                'title' => 'Premier League',
                'date' => new \DateTime('2021-12-28 04:31:00'),
            ],
            [
                'title' => 'Rochegude',
                'date' => new \DateTime('2022-01-04 13:30:00'),
            ],
            [
                'title' => 'Mauressargues',
                'date' => new \DateTime('2022-01-06 13:30:00'),
            ],
            [
                'title' => 'Brujas',
                'date' => new \DateTime('2022-01-08 10:00:00'),
            ],
            [
                'title' => 'Hortus',
                'date' => new \DateTime('2022-01-11 13:00:00'),
            ],
            [
                'title' => 'Saint Mathieu de Tréviers',
                'date' => new \DateTime('2022-01-13 13:00:00'),
            ],
            [
                'title' => 'Dentelles de Montmirail',
                'date' => new \DateTime('2022-01-15 09:30:00'),
            ],
            [
                'title' => 'Bundesliga',
                'date' => new \DateTime('2021-11-29 03:31:00'),
            ],
            [
                'title' => 'Wardi New Year',
                'date' => new \DateTime('2021-12-31 13:00:00'),
            ],
            [
                'title' => 'Présidentielles 2022',
                'date' => new \DateTime('2021-04-10 20:00:00'),
                'categories' => [11],
            ],
            [
                'title' => 'Dakar',
                'date' => new \DateTime('2022-01-01 09:00:00'),
            ],
            [
                'title' => 'Jeux Olympiques d\'hiver',
                'date' => new \DateTime('2022-02-04 08:00:00'),
            ],
            [
                'title' => 'Superbowl',
                'date' => new \DateTime('2022-02-13 21:00:00'),
            ],
            [
                'title' => 'Formula 1',
                'date' => new \DateTime('2022-03-01 21:00:00'),
            ],
            [
                'title' => 'Rolland-Garros',
                'date' => new \DateTime('2022-05-22 13:00:00'),
                'categories' => [1],
            ],
            [
                'title' => 'Tour de France',
                'date' => new \DateTime('2022-07-01 13:00:00'),
                'categories' => [1],
            ],
            [
                'title' => 'Championnats d\'Europe',
                'date' => new \DateTime('2022-09-01 21:00:00'),
                'categories' => [5],
            ],
            [
                'title' => 'Ligue 1',
                'date' => new \DateTime('2022-01-01 11:31:00'),
            ],
            [
                'title' => 'La Liga',
                'date' => new \DateTime('2022-01-01 11:38:00'),
            ],
            [
                'title' => 'Coupe de France',
                'date' => new \DateTime('2022-01-01 11:44:00'),
            ],
            [
                'title' => 'France vs Allemagne',
                'date' => new \DateTime('2022-01-09 19:00:00'),
            ],
            [
                'title' => 'Copa del Rey',
                'date' => new \DateTime('2022-01-01 15:25:00'),
            ],
            [
                'title' => 'FA Cup',
                'date' => new \DateTime('2022-01-01 15:28:00'),
            ],
            [
                'title' => 'Carabao Cup',
                'date' => new \DateTime('2022-01-03 15:29:00'),
            ],
            [
                'title' => 'Serie A',
                'date' => new \DateTime('2022-01-05 13:05:00'),
            ],
            [
                'title' => 'OSC',
                'date' => new \DateTime('2022-01-06 13:00:00'),
            ],
            [
                'title' => 'Bundesliga',
                'date' => new \DateTime('2022-01-06 10:18:00'),
            ],
            [
                'title' => 'WTL',
                'date' => new \DateTime('2022-01-08 10:34:00'),
            ],
            [
                'title' => 'CAN',
                'date' => new \DateTime('2022-01-09 13:51:00'),
            ],
            [
                'title' => 'Supercopa',
                'date' => new \DateTime('2022-01-09 13:22:00'),
            ],
            [
                'title' => 'Winter Series 3',
                'date' => new \DateTime('2022-01-12 18:00:00'),
            ],
            [
                'title' => 'Pro League',
                'date' => new \DateTime('2022-01-11 10:25:00'),
            ],
            [
                'title' => 'GSL Super Tournament',
                'date' => new \DateTime('2022-01-12 09:52:00'),
            ],
            [
                'title' => 'Fin de l\'abonnement Bein',
                'date' => new \DateTime('2022-02-13 23:59:00'),
            ],
            [
                'title' => 'Faire le repas',
                'date' => new \DateTime('2022-01-21 20:00:00'),
            ],
            [
                'title' => 'Ambre',
                'date' => new \DateTime('2022-02-20 13:00:00'),
                'categories' => [17],
            ],
            [
                'title' => 'Gersende',
                'date' => new \DateTime('2023-03-05 13:00:00'),
                'categories' => [17],
            ],
            [
                'title' => 'Vallon Pont d\'Arc',
                'date' => new \DateTime('2022-01-29 10:00:00'),
            ],
            [
                'title' => 'Champion\'s League',
                'date' => new \DateTime('2022-01-25 18:25:00'),
                'categories' => [4],
            ],
            [
                'title' => 'Nations War',
                'date' => new \DateTime('2022-06-11 13:00:00'),
                'categories' => [8],
            ],
            [
                'title' => 'Coupe de france',
                'date' => new \DateTime('2022-01-28 14:12:00'),
            ],
            [
                'title' => 'Champion\'s League',
                'date' => new \DateTime('2022-03-01 15:46:00'),
                'categories' => [3],
            ],
            [
                'title' => 'LNH',
                'date' => new \DateTime('2022-03-11 16:00:00'),
            ],
            [
                'title' => 'Europa League',
                'date' => new \DateTime('2022-03-02 16:08:00'),
                'categories' => [4],
            ],
            [
                'title' => 'Europa League',
                'date' => new \DateTime('2022-03-11 16:14:00'),
                'categories' => [3],
            ],
            [
                'title' => 'Battle Dev',
                'date' => new \DateTime('2022-06-03 20:00:00'),
                'categories' => [13],
            ],
            [
                'title' => 'GSL 2022 S1',
                'date' => new \DateTime('2022-02-14 09:14:00'),
                'categories' => [8],
            ],
            [
                'title' => 'Golden League 2021 - 2022',
                'date' => new \DateTime('2022-01-14 13:17:00'),
            ],
            [
                'title' => '2v2 Preshow',
                'date' => new \DateTime('2022-02-15 15:43:00'),
                'parentEventId' => 158,
            ],
            [
                'title' => 'Lédignan vs Aigues-Mortes',
                'date' => new \DateTime('2022-03-17 20:30:00'),
            ],
            [
                'title' => 'Entretient 5 Moutons',
                'date' => new \DateTime('2022-03-17 10:00:00'),
            ],
            [
                'title' => 'TAF - Nîmes',
                'date' => new \DateTime('2022-03-22 21:00:00'),
                'categories' => [14],
            ],
            [
                'title' => 'Salon TAF - Alès',
                'date' => new \DateTime('2022-04-28 21:00:00'),
                'categories' => [14],
            ],
            [
                'title' => 'Spring Challenge',
                'date' => new \DateTime('2022-04-21 13:00:00'),
                'categories' => [14],
            ],
            [
                'title' => 'Nimes - ALASC',
                'date' => new \DateTime('2022-03-21 21:00:00'),
            ],
            [
                'title' => 'Belgique - Pays Bas',
                'date' => new \DateTime('2022-06-03 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Pays Bas - Belgique',
                'date' => new \DateTime('2022-09-25 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Croatie - France',
                'date' => new \DateTime('2022-06-06 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'France - Croatie',
                'date' => new \DateTime('2022-06-13 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Espagne - Portugal',
                'date' => new \DateTime('2022-06-02 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Portugal - Espagne',
                'date' => new \DateTime('2022-09-27 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Italie - Allemagne',
                'date' => new \DateTime('2022-06-04 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Allemagne - Italie',
                'date' => new \DateTime('2022-06-14 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Angleterre - Allemagne',
                'date' => new \DateTime('2022-09-26 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Allemagne - Angleterre',
                'date' => new \DateTime('2022-06-07 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Italie - Angleterre',
                'date' => new \DateTime('2022-09-23 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'Angleterre - Italie',
                'date' => new \DateTime('2022-06-11 21:00:00'),
                'parentEventId' => 105,
            ],
            [
                'title' => 'France - Serbie',
                'date' => new \DateTime('2022-01-17 20:30:00'),
            ],
            [
                'title' => 'France - Croatie',
                'date' => new \DateTime('2022-01-13 20:30:00'),
            ],
            [
                'title' => 'Portugal - Turquie (Qualif Semifinal 2)',
                'date' => new \DateTime('2022-03-24 20:45:00'),
                'parentEventId' => 107,
            ],
            [
                'title' => 'Macédoine - Portugal (Qualif)',
                'date' => new \DateTime('2022-03-29 20:45:00'),
                'parentEventId' => 107,
            ],
            [
                'title' => 'Group A',
                'date' => new \DateTime('2021-12-29 05:00:00'),
            ],
            [
                'title' => 'Group B',
                'date' => new \DateTime('2021-12-30 05:00:00'),
            ],
            [
                'title' => 'Playoffs',
                'date' => new \DateTime('2021-12-31 05:00:00'),
            ],
            [
                'title' => 'Group D',
                'date' => new \DateTime('2021-12-29 13:00:00'),
            ],
            [
                'title' => 'wc',
                'date' => new \DateTime('2021-12-10 00:26:00'),
            ],
            [
                'title' => 'Leicester - Liverpool',
                'date' => new \DateTime('2021-12-28 21:00:00'),
            ],
            [
                'title' => 'Playoffs',
                'date' => new \DateTime('2021-12-30 13:00:00'),
            ],
            [
                'title' => 'Group A',
                'date' => new \DateTime('2021-12-31 13:00:00'),
            ],
            [
                'title' => 'Premier tour',
                'date' => new \DateTime('2022-04-10 20:00:00'),
                'parentEventId' => 128,
            ],
            [
                'title' => 'Second tour',
                'date' => new \DateTime('2022-04-24 20:00:00'),
                'parentEventId' => 128,
            ],
            [
                'title' => 'Lyon vs PSG',
                'date' => new \DateTime('2022-01-09 20:45:00'),
            ],
            [
                'title' => 'Getafe vs Real Madrid',
                'date' => new \DateTime('2022-01-02 14:00:00'),
            ],
            [
                'title' => 'Atlético vs Rayo Vallecano',
                'date' => new \DateTime('2022-01-02 16:15:00'),
            ],
            [
                'title' => 'Mallorca vs Barcelona',
                'date' => new \DateTime('2022-01-02 21:00:00'),
            ],
            [
                'title' => 'Chelsea vs Liverpool',
                'date' => new \DateTime('2022-01-02 17:15:00'),
            ],
            [
                'title' => 'Group D',
                'date' => new \DateTime('2022-01-04 13:00:00'),
            ],
            [
                'title' => 'Playoffs',
                'date' => new \DateTime('2022-01-05 13:00:00'),
            ],
            [
                'title' => 'Lens vs Lille',
                'date' => new \DateTime('2022-01-04 21:00:00'),
            ],
            [
                'title' => 'Playoffs',
                'date' => new \DateTime('2022-01-05 18:00:00'),
            ],
            [
                'title' => 'Linares vs Barcelona',
                'date' => new \DateTime('2022-01-05 19:30:00'),
            ],
            [
                'title' => 'Chelsea vs Tottenham',
                'date' => new \DateTime('2022-01-05 20:45:00'),
            ],
            [
                'title' => 'Tottenham vs Chelsea',
                'date' => new \DateTime('2022-01-12 20:45:00'),
            ],
            [
                'title' => 'Liverpool vs Arsenal',
                'date' => new \DateTime('2022-01-13 20:45:00'),
            ],
            [
                'title' => 'Arsenal vs Liverpool',
                'date' => new \DateTime('2022-01-20 20:45:00'),
            ],
            [
                'title' => 'Juventus vs Napoli',
                'date' => new \DateTime('2022-01-06 20:45:00'),
            ],
            [
                'title' => 'Milan vs Roma',
                'date' => new \DateTime('2022-01-06 18:30:00'),
            ],
            [
                'title' => 'Bayern vs Munchengladbach',
                'date' => new \DateTime('2022-01-07 20:30:00'),
            ],
            [
                'title' => 'Real Madrid vs Valence',
                'date' => new \DateTime('2022-01-08 21:00:00'),
            ],
            [
                'title' => 'Grenade vs Barcelona',
                'date' => new \DateTime('2022-01-08 18:30:00'),
            ],
            [
                'title' => 'Playoffs - Rounds 1 & 2',
                'date' => new \DateTime('2022-01-21 13:00:00'),
            ],
            [
                'title' => 'Playoffs - Round 3 + Quarter-Finals',
                'date' => new \DateTime('2022-01-22 13:00:00'),
            ],
            [
                'title' => 'Playoffs - Semi-Finals & Final',
                'date' => new \DateTime('2022-01-23 11:00:00'),
            ],
            [
                'title' => 'Showmatchs',
                'date' => new \DateTime('2022-01-09 09:00:00'),
            ],
            [
                'title' => 'Group A',
                'date' => new \DateTime('2022-01-11 15:00:00'),
            ],
            [
                'title' => 'Group B',
                'date' => new \DateTime('2022-01-12 15:00:00'),
            ],
            [
                'title' => 'Groupe C',
                'date' => new \DateTime('2022-01-13 15:00:00'),
            ],
            [
                'title' => 'Group D',
                'date' => new \DateTime('2022-01-14 15:00:00'),
            ],
            [
                'title' => 'QuarterFinals',
                'date' => new \DateTime('2022-01-15 21:25:00'),
            ],
            [
                'title' => 'Semifinals & Final',
                'date' => new \DateTime('2022-01-16 15:00:00'),
            ],
            [
                'title' => 'Barcelona vs Real Madrid',
                'date' => new \DateTime('2022-01-12 21:00:00'),
            ],
            [
                'title' => 'Finale - Real Madrid vs Atletic Bilbao',
                'date' => new \DateTime('2022-01-16 21:00:00'),
            ],
            [
                'title' => 'Atlético Madrid vs Atlétic Club',
                'date' => new \DateTime('2022-01-13 21:00:00'),
            ],
            [
                'title' => 'QuarterFinals 1 & 2',
                'date' => new \DateTime('2022-01-13 15:00:00'),
            ],
            [
                'title' => 'QuarterFinals 3 & 4',
                'date' => new \DateTime('2022-01-14 15:00:00'),
            ],
            [
                'title' => 'Semifinals',
                'date' => new \DateTime('2022-01-15 15:00:00'),
            ],
            [
                'title' => 'Final',
                'date' => new \DateTime('2022-01-16 15:00:00'),
            ],
            [
                'title' => 'Qualifier Round 1',
                'date' => new \DateTime('2022-01-14 11:00:00'),
            ],
            [
                'title' => 'Portugal - Islande',
                'date' => new \DateTime('2022-01-14 20:30:00'),
            ],
            [
                'title' => 'France - Ukraine',
                'date' => new \DateTime('2022-01-15 18:00:00'),
            ],
            [
                'title' => 'Espagne - Suède',
                'date' => new \DateTime('2022-01-15 20:30:00'),
            ],
            [
                'title' => 'Portugal vs Hongrie',
                'date' => new \DateTime('2022-01-16 18:00:00'),
            ],
            [
                'title' => 'Ro16 - Part 1',
                'date' => new \DateTime('2022-01-17 09:00:00'),
            ],
            [
                'title' => 'Ro16 - Part 2',
                'date' => new \DateTime('2022-01-20 09:00:00'),
            ],
            [
                'title' => 'QuarterFinals',
                'date' => new \DateTime('2022-01-24 09:00:00'),
            ],
            [
                'title' => 'Semifinals & Final',
                'date' => new \DateTime('2022-01-27 09:00:00'),
            ],
            [
                'title' => 'Russie vs Slovaquie',
                'date' => new \DateTime('2022-01-17 18:30:00'),
            ],
            [
                'title' => 'Islande - Hongrie',
                'date' => new \DateTime('2022-01-18 18:00:00'),
            ],
            [
                'title' => 'Portugal - Pays Bas',
                'date' => new \DateTime('2022-01-18 20:30:00'),
            ],
            [
                'title' => 'Pologne - Allemagne',
                'date' => new \DateTime('2022-01-18 18:30:00'),
            ],
            [
                'title' => 'Lyon vs Saint Etienne',
                'date' => new \DateTime('2022-01-21 21:00:00'),
            ],
            [
                'title' => 'France - Pays Bas',
                'date' => new \DateTime('2022-01-20 18:00:00'),
            ],
            [
                'title' => 'Russie - Suède',
                'date' => new \DateTime('2022-01-20 15:30:00'),
            ],
            [
                'title' => 'Côte d\'Ivoire - Algérie',
                'date' => new \DateTime('2022-01-20 17:30:00'),
            ],
            [
                'title' => 'Allemagne - Norvège',
                'date' => new \DateTime('2022-01-21 20:30:00'),
            ],
            [
                'title' => 'Russie - Espagne',
                'date' => new \DateTime('2022-01-21 15:30:00'),
            ],
            [
                'title' => 'Pologne - Suède',
                'date' => new \DateTime('2022-01-21 18:00:00'),
            ],
            [
                'title' => 'Chelsea vs Liverpool',
                'date' => new \DateTime('2022-02-27 21:00:00'),
            ],
            [
                'title' => 'Pologne - Russie',
                'date' => new \DateTime('2022-01-23 15:30:00'),
            ],
            [
                'title' => 'Allemagne - Suède',
                'date' => new \DateTime('2022-01-23 18:00:00'),
            ],
            [
                'title' => 'Espagne - Norvège',
                'date' => new \DateTime('2022-01-23 20:30:00'),
            ],
            [
                'title' => 'Chelsea vs Tottenham',
                'date' => new \DateTime('2022-01-23 17:30:00'),
            ],
            [
                'title' => 'Islande - Croatie',
                'date' => new \DateTime('2022-01-24 15:30:00'),
            ],
            [
                'title' => 'France - Montenegro',
                'date' => new \DateTime('2022-01-24 20:30:00'),
            ],
            [
                'title' => 'Suède - Norvège',
                'date' => new \DateTime('2022-01-25 20:30:00'),
            ],
            [
                'title' => 'Islande - Monténegro',
                'date' => new \DateTime('2022-01-26 15:30:00'),
            ],
            [
                'title' => 'Croatie - Pays Bas',
                'date' => new \DateTime('2022-01-26 18:00:00'),
            ],
            [
                'title' => 'France - Danemark',
                'date' => new \DateTime('2022-01-26 20:30:00'),
            ],
            [
                'title' => 'PSG - Real Madrid',
                'date' => new \DateTime('2022-02-15 21:00:00'),
            ],
            [
                'title' => 'Islande - Norvège',
                'date' => new \DateTime('2022-01-28 15:30:00'),
            ],
            [
                'title' => 'Espagne - Danemark',
                'date' => new \DateTime('2022-01-28 18:00:00'),
            ],
            [
                'title' => 'France - Suède',
                'date' => new \DateTime('2022-01-28 20:30:00'),
            ],
            [
                'title' => 'France - Danemark',
                'date' => new \DateTime('2022-01-30 15:30:00'),
            ],
            [
                'title' => 'Espagne - Suède',
                'date' => new \DateTime('2022-01-30 18:00:00'),
            ],
            [
                'title' => 'Marseille - Montpellier',
                'date' => new \DateTime('2022-01-29 21:00:00'),
            ],
            [
                'title' => 'Lens - Monaco',
                'date' => new \DateTime('2022-01-30 21:00:00'),
            ],
            [
                'title' => 'PSG - Nice',
                'date' => new \DateTime('2022-01-31 21:15:00'),
            ],
            [
                'title' => 'Wales - Italia',
                'date' => new \DateTime('2022-03-19 15:15:00'),
            ],
            [
                'title' => 'Ireland - Scotland',
                'date' => new \DateTime('2022-03-19 17:45:00'),
            ],
            [
                'title' => 'France - England',
                'date' => new \DateTime('2022-03-19 21:00:00'),
            ],
            [
                'title' => 'Bahrein',
                'date' => new \DateTime('2022-03-20 16:00:00'),
            ],
            [
                'title' => 'Manchester United vs Atlético Madrid',
                'date' => new \DateTime('2022-03-15 21:00:00'),
            ],
            [
                'title' => 'Juventus vs Villareal',
                'date' => new \DateTime('2022-03-16 21:00:00'),
            ],
            [
                'title' => 'Chelsea - Lille',
                'date' => new \DateTime('2022-03-16 21:00:00'),
            ],
            [
                'title' => 'Monaco - PSG',
                'date' => new \DateTime('2022-03-20 13:00:00'),
            ],
            [
                'title' => 'Marseilles - Nice',
                'date' => new \DateTime('2022-03-20 20:45:00'),
            ],
            [
                'title' => 'Arsenal - Liverpool',
                'date' => new \DateTime('2022-03-16 21:15:00'),
            ],
            [
                'title' => 'Real Madrid vs Barcelona',
                'date' => new \DateTime('2022-03-20 21:00:00'),
            ],
            [
                'title' => 'Ro 12 - 1st Leg',
                'date' => new \DateTime('2021-03-30 15:00:00'),
            ],
            [
                'title' => 'Paris - Nantes',
                'date' => new \DateTime('2022-03-13 16:00:00'),
            ],
            [
                'title' => 'Galatasaray - Barcelona',
                'date' => new \DateTime('2022-03-17 18:45:00'),
            ],
            [
                'title' => 'Lyon - Porto',
                'date' => new \DateTime('2022-03-17 21:00:00'),
            ],
            [
                'title' => 'Ro 16 - 1st Leg',
                'date' => new \DateTime('2021-03-29 16:00:00'),
            ],
            [
                'title' => 'Group A',
                'date' => new \DateTime('2022-03-21 10:30:00'),
            ],
            [
                'title' => 'France - Norvège',
                'date' => new \DateTime('2022-03-19 13:30:00'),
            ],
            [
                'title' => 'France - Danemark',
                'date' => new \DateTime('2022-03-20 16:00:00'),
            ],
            [
                'title' => 'Qualifier 1',
                'date' => new \DateTime('2022-03-22 19:00:00'),
            ],
            [
                'title' => 'Espagne - Danemark',
                'date' => new \DateTime('2022-03-19 16:00:00'),
            ],
            [
                'title' => 'Group B',
                'date' => new \DateTime('2022-03-24 10:00:00'),
                'parentEventId' => 165
            ],
            [
                'title' => 'LSL',
                'date' => new \DateTime('2022-01-24 10:00:00'),
                'categories' => [3]
            ],
            [
                'title' => 'USAM - MHB',
                'date' => new \DateTime('2022-03-26 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'F1 2022 Championship',
                'date' => new \DateTime('2022-02-26 20:00:00'),
                'categories' => [6]
            ],
            [
                'title' => 'Saudi Arabia',
                'date' => new \DateTime('2022-03-27 19:00:00'),
                'parentEventId' => 300
            ],
            [
                'title' => 'Ravin des arcs',
                'date' => new \DateTime('2022-04-02 10:00:00'),
                'categories' => [16]
            ],
            [
                'title' => 'Coupe de France',
                'date' => new \DateTime('2022-02-02 10:00:00'),
                'categories' => [3]
            ],
            [
                'title' => 'Paris - Montpellier',
                'date' => new \DateTime('2022-04-21 20:00:00'),
                'categories' => [3],
                'parentEventId' => 303
            ],
            [
                'title' => 'Demi-finales',
                'date' => new \DateTime('2022-05-14 20:00:00'),
                'categories' => [3],
                'parentEventId' => 303
            ],
            [
                'title' => 'Germany - Israël',
                'date' => new \DateTime('2022-03-26 20:45:00'),
                'categories' => [4],
            ],
            [
                'title' => 'Golden League',
                'date' => new \DateTime('2022-03-26 16:00:00'),
                'categories' => [10],
            ],
            [
                'title' => 'Round 1',
                'date' => new \DateTime('2022-03-27 16:00:00'),
                'parentEventId' => 307
            ],
            [
                'title' => 'ALASC - ACHB',
                'date' => new \DateTime('2022-03-31 20:30:00'),
                'categories' => [3],
            ],
            [
                'title' => 'Auberge espagnol chez Laurent',
                'date' => new \DateTime('2022-05-15 09:00:00'),
                'categories' => [3],
            ],
            [
                'title' => 'Costières 1 - ALASC',
                'date' => new \DateTime('2022-04-04 20:30:00'),
                'categories' => [3],
            ],
            [
                'title' => 'Match amical à Clarensac',
                'date' => new \DateTime('2022-04-06 19:00:00'),
                'categories' => [3],
            ],
            [
                'title' => 'Group C',
                'date' => new \DateTime('2022-03-28 11:30:00'),
                'parentEventId' => 165
            ],
            [
                'title' => 'Velenje - USAM',
                'date' => new \DateTime('2022-03-29 18:45:00'),
                'parentEventId' => 163
            ],
            [
                'title' => 'Nantes - Berlin',
                'date' => new \DateTime('2022-03-29 20:45:00'),
                'parentEventId' => 163
            ],
            [
                'title' => 'USAM - Velenje',
                'date' => new \DateTime('2022-04-05 20:45:00'),
                'parentEventId' => 163
            ],
            [
                'title' => 'Berlin - Nantes',
                'date' => new \DateTime('2022-04-05 18:45:00'),
                'parentEventId' => 163
            ],
            [
                'title' => 'Elverum - Paris',
                'date' => new \DateTime('2022-03-30 18:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Flensburg - Szeged',
                'date' => new \DateTime('2022-03-30 20:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Vardar - Vesprém',
                'date' => new \DateTime('2022-03-31 18:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Porto - Montpellier',
                'date' => new \DateTime('2022-03-31 20:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Vesprém - Vardar',
                'date' => new \DateTime('2022-04-06 18:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Montpellier - Porto',
                'date' => new \DateTime('2022-04-06 20:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Szeged - Flensburg',
                'date' => new \DateTime('2022-04-07 18:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Paris - Elverum',
                'date' => new \DateTime('2022-04-07 20:45:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'ZLAN',
                'date' => new \DateTime('2022-04-08 13:00:00'),
                'categories' => [7]
            ],
            [
                'title' => 'Pologne - Suède',
                'date' => new \DateTime('2022-03-29 20:45:00'),
                'parentEventId' => 107
            ],
            [
                'title' => 'Egypt - Sénégal',
                'date' => new \DateTime('2022-03-29 19:00:00'),
                'parentEventId' => 107
            ],
            [
                'title' => 'Netherlands - Germany',
                'date' => new \DateTime('2022-03-29 20:45:00'),
                'categories' => [4]
            ],
            [
                'title' => 'France - South Afrika',
                'date' => new \DateTime('2022-03-29 21:15:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Group D',
                'date' => new \DateTime('2022-03-31 11:30:00'),
                'parentEventId' => 165
            ],
            [
                'title' => 'Group E',
                'date' => new \DateTime('2022-04-04 11:30:00'),
                'parentEventId' => 165
            ],
            [
                'title' => 'Group A',
                'date' => new \DateTime('2022-03-29 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Group B',
                'date' => new \DateTime('2022-03-30 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Group C',
                'date' => new \DateTime('2022-03-31 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Group D',
                'date' => new \DateTime('2022-04-01 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Livre Noir : Bataille de Marioupole',
                'date' => new \DateTime('2022-04-03 10:00:00'),
                'categories' => [11]
            ],
            [
                'title' => 'Wardi Winter Championship',
                'date' => new \DateTime('2022-03-03 10:00:00'),
                'categories' => [8]
            ],
            [
                'title' => 'J2',
                'date' => new \DateTime('2022-04-01 13:00:00'),
                'parentEventId' => 338
            ],
            [
                'title' => 'Ro4 + Looser => Ro6',
                'date' => new \DateTime('2022-04-02 12:15:00'),
                'parentEventId' => 338
            ],
            [
                'title' => 'Finals',
                'date' => new \DateTime('2022-04-03 13:00:00'),
                'parentEventId' => 338
            ],
            [
                'title' => 'Casters Civil War',
                'date' => new \DateTime('2022-04-08 13:00:00'),
                'categories' => [8]
            ],
            [
                'title' => 'Round 1',
                'date' => new \DateTime('2022-04-08 14:00:00'),
                'parentEventId' => 342
            ],
            [
                'title' => 'Australia',
                'date' => new \DateTime('2022-04-10 15:00:00'),
                'parentEventId' => 300
            ],
            [
                'title' => 'Ligue 1',
                'date' => new \DateTime('2022-01-10 15:00:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Liga',
                'date' => new \DateTime('2022-04-10 15:00:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Bundesliga',
                'date' => new \DateTime('2022-04-10 15:00:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Premier League',
                'date' => new \DateTime('2022-04-10 15:00:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Barcelona - Sevilla',
                'date' => new \DateTime('2022-04-03 21:00:00'),
                'parentEventId' => 346
            ],
            [
                'title' => 'Dortmund - Leipzig',
                'date' => new \DateTime('2022-04-02 18:30:00'),
                'parentEventId' => 347
            ],
            [
                'title' => 'Serie A',
                'date' => new \DateTime('2022-04-10 15:00:00'),
                'categories' => [4]
            ],
            [
                'title' => 'Juventus - Inter',
                'date' => new \DateTime('2022-04-03 20:45:00'),
                'parentEventId' => 351
            ],
            [
                'title' => 'Quarterfinals',
                'date' => new \DateTime('2022-04-02 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Finals',
                'date' => new \DateTime('2022-04-03 18:00:00'),
                'parentEventId' => 167
            ],
            [
                'title' => 'Benfica - Liverpool',
                'date' => new \DateTime('2022-04-05 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Manchester City - Atlético Madrid',
                'date' => new \DateTime('2022-04-05 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Villareal - Bayern',
                'date' => new \DateTime('2022-04-06 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Chelsea - Real Madrid',
                'date' => new \DateTime('2022-04-06 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Liverpool - Benfica',
                'date' => new \DateTime('2022-04-13 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Atlético Madrid - Manchester City',
                'date' => new \DateTime('2022-04-13 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Bayern - Villareal',
                'date' => new \DateTime('2022-04-12 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Real Madrid - Chelsea',
                'date' => new \DateTime('2022-04-12 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Manchester City - Liverpool',
                'date' => new \DateTime('2022-04-10 17:30:00'),
                'parentEventId' => 348
            ],
            [
                'title' => 'Saint Raphael - Montpellier',
                'date' => new \DateTime('2022-04-10 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Paris - Montpellier',
                'date' => new \DateTime('2022-05-08 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Montpellier - Limoges',
                'date' => new \DateTime('2022-05-22 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Montpellier - Nantes',
                'date' => new \DateTime('2022-05-29 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Montpellier - Chambéry',
                'date' => new \DateTime('2022-06-05 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Aix - Montpellier',
                'date' => new \DateTime('2022-06-08 20:00:00'),
                'parentEventId' => 298
            ],
            [
                'title' => 'Liverpool - Manchester United',
                'date' => new \DateTime('2022-04-19 21:00:00'),
                'parentEventId' => 348
            ],
            [
                'title' => 'Chelsea - Arsenal',
                'date' => new \DateTime('2022-04-20 20:45:00'),
                'parentEventId' => 348
            ],
            [
                'title' => 'Sevilla - Real Madrid',
                'date' => new \DateTime('2022-04-17 21:00:00'),
                'parentEventId' => 346
            ],
            [
                'title' => 'RDV Teams avec Scop & CO',
                'date' => new \DateTime('2022-04-06 11:00:00'),
                'categories' => [14]
            ],
            [
                'title' => 'RDV Teams avec Scop & CO',
                'date' => new \DateTime('2022-04-07 11:00:00'),
                'categories' => [14]
            ],
            [
                'title' => 'Group Stage 2',
                'date' => new \DateTime('2022-04-21 11:30:00'),
                'parentEventId' => 165
            ],
            [
                'title' => 'Group A - 1/2',
                'date' => new \DateTime('2022-04-11 11:30:00'),
                'parentEventId' => 375
            ],
            [
                'title' => 'Group A - 2/2',
                'date' => new \DateTime('2022-04-18 11:30:00'),
                'parentEventId' => 375
            ],
            [
                'title' => 'Group B - 1/2',
                'date' => new \DateTime('2022-04-14 11:30:00'),
                'parentEventId' => 375
            ],
            [
                'title' => 'Group B - 2/2',
                'date' => new \DateTime('2022-04-21 11:30:00'),
                'parentEventId' => 375
            ],
            [
                'title' => 'Leipzig - Atalanta',
                'date' => new \DateTime('2022-04-07 18:45:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'West Ham - Lyon',
                'date' => new \DateTime('2022-04-07 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Frankfurt - Barcelona',
                'date' => new \DateTime('2022-04-07 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Atalanta - Leipzig',
                'date' => new \DateTime('2022-04-14 18:45:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Lyon - West Ham',
                'date' => new \DateTime('2022-04-14 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Barcelona - Frankfurt',
                'date' => new \DateTime('2022-04-14 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Semifinals - 1st leg',
                'date' => new \DateTime('2022-04-28 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Semifinals - 2st leg',
                'date' => new \DateTime('2022-05-05 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Final',
                'date' => new \DateTime('2022-05-18 21:00:00'),
                'parentEventId' => 162
            ],
            [
                'title' => 'Final 4',
                'date' => new \DateTime('2022-06-18 15:00:00'),
                'parentEventId' => 160
            ],
            [
                'title' => 'Semifinals - 1st leg',
                'date' => new \DateTime('2022-04-27 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Semifinals - 2st leg',
                'date' => new \DateTime('2022-05-04 21:00:00'),
                'parentEventId' => 157
            ],
            [
                'title' => 'Final',
                'date' => new \DateTime('2022-05-28 21:00:00'),
                'parentEventId' => 157
            ],
        ];

        foreach ($eventsData as $eventData) {
            $event = new Event();
            $event->hidrate($eventData);
            $manager->persist($event);

            // On ajoute le parent s'il y a
            if (isset($eventData['parentEventId'])) {
                $parentId = $eventData['parentEventId'];
                if ($parentId) {
                    $parent = $eventRepository->find($parentId);
                    $parent->addSubEvent($event);
                    $event->setParentEvent($parent);
                    $event->setParentEventId($parentId);
                }
            }

            if (array_key_exists('categories', $eventData)) {
                foreach ($eventData['categories'] as $categoryId) {
                    $event->addCategory($categoryRepository->find($categoryId));
                }
            }

            $manager->flush();
        }
    }
}
