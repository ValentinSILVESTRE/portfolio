<?php

namespace App\DataFixtures;

use App\Entity\LinkType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LinkTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $linkTypeNames = [
            'live',
            'replay',
            'info',
        ];

        foreach ($linkTypeNames as $linkTypeName) {
            $linktype = new LinkType($linkTypeName);
            $manager->persist($linktype);
            $manager->flush();
        }
    }
}
