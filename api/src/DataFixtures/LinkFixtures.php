<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Link;
use App\Entity\LinkType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LinkFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            LinkTypeFixtures::class,
            EventFixtures::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $linkTypeRepository = $manager->getRepository(LinkType::class);
        $eventRepository = $manager->getRepository(Event::class);

        $linksData = [
            [
                'url' => 'https://www.youtube.com/channel/UCK5eBtuoj_HkdXKHNmBLAXg',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(165),
            ],
            [
                'url' => 'https://liquipedia.net/starcraft2/Global_StarCraft_II_League/2022/Season_1#Group_Stage_2',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(375),
            ],
            [
                'url' => 'https://en.wikipedia.org/wiki/2022_FIFA_World_Cup_qualification_(UEFA)#Path_C',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(188),
            ],
            [
                'url' => 'https://ehftv.com/home',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(163),
            ],
            [
                'url' => 'https://ehfel.eurohandball.com/men/2021-22/standings/kWvDaphagqoomWsHrgovow/last-16/',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(163),
            ],
            [
                'url' => 'https://en.wikipedia.org/wiki/2021%E2%80%9322_EHF_Champions_League#Playoffs',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(160),
            ],
            [
                'url' => 'https://ehftv.com/hand-ball-match?categories=Full%20games&competitions=EHF%20Champions%20League%20Men',
                'type' => $linkTypeRepository->find(2),
                'event' => $eventRepository->find(160),
            ],
            [
                'url' => 'https://ehftv.com/home',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(160),
            ],
            [
                'url' => 'https://www.codingame.com/home',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(172),
            ],
            [
                'url' => 'https://www.laregion.fr/Salon-TAF-de-Tarbes-16504',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(171),
            ],
            [
                'url' => 'https://en.wikipedia.org/wiki/2022%E2%80%9323_UEFA_Nations_League#League_A',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(105),
            ],
            [
                'url' => 'https://en.wikipedia.org/wiki/2022_FIFA_World_Cup#toc',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(107),
            ],
            [
                'url' => 'https://www.youtube.com/watch?v=pCan3yzwRLA',
                'type' => $linkTypeRepository->find(2),
                'event' => $eventRepository->find(297),
            ],
            [
                'url' => 'https://handnews.fr/lnh-2/',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(298),
            ],
            [
                'url' => 'http://fomny.com/Video-2/france/04/Bein-sport/Bein-sport.php',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(298),
            ],
            [
                'url' => 'http://fomny.com/Video-2/france/02/Canal-plus/Canal-plus--france.php',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(300),
            ],
            [
                'url' => 'https://www.formula1.com/en/racing/2022.html',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(300),
            ],
            [
                'url' => 'https://www.facebook.com/events/688297525543007/?active_tab=discussion',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(302),
            ],
            [
                'url' => 'https://liquipedia.net/ageofempires/Golden_League/Round/1#Results',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(308),
            ],
            [
                'url' => 'https://liquipedia.net/ageofempires/Golden_League',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(307),
            ],
            [
                'url' => 'https://www.twitch.tv/marinelord',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(307),
            ],
            [
                'url' => 'https://www.twitch.tv/egctv',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(307),
            ],
            [
                'url' => 'https://ehfcl.eurohandball.com/men/2021-22/matches/',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(160),
            ],
            [
                'url' => 'https://www.twitch.tv/zerator',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(326),
            ],
            [
                'url' => 'https://www.z-lan.fr/',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(326),
            ],
            [
                'url' => 'https://liquipedia.net/starcraft2/StarCraft_II:_NationWars_7/Preshow#Group_Stage',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(167),
            ],
            [
                'url' => 'https://www.twitch.tv/wardiii',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(167),
            ],
            [
                'url' => 'https://www.youtube.com/channel/UCysox6U9HnqgCiJ-Pd-Jy_w',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(337),
            ],
            [
                'url' => 'https://www.twitch.tv/wardiii',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(338),
            ],
            [
                'url' => 'https://liquipedia.net/starcraft2/WardiTV_Winter_Championship/2022#Results',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(338),
            ],
            [
                'url' => 'https://www.twitch.tv/wardiii',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(342),
            ],
            [
                'url' => 'https://liquipedia.net/starcraft2/Casters_Civil_War#Group_Stage',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(342),
            ],
            [
                'url' => 'https://official.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(347),
            ],
            [
                'url' => 'https://official.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(346),
            ],
            [
                'url' => 'https://official.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(351),
            ],
            [
                'url' => 'https://official.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(348),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=serie+a#sie=lg;/g/11n0vx7n5d;2;/m/03zv9;st;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(351),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=premier+league#sie=lg;/g/11p44qhs93;2;/m/02_tc;st;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(348),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=laliga#sie=lg;/g/11mqlmppsd;2;/m/09gqx;st;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(346),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=bundesliga+#sie=lg;/g/11m__0kr76;2;/m/037169;st;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(347),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=ligue1#sie=lg;/g/11qq96slgb;2;/m/044hxl;st;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(345),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=champions+league#sie=lg;/g/11j8x175ph;2;/m/0c1q0;mt;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(157),
            ],
            [
                'url' => 'https://streamonsport11.xyz/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(157),
            ],
            [
                'url' => 'https://official.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(157),
            ],
            [
                'url' => 'https://www.google.com/search?channel=fs&client=ubuntu&q=europa+league#sie=lg;/g/11hf89b9b3;2;/m/01hrtp;mt;fp;1;;',
                'type' => $linkTypeRepository->find(3),
                'event' => $eventRepository->find(162),
            ],
            [
                'url' => 'https://streamonsport11.xyz/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(162),
            ],
            [
                'url' => 'https://original.footybite.cc/',
                'type' => $linkTypeRepository->find(1),
                'event' => $eventRepository->find(162),
            ],
        ];

        foreach ($linksData as $linkData) {
            $link = new Link();
            $link->hidrate($linkData);
            $manager->persist($link);
            $manager->flush();
        }
    }
}
