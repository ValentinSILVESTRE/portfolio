<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/category')]
class CategoryController extends AbstractController
{
    #[Route('', name: 'category_list', methods: ['GET', 'HEAD'])]
    public function list(
        CategoryRepository $categoryRepository,
    ): Response {
        return $this->json($categoryRepository->findAll(), 200, [], ['groups' => 'category']);
    }

    #[Route('/{id<\d+>}', name: 'category_show', methods: ['GET', 'HEAD'])]
    public function show(
        Category $category,
        CategoryRepository $categoryRepository
    ): Response {
        $category = $categoryRepository->find($category);
        return $this->json($category, 200, [], ['groups' => 'category']);
    }

    #[Route('', name: 'category_new', methods: ['POST'])]
    public function new(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        CategoryRepository $categoryRepository,
    ): Response {
        $data = $request->getContent();

        $category = $serializer->deserialize($data, Category::class, 'json', [
            'groups' => 'category'
        ]);

        $entityManager->persist($category);
        $entityManager->flush();

        return $this->json($category, 201, [], [
            'groups' => 'category'
        ]);
    }

    // TODO : MARCHE PÔ
    #[Route('/{id<\d+>}', name: 'category_update', methods: ['PUT'])]
    public function update(
        int $id,
        Request $request,
        CategoryRepository $categoryRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ): Response {
        $category = $categoryRepository->find($id);
        if (!$category) {
            return $this->json([
                'message' => 'Category not found'
            ], 404);
        }

        // $newCategory = new Category();

        $data = $request->getContent();

        $serializer->deserialize($data, Category::class, 'json', [
            'object_to_populate' => $category
        ]);

        $parent = $category->getParentCategory();
        // Si le parent est déjà un enfant, alors on renvoie erreur 508 loop detected
        if (
            $parent &&
            ($category->isParentOf($parent->getId()) ||
                $category->getId() === $parent->getId()
            )
        ) {
            return $this->json([
                'message' => 'Category cannot be his own parent or on of his children'
            ], 508);
        }

        // Sinon met à jour uniquement
        $entityManager->flush();

        return $this->json($category, 200, [], [
            'groups' => 'category'
        ]);
    }

    #[Route('/{id<\d+>}', name: 'category_delete', methods: ['DELETE'])]
    public function delete(
        int $id,
        categoryRepository $categoryRepository,
        EntityManagerInterface $entityManager
    ): Response {
        $category = $categoryRepository->find($id);
        if (!$category) {
            return $this->json([
                'message' => 'Category not found'
            ], 404);
        }

        $entityManager->remove($category);
        $entityManager->flush();
        return $this->json([
            'message' => 'Category deleted successfully'
        ], 200, [], [
            'groups' => 'category'
        ]);
    }
}
