<?php

namespace App\Controller;

use App\Entity\LinkType;
use App\Repository\LinkTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/linktype')]
class LinkTypeController extends AbstractController
{
    #[Route('', name: 'linkType_list', methods: ['GET', 'HEAD'])]
    public function list(
        LinkTypeRepository $linkTypeRepository,
    ): Response {
        return $this->json($linkTypeRepository->findAll(), 200, [], [
            'groups' => 'linkType'
        ]);
    }

    #[Route('/{id}', name: 'linkType_show', methods: ['GET', 'HEAD'])]
    public function show(
        LinkType $linkType,
        LinkTypeRepository $linkTypeRepository
    ): Response {
        $linkType = $linkTypeRepository->find($linkType);
        return $this->json($linkType, 200, [], [
            'groups' => 'linkType'
        ]);
    }

    #[Route('', name: 'linkType_new', methods: ['POST'])]
    public function new(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
    ): Response {
        $data = $request->getContent();

        $linkType = $serializer->deserialize($data, LinkType::class, 'json', []);

        $entityManager->persist($linkType);

        $entityManager->flush();

        return $this->json($linkType, 201, [], ['groups' => 'linkType']);
    }

    #[Route('/{id}', name: "linkType_update", methods: ["PUT"])]
    public function update(
        int $id,
        Request $request,
        LinkTypeRepository $linkTypeRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ): Response {
        $linkType = $linkTypeRepository->find($id);
        if (!$linkType) {
            return $this->json([
                "message" => "LinkType not found"
            ], 404);
        }

        $data = $request->getContent();

        $serializer->deserialize($data, LinkType::class, 'json', [
            'object_to_populate' => $linkType
        ]);

        $entityManager->flush();
        return $this->json($linkType, 200, [], ['groups' => 'linkType']);
    }

    #[Route('/{id}', name: 'linkType_delete', methods: ["DELETE"])]
    public function delete(
        int $id,
        LinkTypeRepository $linkTypeRepository,
        EntityManagerInterface $entityManager
    ): Response {
        $linkType = $linkTypeRepository->find($id);
        if (!$linkType) {
            return $this->json([
                "message" => "LinkType not found"
            ], 404);
        }

        $entityManager->remove($linkType);
        $entityManager->flush();
        return $this->json(
            [
                "message" => "LinkType deleted successfully"
            ],
            200,
            [],
            [
                'groups' => 'linkType'
            ]
        );
    }
}
