<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/pokemon')]
class PokemonShowdownController extends AbstractController
{
    private $pokemonsUrl = 'https://play.pokemonshowdown.com/data/pokedex.json';
    private $movesUrl = 'https://play.pokemonshowdown.com/data/moves.json';
    private $learnsetsUrl = 'https://play.pokemonshowdown.com/data/learnsets.json';
    private $ubersetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8ubers.json';
    private $ousetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8ou.json';
    private $uusetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8uu.json';
    private $rusetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8ru.json';
    private $nusetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8nu.json';
    private $pusetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8pu.json';
    private $zusetsUrl = 'http://play.pokemonshowdown.com/data/sets/gen8zu.json';
    private $tierList = ['nationaldexag', 'uber', 'ou', 'uu', 'ru', 'nu', 'pu', 'zu'];

    private $setsUrls = [
        'http://play.pokemonshowdown.com/data/sets/gen8nationaldexag.json',
        'http://play.pokemonshowdown.com/data/sets/gen8ubers.json',
        'http://play.pokemonshowdown.com/data/sets/gen8ou.json',
        'http://play.pokemonshowdown.com/data/sets/gen8uu.json',
        'http://play.pokemonshowdown.com/data/sets/gen8ru.json',
        'http://play.pokemonshowdown.com/data/sets/gen8nu.json',
        'http://play.pokemonshowdown.com/data/sets/gen8pu.json',
        'http://play.pokemonshowdown.com/data/sets/gen8zu.json',
        'http://play.pokemonshowdown.com/data/sets/gen7anythinggoes.json',
        'http://play.pokemonshowdown.com/data/sets/gen7.json',
    ];

    #[Route('/set/{name}/{tier}', name: 'set-read', methods: ['GET', 'HEAD'])]
    public function set_read(string $name, string $tier)
    {
        $formatedName = $this->formatedName($name);

        // On s'assure que le tier est valable
        if (in_array($tier, $this->tierList)) {

            // On récupère l'index du tier (0 => uber ...)
            $tierIndex = 0;
            for ($i = 0; $i < count($this->tierList); $i++) {
                if ($this->tierList[$i] === $tier) {
                    $tierIndex = $i;
                }
            }

            // On récupère la liste des capacités du pokemon
            $client = HttpClient::create();
            $response = $client->request('GET', $this->pokemonsUrl);
            $pokemons = $response->toArray();
            $abilities = $pokemons[$this->formatName($name)]['abilities'];

            for ($i = $tierIndex; $i < count($this->tierList); $i++) {
                $url = $this->setsUrls[$i];
                // On regarde si on trouve dans le tier
                $client = HttpClient::create();
                $response = $client->request('GET', $url);
                $sets = $response->toArray();

                // On recherche dans les stats
                if (array_key_exists($formatedName, $sets['stats'])) {
                    $set = $sets['stats'][$formatedName]['Showdown Usage'];

                    return $this->json([
                        'abilities' => $abilities,
                        'tier' => $this->tierList[$i],
                        'set' => $set
                    ]);
                }

                // On recherche dans les dexs
                if (array_key_exists($formatedName, $sets['dex'])) {
                    $firstKey = array_keys($sets['dex'][$formatedName])[0];
                    $set = $sets['dex'][$formatedName][$firstKey];
                    return $this->json([
                        'abilities' => $abilities,
                        'tier' => $this->tierList[$i],
                        'set' => $set
                    ]);
                }
            }

            for ($i = $tierIndex - 1; $i >= 0; $i--) {
                $url = $this->setsUrls[$i];
                // On regarde si on trouve dans le tier
                $client = HttpClient::create();
                $response = $client->request('GET', $url);
                $sets = $response->toArray();

                // On recherche dans les stats
                if (array_key_exists($formatedName, $sets['stats'])) {
                    $set = $sets['stats'][$formatedName]['Showdown Usage'];
                    return $this->json([
                        'abilities' => $abilities,
                        'tier' => $this->tierList[$i],
                        'set' => $set
                    ]);
                }

                // On recherche dans les dexs
                if (array_key_exists($formatedName, $sets['dex'])) {
                    $firstKey = array_keys($sets['dex'][$formatedName])[0];
                    $set = $sets['dex'][$formatedName][$firstKey];
                    return $this->json([
                        'abilities' => $abilities,
                        'tier' => $this->tierList[$i],
                        'set' => $set
                    ]);
                }
            }
        } else {
            return $this->json("Pas de set populaire trouvé pour $formatedName en $tier");
        }
    }

    #[Route('/pokemon', name: 'pokemon-list', methods: ['GET', 'HEAD'])]
    public function pokemon_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->pokemonsUrl);
        $pokemons = $response->toArray();

        return $this->json($pokemons);
    }

    #[Route('/pokemon/{name}', name: 'pokemon-read', methods: ['GET', 'HEAD'])]
    public function pokemon_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->pokemonsUrl);
        $pokemons = $response->toArray();
        $pokemon = $pokemons[$name];

        return $this->json($pokemon);
    }

    #[Route('/move', name: 'move-list', methods: ['GET', 'HEAD'])]
    public function move_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->movesUrl);
        $moves = $response->toArray();

        return $this->json($moves);
    }

    #[Route('/move/{name}', name: 'move-read', methods: ['GET', 'HEAD'])]
    public function move_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->movesUrl);
        $moves = $response->toArray();
        $move = $moves[$name];

        return $this->json($move);
    }

    #[Route('/learnset', name: 'learnset-list', methods: ['GET', 'HEAD'])]
    public function learnset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->learnsetsUrl);
        $learnsets = $response->toArray();

        return $this->json($learnsets);
    }

    #[Route('/learnset/{name}', name: 'learnset-read', methods: ['GET', 'HEAD'])]
    public function learnset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->learnsetsUrl);
        $learnsets = $response->toArray();
        $learnset = $learnsets[$name];

        return $this->json($learnset);
    }

    #[Route('/nationaldexagset', name: 'nationaldexagset-list', methods: ['GET', 'HEAD'])]
    public function nationaldexagset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->nationaldexagsetsUrl);
        $nationaldexagsets = $response->toArray()['stats'];

        return $this->json($nationaldexagsets);
    }

    #[Route('/nationaldexagset/{name}', name: 'nationaldexagset-read', methods: ['GET', 'HEAD'])]
    public function nationaldexagset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->nationaldexagsetsUrl);
        $nationaldexagsets = $response->toArray();
        $nationaldexagset = $nationaldexagsets['stats'][$name]['Showdown Usage'];

        return $this->json($nationaldexagset);
    }

    #[Route('/uberset', name: 'uberset-list', methods: ['GET', 'HEAD'])]
    public function uberset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->ubersetsUrl);
        $ubersets = $response->toArray()['stats'];

        return $this->json($ubersets);
    }

    #[Route('/uberset/{name}', name: 'uberset-read', methods: ['GET', 'HEAD'])]
    public function uberset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->ubersetsUrl);
        $ubersets = $response->toArray();
        $uberset = $ubersets['stats'][$name]['Showdown Usage'];

        return $this->json($uberset);
    }

    #[Route('/ouset', name: 'ouset-list', methods: ['GET', 'HEAD'])]
    public function ouset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->ousetsUrl);
        $ousets = $response->toArray()['stats'];

        return $this->json($ousets);
    }

    #[Route('/ouset/{name}', name: 'ouset-read', methods: ['GET', 'HEAD'])]
    public function ouset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->ousetsUrl);
        $ousets = $response->toArray();
        $ouset = $ousets['stats'][$name]['Showdown Usage'];

        return $this->json($ouset);
    }

    #[Route('/uuset', name: 'uuset-list', methods: ['GET', 'HEAD'])]
    public function uuset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->uusetsUrl);
        $uusets = $response->toArray()['stats'];

        return $this->json($uusets);
    }

    #[Route('/uuset/{name}', name: 'uuset-read', methods: ['GET', 'HEAD'])]
    public function uuset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->uusetsUrl);
        $uusets = $response->toArray();
        $uuset = $uusets['stats'][$name]['Showdown Usage'];

        return $this->json($uuset);
    }

    #[Route('/ruset', name: 'ruset-list', methods: ['GET', 'HEAD'])]
    public function ruset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->rusetsUrl);
        $rusets = $response->toArray()['stats'];

        return $this->json($rusets);
    }

    #[Route('/ruset/{name}', name: 'ruset-read', methods: ['GET', 'HEAD'])]
    public function ruset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->rusetsUrl);
        $rusets = $response->toArray();
        $ruset = $rusets['stats'][$name]['Showdown Usage'];

        return $this->json($ruset);
    }

    #[Route('/nuset', name: 'nuset-list', methods: ['GET', 'HEAD'])]
    public function nuset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->nusetsUrl);
        $nusets = $response->toArray()['stats'];

        return $this->json($nusets);
    }

    #[Route('/nuset/{name}', name: 'nuset-read', methods: ['GET', 'HEAD'])]
    public function nuset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->nusetsUrl);
        $nusets = $response->toArray();
        $nuset = $nusets['stats'][$name]['Showdown Usage'];

        return $this->json($nuset);
    }

    #[Route('/puset', name: 'puset-list', methods: ['GET', 'HEAD'])]
    public function puset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->pusetsUrl);
        $pusets = $response->toArray()['stats'];

        return $this->json($pusets);
    }

    #[Route('/puset/{name}', name: 'puset-read', methods: ['GET', 'HEAD'])]
    public function puset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->pusetsUrl);
        $pusets = $response->toArray();
        $puset = $pusets['stats'][$name]['Showdown Usage'];

        return $this->json($puset);
    }

    #[Route('/zuset', name: 'zuset-list', methods: ['GET', 'HEAD'])]
    public function zuset_list(): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->zusetsUrl);
        $zusets = $response->toArray()['stats'];

        return $this->json($zusets);
    }

    #[Route('/zuset/{name}', name: 'zuset-read', methods: ['GET', 'HEAD'])]
    public function zuset_read(string $name): Response
    {
        $client = HttpClient::create();
        $response = $client->request('GET', $this->zusetsUrl);
        $zusets = $response->toArray();
        $zuset = $zusets['stats'][$name]['Showdown Usage'];

        return $this->json($zuset);
    }

    #[Route('/test', name: 'test', methods: ['GET', 'HEAD'])]
    public function test(): void
    {
        // $client = HttpClient::create();
        // $response = $client->request('GET', 'http://play.pokemonshowdown.com/data/learnsets.js');
        // $zusets = $response->toArray();
        // $zuset = $zusets['roserade'];

        // return $this->json($zuset);
    }

    public function formatName(string $name)
    {
        return str_replace(['-', ' '], '', strtolower($name));
    }

    public function formatedName(String $name = null): String
    {
        if ($name === 'Zygarde-10') return 'Zygarde-10%';
        return $name;
    }
}
