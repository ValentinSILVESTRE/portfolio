<?php

namespace App\Controller;

use App\Entity\Link;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/link')]
class LinkController extends AbstractController
{
    #[Route('', name: 'link_list', methods: ['GET', 'HEAD'])]
    public function list(
        LinkRepository $linkRepository,
    ): Response {
        return $this->json($linkRepository->findAll(), 200, [], [
            'groups' => 'link'
        ]);
    }

    #[Route('/{id}', name: 'link_show', methods: ['GET', 'HEAD'])]
    public function show(
        Link $link,
        LinkRepository $linkRepository
    ): Response {
        $link = $linkRepository->find($link);
        return $this->json($link, 200, [], [
            'groups' => 'link'
        ]);
    }

    #[Route('', name: 'link_new', methods: ['POST'])]
    public function new(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
    ): Response {
        $data = $request->getContent();
        $link = $serializer->deserialize($data, Link::class, 'json');
        // dd($link);
        $entityManager->persist($link);
        $entityManager->flush();
        return $this->json($link, 201, [], [
            'groups' => 'link'
        ]);
    }

    #[Route('/{id}', name: 'link_update', methods: ['PUT'])]
    public function update(
        int $id,
        Request $request,
        LinkRepository $linkRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ): Response {
        $link = $linkRepository->find($id);
        if (!$link) {
            return $this->json([
                'message' => 'Link not found'
            ], 404);
        }

        $data = $request->getContent();

        $serializer->deserialize($data, Link::class, 'json', [
            'object_to_populate' => $link
        ]);

        $entityManager->flush();
        return $this->json($link, 200, [], [
            'groups' => 'link'
        ]);
    }

    #[Route('/{id}', name: 'link_delete', methods: ['DELETE'])]
    public function delete(
        int $id,
        LinkRepository $linkRepository,
        EntityManagerInterface $entityManager
    ): Response {
        $link = $linkRepository->find($id);
        if (!$link) {
            return $this->json([
                'message' => 'Link not found'
            ], 404);
        }

        $entityManager->remove($link);
        $entityManager->flush();
        return $this->json([
            'message' => 'Link deleted successfully'
        ]);
    }
}
