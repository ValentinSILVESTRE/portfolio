<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/event')]
class EventController extends AbstractController
{
    #[Route('', name: 'event_list', methods: ['GET', 'HEAD'])]
    public function list(
        EventRepository $eventRepository,
    ): Response {
        return $this->json($eventRepository->findAll(), 200, [], ['groups' => 'event']);
    }

    #[Route('/future', name: 'futures_event_list', methods: ['GET', 'HEAD'])]
    public function futures_event_list(
        EventRepository $eventRepository,
    ): Response {
        return $this->json($eventRepository->findAllFutures(), 200, [], ['groups' => 'event']);
    }

    #[Route('/{id<\d+>}', name: 'event_show', methods: ['GET', 'HEAD'])]
    public function show(
        Event $event,
        EventRepository $eventRepository
    ): Response {
        $event = $eventRepository->find($event);
        return $this->json($event, 200, [], ['groups' => 'event']);
    }

    #[Route('/{query}', name: 'event_search', methods: ['GET', 'HEAD'])]
    public function search(
        string $query,
        EventRepository $eventRepository
    ): Response {
        return $this->json($eventRepository->findByTitle($query), 200, [], ['groups' => 'event']);
    }

    #[Route('', name: 'event_new', methods: ['POST'])]
    public function new(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        EventRepository $eventRepository,
    ): Response {
        $data = $request->getContent();

        $event = $serializer->deserialize($data, Event::class, 'json', [
            'groups' => 'event'
        ]);

        $entityManager->persist($event);

        // On ajoute le parent si on l'a définit et qu'il existe dans la bbd
        if (
            $event->getParentEventId() != null &&
            $eventRepository->find($event->getParentEventId())
        ) {
            $parentEvent = $eventRepository->find($event->getParentEventId());
            $parentEvent->addSubEvent($event);
        }

        $entityManager->flush();

        return $this->json($event, 201, [], [
            'groups' => 'event'
        ]);
    }

    #[Route('/{id<\d+>}', name: 'event_update', methods: ['PUT'])]
    public function update(
        int $id,
        Request $request,
        EventRepository $eventRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ): Response {
        $event = $eventRepository->find($id);
        if (!$event) {
            return $this->json([
                'message' => 'event not found'
            ], 404);
        }

        $data = $request->getContent();

        $serializer->deserialize($data, Event::class, 'json', [
            'object_to_populate' => $event
        ]);

        $parent = $event->getParentEvent();
        // Si le parent est déjà un enfant, alors on renvoie une erreur 508 loop detected
        if (
            $parent &&
            ($event->isParentOf($parent->getId()) ||
                $event->getId() === $parent->getId()
            )
        ) {
            return $this->json([
                'message' => 'Event cannot be his own parent or on of his children'
            ], 508);
        }

        $entityManager->flush();
        return $this->json($event, 200, [], [
            'groups' => 'event'
        ]);
    }

    #[Route('/{id<\d+>}', name: 'event_delete', methods: ['DELETE'])]
    public function delete(
        int $id,
        EventRepository $eventRepository,
        EntityManagerInterface $entityManager
    ): Response {
        $event = $eventRepository->find($id);
        if (!$event) {
            return $this->json([
                'message' => 'Event not found'
            ], 404);
        }

        $entityManager->remove($event);
        $entityManager->flush();
        return $this->json([
            'message' => 'Event deleted successfully'
        ], 200, [], [
            'groups' => 'event'
        ]);
    }
}
