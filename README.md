**<h1 align='center' id='Informations'>Informations</h1>**

[![LTS](https://img.shields.io/badge/Symfony-5.4-blue)](https://symfony.com/releases/5.4)
[![LTS](https://img.shields.io/badge/Composer-2.2.3-purple)](https://getcomposer.org/changelog/2.2.3)
[![LTS](https://img.shields.io/badge/PHP-8.1-oranged)](https://www.php.net/releases/8.1/en.php)
[![LTS](https://img.shields.io/badge/Angular-13.2.5-yellow)](https://angular.io/guide/releases)

---

**<h1 align='center' id='Utilisation'>Utilisation</h1>**
Site disponible à l'addresse [https://www.valentin.silvestre.com/](https://www.valentin.silvestre.com/)

---

**<h1 align='center' id='Sommaire'>Sommaire</h1>**

1.[To Do](#To_Do)

2.[Projets](#Projets)

**<h1 align='center' id='To_Do'>1. To Do</h1>**

## Calendrier

- Faire une pagination
- Choisir une icône par event plutôt que celui de la catégorie (rolland garros : football icone ?)
- Faire un attribut final pour les events qui n'auront pas d'enfants
- Faire un composant event dans la liste globale
- Faire une seule fois la requête des event et dans le service
- Faire une limite d'event requetés en get
- Empecher de faire planter le site en faisant 1000 requetes
- Faire une barre latérale avec les options du calendrier (acceuil, new event, search, ...)
- Faire un bouton totop
- Afficher les événements par jour / semaine / mois / passés / recherche
- Faire de l'héritage de composants, notament pour screenSize (responsive)

## Urgent

- Afficher le h1 sauf s'il est VRAIMENT trop long

## Principal

- Calendar: main-calendar et search sont des copiés collés, ils doivent partager la logique
- Faire des objets pour les sons avec leur url et leur durée

## Secondaire

- Jeu de la vie: Permettre d'avancer d'un seul tour
- Taquin
  - Au début on affiche l'image, on la découpe et on éparpilles les morceaux, le tout animé
  - Trier les images par hauteur
  - Drag and drop

## Bugs

- Pokemon introuvable
- Pokemon: Photon Geyser attaque tout le temps sur le spécial
- Calendar: Recherche contenant uniquement des chiffres

---

**<h1 align='center' id='Projets'>2. Projets</h1>**

| Nom              | HTML | CSS | JS  | PHP | SYMFONY | ANGULAR | EXTERNE | RESPONSIVE |
| :--------------- | :--: | :-: | :-: | :-: | :-----: | :-----: | :-----: | :--------: |
| Portfolio        |  ✅  | ✅  | ✅  | ✅  |   ✅    |   ✅    |   ❌    |     ✅     |
| Stage MHB        |  ✅  | ✅  | ✅  | ✅  |   ✅    |   ❌    |   ✅    |     ❌     |
| Ancien portfolio |  ✅  | ✅  | ✅  | ✅  |   ✅    |   ❌    |   ✅    |     ❌     |
| Calendar         |  ✅  | ✅  | ✅  | ✅  |   ✅    |   ✅    |   ❌    |     ✅     |
| Pokemon          |  ✅  | ✅  | ✅  | ✅  |   ✅    |   ✅    |   ❌    |     ✅     |
| Taquin           |  ✅  | ✅  | ✅  | ✅  |   ❌    |   ✅    |   ❌    |     ✅     |
| Puissance 4      |  ✅  | ✅  | ✅  | ✅  |   ❌    |   ✅    |   ❌    |     ✅     |
| Mines            |  ✅  | ✅  | ✅  | ✅  |   ❌    |   ✅    |   ❌    |     ✅     |
| Game of Life     |  ✅  | ✅  | ✅  | ✅  |   ❌    |   ✅    |   ❌    |     ✅     |
| Mondrian         |  ✅  | ✅  | ❌  | ❌  |   ❌    |   ❌    |   ❌    |     ✅     |